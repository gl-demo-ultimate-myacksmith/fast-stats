use assert_cmd::Command;
use assert_fs::{prelude::*, NamedTempFile, TempDir};
use std::time::Duration;

#[derive(Copy, Clone, Debug)]
struct TestCase {
    log_name: &'static str,
    log_type: &'static str,
    snapshot: &'static str,
}

const TEST_CASES: &[TestCase] = &[
    TestCase {
        log_name: "tests/data/api_long.log",
        log_type: "api",
        snapshot: "api",
    },
    TestCase {
        log_name: "tests/data/prod_long.log",
        log_type: "production",
        snapshot: "prod",
    },
    TestCase {
        log_name: "tests/data/gitaly_long.log",
        log_type: "gitaly",
        snapshot: "gitaly",
    },
    TestCase {
        log_name: "tests/data/sidekiq_long.log",
        log_type: "sidekiq",
        snapshot: "sidekiq",
    },
    TestCase {
        log_name: "tests/data/shell_long.log",
        log_type: "shell",
        snapshot: "shell",
    },
];

const STAT_FIELDS: &[&str] = &[
    "count", "rps", "p99", "p95", "median", "max", "min", "score", "fail", "std-dev",
];

const TOP_FIELDS: &[&str] = &[
    "count",
    "rps",
    "duration",
    "db",
    "redis",
    "gitaly",
    "rugged",
    "queue",
    "cpu-s",
    "response-bytes",
    "disk-read",
    "disk-write",
    "mem",
    "fail-ct",
];

const FORMATS: &[&str] = &["text", "json", "csv", "md"];

// Give commands up to five seconds to execute to give anemic runners
// an excess of time. The main goal is to confirm that the process
// isn't inadvertently blocking on stdin.
#[test]
fn run_stats() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_stats_stdin() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .pipe_stdin("tests/data/gitaly_long.log")
        .unwrap()
        .timeout(Duration::from_secs(5))
        .assert();
    assert.success();
}

#[test]
fn run_stats_verbose() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg("--verbose")
            .arg("--thread-ct") // Run single-threaded for deterministic output.
            .arg("1")
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats_verbose", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_stats_verbose_color() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let output = cmd
        .arg("--color-output")
        .arg("--thread-ct")
        .arg("1")
        .arg("--verbose")
        .arg("tests/data/api_long.log")
        .timeout(Duration::from_secs(5))
        .unwrap();

    assert!(output.status.success());
    let snapshot = "stats_verbose_color";
    insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
}

#[test]
fn run_stats_interval() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg("--interval")
            .arg("1m")
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats_interval", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_stats_sort_by() {
    for &tc in TEST_CASES {
        for &sort_by in STAT_FIELDS {
            let mut cmd = Command::cargo_bin("fast-stats").unwrap();
            let output = cmd
                .arg("--sort-by")
                .arg(sort_by)
                .arg(tc.log_name)
                .timeout(Duration::from_secs(5))
                .unwrap();

            assert!(output.status.success());
            let snapshot = format!("{}_stats_sort_by_{sort_by}", tc.snapshot);
            insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
        }
    }
}

#[test]
fn run_stats_print_field() {
    for &tc in TEST_CASES {
        for &print_field in STAT_FIELDS {
            let mut cmd = Command::cargo_bin("fast-stats").unwrap();
            let output = cmd
                .arg("--print-fields")
                .arg(print_field)
                .arg("--")
                .arg(tc.log_name)
                .timeout(Duration::from_secs(5))
                .unwrap();

            assert!(output.status.success());
            let snapshot = format!("{}_stats_print_field_{print_field}", tc.snapshot);
            insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
        }
    }
}

#[test]
fn run_stats_format() {
    for &tc in TEST_CASES {
        for &format in FORMATS {
            let mut cmd = Command::cargo_bin("fast-stats").unwrap();
            let output = cmd
                .arg("--format")
                .arg(format)
                .arg(tc.log_name)
                .timeout(Duration::from_secs(5))
                .unwrap();

            assert!(output.status.success());
            let snapshot = format!("{}_stats_format_{format}", tc.snapshot);
            insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
        }
    }
}

#[test]
fn run_stats_log_type() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg("--type")
            .arg(tc.log_type)
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats_log_type", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_stats_limit() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg("--limit")
            .arg("5")
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats_limit", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_stats_search() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let output = cmd
        .arg("--search")
        .arg("findcommit")
        .arg("tests/data/gitaly_long.log")
        .timeout(Duration::from_secs(5))
        .unwrap();

    assert!(output.status.success());
    let snapshot = "stats_search";
    insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
}

#[test]
fn run_stats_single_threaded() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .arg("--thread-ct")
        .arg("1")
        .arg("tests/data/prod_long.log")
        .timeout(Duration::from_secs(5))
        .assert();

    assert.success();
}

#[test]
fn run_stats_cmp() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg("--compare")
            .arg(tc.log_name)
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats_cmp", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_stats_invalid_cmp() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .arg("--compare")
        .arg("tests/data/gitaly_long.log")
        .arg("tests/data/sidekiq_long.log")
        .timeout(Duration::from_secs(5))
        .assert();
    assert.failure();
}

#[test]
fn run_stats_bench() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg("--benchmark")
            .arg("15.0")
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats_benchmark", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_stats_invalid_bench() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .arg("--benchmark")
        .arg("not-a-version")
        .arg("tests/data/sidekiq_long.log")
        .timeout(Duration::from_secs(5))
        .assert();
    assert.failure();
}

#[test]
fn run_top_display() {
    for &tc in TEST_CASES {
        for &format in &["both", "percentage", "value"] {
            let mut cmd = Command::cargo_bin("fast-stats").unwrap();
            let output = cmd
                .arg("top")
                .arg("--display")
                .arg(format)
                .arg(tc.log_name)
                .timeout(Duration::from_secs(5))
                .unwrap();

            assert!(output.status.success());
            let snapshot = format!("{}_stats_top_{format}", tc.snapshot);
            insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
        }
    }
}

#[test]
fn run_top_sort_by() {
    for &tc in TEST_CASES {
        for &field in TOP_FIELDS {
            let mut cmd = Command::cargo_bin("fast-stats").unwrap();
            let output = cmd
                .arg("top")
                .arg("--sort-by")
                .arg(field)
                .arg(tc.log_name)
                .timeout(Duration::from_secs(5))
                .unwrap();

            assert!(output.status.success());
            let snapshot = format!("{}_stats_top_sort_{field}", tc.snapshot);
            insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
        }
    }
}

#[test]
fn run_top_interval() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg("top")
            .arg("--interval")
            .arg("1m")
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats_top_interval", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_top_limit() {
    for &tc in TEST_CASES {
        let mut cmd = Command::cargo_bin("fast-stats").unwrap();
        let output = cmd
            .arg("top")
            .arg("--limit")
            .arg("5")
            .arg(tc.log_name)
            .timeout(Duration::from_secs(5))
            .unwrap();

        assert!(output.status.success());
        let snapshot = format!("{}_stats_top_limit", tc.snapshot);
        insta::assert_snapshot!(snapshot, String::from_utf8_lossy(&output.stdout));
    }
}

#[test]
fn run_top_stdin() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .arg("top")
        .pipe_stdin("tests/data/gitaly_long.log")
        .unwrap()
        .timeout(Duration::from_secs(5))
        .assert();
    assert.success();
}

#[test]
fn run_errors() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .arg("errors")
        .arg("tests/data/gitaly_long.log")
        .timeout(Duration::from_secs(5))
        .assert();
    assert.success();
}

#[test]
fn run_errors_stdin() {
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .arg("errors")
        .pipe_stdin("tests/data/gitaly_long.log")
        .unwrap()
        .timeout(Duration::from_secs(5))
        .assert();
    assert.success();
}

#[test]
fn run_plot() {
    let tmp_dir = TempDir::new().unwrap();
    tmp_dir
        .copy_from("tests/data", &["gitaly_long.log"])
        .unwrap();
    let in_path = tmp_dir.path().to_owned().join("gitaly_long.log");

    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .current_dir(tmp_dir.path())
        .arg("plot")
        .arg(in_path)
        .timeout(Duration::from_secs(5))
        .assert();
    assert.success();

    let out_path = tmp_dir.path().to_owned().join("gitaly_long_plot.png");
    let kind = infer::get_from_path(out_path).unwrap().unwrap();
    assert_eq!(kind.extension(), "png");
}

#[test]
fn run_plot_outfile() {
    let tmp_file = NamedTempFile::new("plot_out.png").unwrap();
    let mut cmd = Command::cargo_bin("fast-stats").unwrap();
    let assert = cmd
        .arg("plot")
        .arg("tests/data/gitaly_long.log")
        .arg("--outfile")
        .arg(tmp_file.path())
        .timeout(Duration::from_secs(5))
        .assert();
    assert.success();

    let kind = infer::get_from_path(tmp_file.path()).unwrap().unwrap();
    assert_eq!(kind.extension(), "png");
}
