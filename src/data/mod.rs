use crate::event::basic::BasicEvent;
use crate::event::Event;
use crate::log_type::LogType;
use crate::time_chunks::Interval;

pub mod detail;
pub mod raw;

/// A trait for the ability to insert a BasicEvent
/// into the implementing container.
pub trait BasicData: RawOutput {
    fn insert_basic(&mut self, event: BasicEvent);
}

pub trait RawOutput: Send + Sync {
    fn new(args: RawArgs) -> Self;
    fn insert_event<T: Event>(&mut self, event: T);
    fn coalesce(&mut self, local_map: Self);
    fn finalize(&mut self);
}

pub trait FilterOutput: RawOutput {
    fn filter(self, term: Option<&str>) -> Self;
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct RawArgs {
    pub log_t: LogType,
    pub interval: Option<Interval>,
}

impl RawArgs {
    pub fn new(log_t: LogType, interval: Option<Interval>) -> RawArgs {
        RawArgs { log_t, interval }
    }
}

impl From<LogType> for RawArgs {
    fn from(log_t: LogType) -> RawArgs {
        RawArgs::new(log_t, None)
    }
}
