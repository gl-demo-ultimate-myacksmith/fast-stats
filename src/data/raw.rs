use super::{BasicData, BasicEvent, FilterOutput, RawArgs, RawOutput};
use crate::event::Event;
use crate::log_type::LogType;
use crate::stats::Stats;
use crate::stats_vec::StatsVec;
use crate::HashMap;

use chrono::Duration;
use rayon::prelude::*;

#[derive(Clone, PartialEq, Debug)]
pub struct RawData {
    pub data: Vec<f64>,
    pub fails: usize,
}

impl RawData {
    #[inline]
    pub fn new() -> RawData {
        RawData {
            data: Vec::new(),
            fails: 0,
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct RawDataMap {
    pub data: HashMap<String, RawData>,
    pub start_time: String,
    pub end_time: String,
    pub log_t: LogType,
}

impl BasicData for RawDataMap {
    fn insert_basic(&mut self, event: BasicEvent) {
        // Don't count info events towards totals
        if event.status.is_info() {
            return;
        }

        self.update_time(event.time);

        let action = event.action;
        let (_key, entry) = self
            .data
            .raw_entry_mut()
            .from_key(action.as_ref())
            .or_insert_with(|| (action.as_ref().to_string(), RawData::new()));

        entry.data.push(event.dur_ms);

        if event.status.is_fail() {
            entry.fails += 1;
        }
    }
}

impl RawOutput for RawDataMap {
    fn new(args: RawArgs) -> RawDataMap {
        RawDataMap {
            data: HashMap::default(),
            start_time: String::from("9999"), // greater than all valid times
            end_time: String::from("0000"),   // less than all valid times
            log_t: args.log_t,
        }
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        // Don't count info events towards totals
        if event.status().is_info() {
            return;
        }

        self.update_time(event.raw_time());

        let action = event.action();
        let (_key, entry) = self
            .data
            .raw_entry_mut()
            .from_key(action.as_ref())
            .or_insert_with(|| (action.to_string(), RawData::new()));

        entry.data.push(event.dur_ms());

        if event.status().is_fail() {
            entry.fails += 1;
        }
    }

    fn coalesce(&mut self, local_map: RawDataMap) {
        for (action, local_data) in local_map.data {
            let main_entry = self.data.entry(action).or_insert_with(RawData::new);
            main_entry.data.extend(local_data.data);
            main_entry.fails += local_data.fails;
        }

        self.start_time = self.start_time.clone().min(local_map.start_time);
        self.end_time = self.end_time.clone().max(local_map.end_time);
    }

    fn finalize(&mut self) {}
}

impl FilterOutput for RawDataMap {
    fn filter(mut self, term: Option<&str>) -> RawDataMap {
        if let Some(s) = term {
            self.data
                .retain(|key, _| key.to_ascii_lowercase().contains(s));
        }

        self
    }
}

impl RawDataMap {
    pub fn duration(&self) -> Duration {
        match (
            self.log_t.parse_time(&self.start_time),
            self.log_t.parse_time(&self.end_time),
        ) {
            (Some(start), Some(end)) => end - start,
            _ => Duration::zero(),
        }
    }

    fn update_time(&mut self, time: &str) {
        if time < self.start_time.as_str() {
            self.start_time = time.to_string();
        }

        if time > self.end_time.as_str() {
            self.end_time = time.to_string();
        }
    }

    pub fn into_stats_vec(mut self) -> StatsVec {
        let dur = self.duration();
        let stats_t = self.log_t.stats_t();

        let stats: Vec<_> = self
            .data
            .par_drain()
            .map(|(k, v)| Stats::from_raw_data(k, v, dur))
            .collect();

        StatsVec { stats_t, stats }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::event::*;

    #[test]
    fn raw_data_map_insert_event() {
        let input = r#"2019-02-01_11:30:20.13239 time="2019-02-01T11:30:20Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.method=InfoRefsUploadPack grpc.request.fullMethod=/gitaly.SmartHTTPService/InfoRefsUploadPack grpc.request.glRepository=project-14306 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.SmartHTTPService grpc.start_time="2019-02-01T11:30:19Z" grpc.time_ms=322.1 peer.address=@ span.kind=server system=grpc"#;
        let event = GitalyEvent::parse(input).unwrap();
        let mut raw = RawDataMap::new(LogType::GitalyText.into());
        raw.insert_event(event);

        assert_eq!(
            raw.data.into_iter().collect::<Vec<_>>(),
            vec![(
                String::from("InfoRefsUploadPack"),
                RawData {
                    data: vec![322.1],
                    fails: 0,
                }
            )]
        );
    }

    #[test]
    fn raw_data_map_insert_failed_event() {
        let input = r#"{"correlation_id":"LHjQZjYDHQ9","error":"rpc error: code = FailedPrecondition desc = conflict side missing","grpc.code":"FailedPrecondition","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"limited","grpc.method":"ListConflictFiles","grpc.request.deadline":"2020-11-18T17:47:44Z","grpc.request.fullMethod":"/gitaly.ConflictsService/ListConflictFiles","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-26273","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.ConflictsService","grpc.start_time":"2020-11-18T17:46:48Z","grpc.time_ms":188.34,"level":"warning","msg":"finished streaming call with code FailedPrecondition","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:46:48.854Z"}"#;
        let event = GitalyJsonEvent::parse(input).unwrap();
        let mut raw = RawDataMap::new(LogType::GitalyJson.into());
        raw.insert_event(event);

        assert_eq!(
            raw.data.into_iter().collect::<Vec<_>>(),
            vec![(
                String::from("ListConflictFiles"),
                RawData {
                    data: vec![188.34],
                    fails: 1,
                }
            )]
        );
    }

    #[test]
    fn raw_data_map_coalesce() {
        let input_a = r#"{"correlation_id":"qNWQPts8Zs","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"GetBlobs","grpc.request.deadline":"2020-11-18T17:22:37Z","grpc.request.fullMethod":"/gitaly.BlobService/GetBlobs","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-39207","grpc.request.repoPath":"@hashed/6a/f7/6af704b5fae984705e2046c7ed9972a0c10e681ccfaf8f521a50c45c1df1ca7a.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.BlobService","grpc.start_time":"2020-11-18T17:22:27Z","grpc.time_ms":42.833,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.021Z"}
{"correlation_id":"qYFWWJJAZ25","grpc.code":"OK","grpc.meta.client_name":"gitlab-shell","grpc.meta.deadline_type":"none","grpc.method":"SSHUploadPack","grpc.request.fullMethod":"/gitaly.SSHService/SSHUploadPack","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-38026","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.SSHService","grpc.start_time":"2020-11-18T17:29:27Z","grpc.time_ms":302.327,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:29:28.049Z"}"#;
        let input_b = r#"{"correlation_id":"CyxviAtYMAa","grpc.code":"OK","grpc.meta.client_name":"gitlab-shell","grpc.meta.deadline_type":"none","grpc.method":"SSHUploadPack","grpc.request.fullMethod":"/gitaly.SSHService/SSHUploadPack","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-1515","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.SSHService","grpc.start_time":"2020-11-18T17:22:28Z","grpc.time_ms":199.295,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.802Z"}
{"correlation_id":"7MGB7vWeyt5","grpc.code":"OK","grpc.meta.client_name":"gitlab-shell","grpc.meta.deadline_type":"none","grpc.method":"SSHUploadPack","grpc.request.fullMethod":"/gitaly.SSHService/SSHUploadPack","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-38026","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.SSHService","grpc.start_time":"2020-11-18T17:30:27Z","grpc.time_ms":297.168,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:30:28.115Z"}"#;
        let mut raw_a = RawDataMap::new(LogType::GitalyJson.into());
        let mut raw_b = RawDataMap::new(LogType::GitalyJson.into());
        let events_a: Vec<_> = input_a
            .lines()
            .map(|l| GitalyJsonEvent::parse(l).unwrap())
            .collect();
        let events_b: Vec<_> = input_b
            .lines()
            .map(|l| GitalyJsonEvent::parse(l).unwrap())
            .collect();

        events_a.into_iter().for_each(|e| raw_a.insert_event(e));
        events_b.into_iter().for_each(|e| raw_b.insert_event(e));
        raw_a.coalesce(raw_b);

        assert_eq!(raw_a.start_time, String::from("2020-11-18T17:22:28.021Z"));
        assert_eq!(raw_a.end_time, String::from("2020-11-18T17:30:28.115Z"));

        let mut blob_data = raw_a.data.get("GetBlobs").unwrap().data.clone();
        blob_data.sort_by(|x, y| x.partial_cmp(y).unwrap());
        assert_eq!(blob_data, vec![42.833]);

        let mut ssh_data = raw_a.data.get("SSHUploadPack").unwrap().data.clone();
        ssh_data.sort_by(|x, y| x.partial_cmp(y).unwrap());
        assert_eq!(ssh_data, vec![199.295, 297.168, 302.327]);

        assert_eq!(raw_a.data.get("GetBlobs").unwrap().fails, 0);
        assert_eq!(raw_a.data.get("SSHUploadPack").unwrap().fails, 0);
    }

    #[test]
    fn raw_data_map_filter() {
        let input = r#"{"correlation_id":"qNWQPts8Zs","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.meta.deadline_type":"regular","grpc.method":"GetBlobs","grpc.request.deadline":"2020-11-18T17:22:37Z","grpc.request.fullMethod":"/gitaly.BlobService/GetBlobs","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-39207","grpc.request.repoPath":"@hashed/6a/f7/6af704b5fae984705e2046c7ed9972a0c10e681ccfaf8f521a50c45c1df1ca7a.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.BlobService","grpc.start_time":"2020-11-18T17:22:27Z","grpc.time_ms":42.833,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:22:28.021Z"}
{"correlation_id":"qYFWWJJAZ25","grpc.code":"OK","grpc.meta.client_name":"gitlab-shell","grpc.meta.deadline_type":"none","grpc.method":"SSHUploadPack","grpc.request.fullMethod":"/gitaly.SSHService/SSHUploadPack","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-38026","grpc.request.repoPath":"group/project.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"group","grpc.service":"gitaly.SSHService","grpc.start_time":"2020-11-18T17:29:27Z","grpc.time_ms":302.327,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","pid":6113,"span.kind":"server","system":"grpc","time":"2020-11-18T17:29:28.049Z"}"#;

        let mut raw = RawDataMap::new(LogType::GitalyJson.into());
        let events: Vec<_> = input
            .lines()
            .map(|l| GitalyJsonEvent::parse(l).unwrap())
            .collect();
        events.into_iter().for_each(|e| raw.insert_event(e));

        let filtered = raw.filter(Some("upload"));

        let keys: Vec<_> = filtered.data.keys().cloned().collect();
        assert_eq!(keys, vec!["SSHUploadPack"]);
    }
}
