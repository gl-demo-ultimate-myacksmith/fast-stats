use crate::data::raw::RawData;
use crate::data::{FilterOutput, RawArgs, RawOutput};
use crate::event::{Event, Status};
use crate::field::{EventField, StatField};
use crate::stats::{Stats, StatsType};
use crate::stats_vec::{StatsVec, VerboseStatsVec};
use crate::util::{self, BLUE, CYAN, GREEN, RED, RESET, YELLOW};
use crate::HashMap;

use chrono::prelude::*;
use rayon::prelude::*;

#[derive(Clone, PartialEq, Debug)]
pub struct DetailTimeEvent {
    pub time: DateTime<FixedOffset>,
    pub dur: f64,
    pub status: Status,
    pub db: Option<f64>,
    pub gitaly: Option<f64>,
    pub rugged: Option<f64>,
    pub redis: Option<f64>,
    pub queue: Option<f64>,
    pub cpu_s: Option<f64>,
    pub correlation_id: Option<String>,
    pub user: Option<String>,
    pub project: Option<String>,
    pub path: Option<String>,
    pub client: Option<String>,
}

impl DetailTimeEvent {
    pub fn new<T: Event>(event: &T, time: DateTime<FixedOffset>) -> DetailTimeEvent {
        DetailTimeEvent {
            time,
            dur: event.dur_ms() / 1000.0,
            db: event.db(),
            gitaly: event.gitaly(),
            rugged: event.rugged(),
            redis: event.redis(),
            queue: event.queue(),
            cpu_s: event.cpu_s(),
            status: event.status(),
            correlation_id: event.correlation_id().map(|c| c.to_string()),
            user: event.user().map(|u| u.to_string()),
            project: event.project().map(|p| p.to_string()),
            path: event.path().map(|p| p.to_string()),
            client: event.client_type().map(|c| c.to_string()),
        }
    }

    pub fn as_string(&self, title: &str, color: bool) -> String {
        if color {
            format!(
                "{} dur: {:5.2}s  {}db: {:5.2}s{}  {}redis: {:5.2}s{}  {}gitaly: {:5.2}s{}  {}rugged: {:5.2}s{}  {}cpu: {:5.2}s{} -- {}  {}",
                title,
                self.dur,
                BLUE,
                self.db.unwrap_or_default(),
                RESET,
                RED,
                self.redis.unwrap_or_default(),
                RESET,
                GREEN,
                self.gitaly.unwrap_or_default(),
                RESET,
                YELLOW,
                self.rugged.unwrap_or_default(),
                RESET,
                CYAN,
                self.cpu_s.unwrap_or_default(),
                RESET,
                self.time.to_rfc3339_opts(SecondsFormat::Millis, true),
                self.correlation_id.as_deref().unwrap_or_default(),
            )
        } else {
            format!(
                "{} dur: {:5.2}s  db: {:5.2}s  redis: {:5.2}s  gitaly: {:5.2}s  rugged: {:5.2}s  cpu: {:5.2}s -- {} {}",
                title,
                self.dur,
                self.db.unwrap_or_default(),
                self.redis.unwrap_or_default(),
                self.gitaly.unwrap_or_default(),
                self.rugged.unwrap_or_default(),
                self.cpu_s.unwrap_or_default(),
                self.time.to_rfc3339(),
                self.correlation_id.as_deref().unwrap_or_default(),
            )
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct DetailTimeMap {
    pub data: HashMap<String, Vec<DetailTimeEvent>>,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
}

impl RawOutput for DetailTimeMap {
    fn new(_args: RawArgs) -> DetailTimeMap {
        DetailTimeMap {
            data: HashMap::default(),
            start_time: DateTime::parse_from_rfc3339("3000-01-01T00:00:00Z").unwrap(), // larger than any valid start time
            end_time: DateTime::parse_from_rfc3339("1000-01-01T00:00:00Z").unwrap(), // smaller than any valid start time
        }
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        // Don't count info events towards totals
        if event.status().is_info() {
            return;
        }

        if let Some(ts) = T::parse_time(event.raw_time()) {
            self.update_time(&ts);

            let detail_event = DetailTimeEvent::new(&event, ts);

            let action = event.action();
            let (_key, entry) = self
                .data
                .raw_entry_mut()
                .from_key(action.as_ref())
                .or_insert_with(|| (action.to_string(), Vec::new()));

            entry.push(detail_event);
        }
    }

    fn coalesce(&mut self, local_map: DetailTimeMap) {
        for (action, local_data) in local_map.data {
            let main_entry = self.data.entry(action).or_insert_with(Vec::new);
            main_entry.extend(local_data);
        }

        self.start_time = self.start_time.min(local_map.start_time);
        self.end_time = self.end_time.max(local_map.end_time);
    }

    fn finalize(&mut self) {}
}

impl FilterOutput for DetailTimeMap {
    fn filter(mut self, term: Option<&str>) -> DetailTimeMap {
        if let Some(s) = term {
            self.data
                .retain(|key, _| key.to_ascii_lowercase().contains(s));
        }

        self
    }
}

impl DetailTimeMap {
    pub fn sort_by(&mut self, field: EventField) {
        use EventField as Ef;
        self.data.par_iter_mut().for_each(|(_action, items)| {
            items.sort_unstable_by(|x, y| match field {
                Ef::Duration => util::cmp(x.dur, y.dur),
                Ef::Db => util::cmp(x.db, y.db),
                Ef::Redis => util::cmp(x.redis, y.redis),
                Ef::Gitaly => util::cmp(x.gitaly, y.gitaly),
                Ef::Rugged => util::cmp(x.rugged, y.rugged),
                Ef::CpuS => util::cmp(x.cpu_s, y.cpu_s),
                Ef::Queue => util::cmp(x.queue, y.queue),
                _ => std::cmp::Ordering::Equal, // Don't sort.
            })
        });
    }

    // Useful for testing
    #[allow(dead_code)]
    pub fn into_stats_vec(self, stats_t: StatsType) -> StatsVec {
        let dur = self.end_time - self.start_time;
        let stats: Vec<_> = self
            .data
            .into_par_iter()
            .map(|(action, events)| {
                let mut raw = RawData::new();
                raw.fails = events.iter().filter(|e| e.status.is_fail()).count();
                raw.data.extend(events.into_iter().map(|e| e.dur * 1000.0));

                Stats::from_raw_data(action, raw, dur)
            })
            .collect();

        StatsVec::new(stats_t, stats)
    }

    pub fn set_times(
        &mut self,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
    ) {
        self.start_time = start_time;
        self.end_time = end_time;
    }

    pub fn into_verbose_stats_vec(
        self,
        stats_t: StatsType,
        sort_by: StatField,
        color: bool,
    ) -> VerboseStatsVec {
        let examples = self.find_examples(color);

        let dur = self.end_time - self.start_time;

        let stats: Vec<_> = self
            .data
            .into_par_iter()
            .map(|(action, events)| {
                let mut raw = RawData::new();
                raw.fails = events.iter().filter(|e| e.status.is_fail()).count();
                raw.data.extend(events.into_iter().map(|e| e.dur * 1000.0));

                Stats::from_raw_data(action, raw, dur)
            })
            .collect();

        let stats_v = StatsVec::new(stats_t, stats).sort_by(sort_by);

        VerboseStatsVec::new(stats_t, stats_v, examples)
    }

    fn find_examples(&self, color: bool) -> HashMap<String, [Option<String>; 4]> {
        self.data
            .par_iter()
            .map(|(action, events)| {
                let (p100, _) = util::percentile_obj(events, 100);
                let (mut p99, p99_pctl) = util::percentile_obj(events, 99);
                let (mut p95, p95_pctl) = util::percentile_obj(events, 95);
                let (mut median, median_pctl) = util::percentile_obj(events, 50);

                if median == p100 || median == p99 || median == p95 {
                    median = None;
                }

                if p95 == p99 || p95 == p100 {
                    p95 = None;
                }

                if p99 == p100 {
                    p99 = None;
                }

                let examples = [
                    p100.map(|e| e.as_string("MAX", color)),
                    p99.map(|e| e.as_string(&format!("P{}", p99_pctl), color)),
                    p95.map(|e| e.as_string(&format!("P{}", p95_pctl), color)),
                    median.map(|e| e.as_string(&format!("P{}", median_pctl), color)),
                ];

                (action.clone(), examples)
            })
            .collect()
    }

    fn update_time(&mut self, time: &DateTime<FixedOffset>) {
        if time < &self.start_time {
            self.start_time = *time;
        }

        if time > &self.end_time {
            self.end_time = *time;
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::data::raw::RawDataMap;
    use crate::data::RawArgs;
    use crate::execute::DynBatcher;
    use crate::field::StatField;
    use crate::log_type::LogType;
    use crate::parse::{BasicParser, ParseArgs};
    use crate::stats::*;

    use std::fs::File;

    #[test]
    fn detail_stats_v_matches_normal() {
        let input_file = File::open("tests/data/api.log").unwrap();
        let batcher = DynBatcher::new(50, Box::new(input_file));
        let args = ParseArgs::new(None, 1, Vec::new());
        let stats_v = BasicParser::<RawDataMap>::new(RawArgs::new(LogType::ApiJsonDurS, None))
            .parse(batcher, args.clone())
            .into_stats_vec()
            .sort_by(StatField::Count);

        let detail_file = File::open("tests/data/api.log").unwrap();
        let detail_batcher = DynBatcher::new(50, Box::new(detail_file));
        let detail_stats =
            LogType::ApiJsonDurS.calculate_detailed_time_data(None, detail_batcher, args);
        let detail_stats_v = detail_stats
            .into_stats_vec(StatsType::Api)
            .sort_by(StatField::default());

        assert_eq!(stats_v, detail_stats_v);
    }
}
