use super::*;
use crate::log_type::LogType;

use std::borrow::Cow;

/// The items needed to calculate duration stats and failure rate
/// for all log types.
#[derive(Clone, PartialEq, Debug)]
pub struct BasicEvent<'a> {
    pub action: Cow<'a, str>,
    pub dur_ms: f64,
    pub status: Status,
    pub time: &'a str,
}

impl<'a> BasicEvent<'a> {
    pub fn parse_fn(log_t: LogType) -> fn(&str) -> Option<BasicEvent> {
        match log_t {
            LogType::ProdJson => {
                |line| -> Option<BasicEvent> { ProdJsonEvent::parse(line).map(|e| e.into()) }
            }
            LogType::ProdJsonDurS => {
                |line| -> Option<BasicEvent> { ProdJsonDurSEvent::parse(line).map(|e| e.into()) }
            }
            LogType::ApiJson => {
                |line| -> Option<BasicEvent> { ApiJsonEvent::parse(line).map(|e| e.into()) }
            }
            LogType::ApiJsonDurS => {
                |line| -> Option<BasicEvent> { ApiJsonDurSEvent::parse(line).map(|e| e.into()) }
            }
            LogType::GitalyText => {
                |line| -> Option<BasicEvent> { GitalyEvent::parse(line).map(|e| e.into()) }
            }
            LogType::GitalyJson => {
                |line| -> Option<BasicEvent> { GitalyJsonEvent::parse(line).map(|e| e.into()) }
            }
            LogType::SidekiqText => {
                |line| -> Option<BasicEvent> { SidekiqEvent::parse(line).map(|e| e.into()) }
            }
            LogType::SidekiqJson => {
                |line| -> Option<BasicEvent> { SidekiqJsonEvent::parse(line).map(|e| e.into()) }
            }
            LogType::SidekiqJsonDurS => {
                |line| -> Option<BasicEvent> { SidekiqJsonDurSEvent::parse(line).map(|e| e.into()) }
            }
            LogType::ShellJson => {
                |line| -> Option<BasicEvent> { ShellJsonEvent::parse(line).map(|e| e.into()) }
            }
            _ => unreachable!(),
        }
    }

    pub fn parse_time(&self) -> Option<DateTime<FixedOffset>> {
        DateTime::parse_from_rfc3339(self.time).ok()
    }
}

impl<'a> From<ApiJsonEvent<'a>> for BasicEvent<'a> {
    fn from(event: ApiJsonEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(event.route),
            dur_ms: event.duration,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<ApiJsonDurSEvent<'a>> for BasicEvent<'a> {
    fn from(event: ApiJsonDurSEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(event.route),
            dur_ms: event.duration_s * 1000.0,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<ProdJsonEvent<'a>> for BasicEvent<'a> {
    fn from(event: ProdJsonEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(format!("{}#{}", event.controller, event.action)),
            dur_ms: event.duration,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<ProdJsonDurSEvent<'a>> for BasicEvent<'a> {
    fn from(event: ProdJsonDurSEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(format!("{}#{}", event.controller, event.action)),
            dur_ms: event.duration_s * 1000.0,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<GitalyEvent<'a>> for BasicEvent<'a> {
    fn from(event: GitalyEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(event.method),
            dur_ms: event.time_ms,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<GitalyJsonEvent<'a>> for BasicEvent<'a> {
    fn from(event: GitalyJsonEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(event.method),
            dur_ms: event.time_ms,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<SidekiqEvent<'a>> for BasicEvent<'a> {
    fn from(event: SidekiqEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(event.class),
            dur_ms: event.duration,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<SidekiqJsonEvent<'a>> for BasicEvent<'a> {
    fn from(event: SidekiqJsonEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(event.class),
            dur_ms: event.duration,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<SidekiqJsonDurSEvent<'a>> for BasicEvent<'a> {
    fn from(event: SidekiqJsonDurSEvent<'a>) -> Self {
        BasicEvent {
            action: Cow::from(event.class),
            dur_ms: event.duration_s * 1000.0,
            status: event.status(),
            time: event.time,
        }
    }
}

impl<'a> From<ShellJsonEvent<'a>> for BasicEvent<'a> {
    fn from(event: ShellJsonEvent<'a>) -> Self {
        let action = if let Some(url) = event.url {
            let path: String = url.split("api/v4").skip(1).collect();
            path.split("?key=").next().unwrap_or_default().to_owned()
        } else {
            String::from("n/a")
        };

        BasicEvent {
            action: Cow::from(action),
            dur_ms: event.duration_ms.unwrap_or_default(),
            status: event.status(),
            time: event.time,
        }
    }
}
