use crate::data::detail::DetailTimeMap;
use crate::data::{FilterOutput, RawArgs, RawOutput};
use crate::error::ErrorMap;
use crate::event::*;
use crate::execute::DynBatcher;
use crate::parse::{self, ParseArgs};
use crate::stats::StatsType;
use crate::time_chunks::{Interval, UnsortedTimeMap};

use chrono::prelude::*;
use std::process;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum LogType {
    ApiJson,
    ApiJsonDetail,
    ApiJsonError,
    ApiJsonDurS,
    ApiJsonDurSDetail,
    ApiJsonDurSError,
    GitalyText,
    GitalyJson,
    GitalyJsonDetail,
    GitalyJsonError,
    ProdJson,
    ProdJsonDetail,
    ProdJsonError,
    ProdJsonDurS,
    ProdJsonDurSDetail,
    ProdJsonDurSError,
    ShellJson,
    SidekiqText,
    SidekiqJson,
    SidekiqJsonDetail,
    SidekiqJsonError,
    SidekiqJsonDurS,
    SidekiqJsonDurSDetail,
    SidekiqJsonDurSError,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum OutputType {
    Detail,
    Error,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum AppType {
    Rails,
    Gitaly,
    Shell,
}

impl LogType {
    pub fn specify(self, out_t: OutputType) -> LogType {
        use LogType as Lt;
        use OutputType as Ot;

        match self {
            Lt::ApiJson => match out_t {
                Ot::Detail => Lt::ApiJsonDetail,
                Ot::Error => Lt::ApiJsonError,
            },
            Lt::ApiJsonDurS => match out_t {
                Ot::Detail => Lt::ApiJsonDurSDetail,
                Ot::Error => Lt::ApiJsonDurSError,
            },
            Lt::ProdJson => match out_t {
                Ot::Detail => Lt::ProdJsonDetail,
                Ot::Error => Lt::ProdJsonError,
            },
            Lt::ProdJsonDurS => match out_t {
                Ot::Detail => Lt::ProdJsonDurSDetail,
                Ot::Error => Lt::ProdJsonDurSError,
            },
            Lt::GitalyText => {
                eprintln!("This option is not supported for non-JSON logs");
                std::process::exit(0);
            }
            Lt::GitalyJson => match out_t {
                Ot::Detail => Lt::GitalyJsonDetail,
                Ot::Error => Lt::GitalyJsonError,
            },
            Lt::SidekiqText => {
                eprintln!("This option is not supported for non-JSON logs");
                std::process::exit(0);
            }
            Lt::SidekiqJson => match out_t {
                Ot::Detail => Lt::SidekiqJsonDetail,
                Ot::Error => Lt::SidekiqJsonError,
            },
            Lt::SidekiqJsonDurS => match out_t {
                Ot::Detail => Lt::SidekiqJsonDurSDetail,
                Ot::Error => Lt::SidekiqJsonDurSError,
            },
            Lt::ShellJson => Lt::ShellJson,
            _ => unreachable!(),
        }
    }

    pub fn calculate_unsorted_time_stats<T>(
        &self,
        intvl: Interval,
        batcher: DynBatcher,
        args: ParseArgs,
    ) -> UnsortedTimeMap<T>
    where
        T: RawOutput + 'static,
    {
        self.parse_raw_data(Some(intvl), batcher, args)
    }

    pub fn calculate_top_stats<O>(&self, batcher: DynBatcher, args: ParseArgs) -> O
    where
        O: RawOutput + Send + 'static,
    {
        use LogType as Lt;

        match self {
            Lt::SidekiqText | Lt::SidekiqJson => {
                eprintln!("top subcommand does not support older Sidekiq logs");
                process::exit(0);
            }
            Lt::GitalyText => {
                eprintln!("top subcommand does not support non-JSON Gitaly logs");
                process::exit(0);
            }
            _ => self.parse_raw_data(None, batcher, args),
        }
    }

    pub fn calculate_detailed_time_data(
        &self,
        intvl: Option<Interval>,
        batcher: DynBatcher,
        args: ParseArgs,
    ) -> DetailTimeMap {
        self.parse_raw_data::<DetailTimeMap>(intvl, batcher, args.clone())
            .filter(args.search_for.as_deref())
    }

    pub fn calculate_error_summary(&self, batcher: DynBatcher, args: ParseArgs) -> ErrorMap {
        self.parse_raw_data(None, batcher, args)
    }

    pub fn parse_raw_data<O>(
        &self,
        intvl: Option<Interval>,
        batcher: DynBatcher,
        args: ParseArgs,
    ) -> O
    where
        O: RawOutput + Send + 'static,
    {
        let out_args = RawArgs::new(*self, intvl);

        self.parse(out_args, batcher, args)
    }

    fn parse<O>(&self, out_args: RawArgs, batcher: DynBatcher, args: ParseArgs) -> O
    where
        O: RawOutput + Send + 'static,
    {
        parse::parse::<O>(out_args, batcher, args)
    }

    pub fn parse_time(&self, time: &str) -> Option<DateTime<FixedOffset>> {
        use LogType as Lt;

        match self {
            Lt::ApiJson => ApiJsonEvent::parse_time(time),
            Lt::ApiJsonDetail => ApiJsonEventDetail::parse_time(time),
            Lt::ApiJsonError => ApiJsonEventError::parse_time(time),
            Lt::ApiJsonDurS => ApiJsonDurSEvent::parse_time(time),
            Lt::ApiJsonDurSDetail => ApiJsonDurSEventDetail::parse_time(time),
            Lt::ApiJsonDurSError => ApiJsonDurSEventError::parse_time(time),
            Lt::GitalyText => GitalyEvent::parse_time(time),
            Lt::GitalyJson => GitalyJsonEvent::parse_time(time),
            Lt::GitalyJsonDetail => GitalyJsonEventDetail::parse_time(time),
            Lt::GitalyJsonError => GitalyJsonEventError::parse_time(time),
            Lt::ProdJson => ProdJsonEvent::parse_time(time),
            Lt::ProdJsonDetail => ProdJsonEventDetail::parse_time(time),
            Lt::ProdJsonError => ProdJsonEventError::parse_time(time),
            Lt::ProdJsonDurS => ProdJsonDurSEvent::parse_time(time),
            Lt::ProdJsonDurSDetail => ProdJsonDurSEventDetail::parse_time(time),
            Lt::ProdJsonDurSError => ProdJsonDurSEventError::parse_time(time),
            Lt::ShellJson => ShellJsonEvent::parse_time(time),
            Lt::SidekiqText => SidekiqEvent::parse_time(time),
            Lt::SidekiqJson => SidekiqJsonEvent::parse_time(time),
            Lt::SidekiqJsonDetail => SidekiqJsonEventDetail::parse_time(time),
            Lt::SidekiqJsonError => SidekiqJsonEventError::parse_time(time),
            Lt::SidekiqJsonDurS => SidekiqJsonDurSEvent::parse_time(time),
            Lt::SidekiqJsonDurSDetail => SidekiqJsonDurSEventDetail::parse_time(time),
            Lt::SidekiqJsonDurSError => SidekiqJsonDurSEventError::parse_time(time),
        }
    }

    pub const fn stats_t(&self) -> StatsType {
        use LogType as Lt;

        match self {
            Lt::ApiJson
            | Lt::ApiJsonDetail
            | Lt::ApiJsonError
            | Lt::ApiJsonDurS
            | Lt::ApiJsonDurSDetail
            | Lt::ApiJsonDurSError => StatsType::Api,
            Lt::GitalyText | Lt::GitalyJson | Lt::GitalyJsonDetail | Lt::GitalyJsonError => {
                StatsType::Gitaly
            }
            Lt::ProdJson
            | Lt::ProdJsonDetail
            | Lt::ProdJsonError
            | Lt::ProdJsonDurS
            | Lt::ProdJsonDurSDetail
            | Lt::ProdJsonDurSError => StatsType::Prod,
            Lt::ShellJson => StatsType::Shell,
            Lt::SidekiqText
            | Lt::SidekiqJson
            | Lt::SidekiqJsonDetail
            | Lt::SidekiqJsonError
            | Lt::SidekiqJsonDurS
            | Lt::SidekiqJsonDurSDetail
            | Lt::SidekiqJsonDurSError => StatsType::Sidekiq,
        }
    }

    pub fn app_type(&self) -> AppType {
        use LogType as Lt;
        match self {
            Lt::ApiJson
            | Lt::ApiJsonDetail
            | Lt::ApiJsonError
            | Lt::ApiJsonDurS
            | Lt::ApiJsonDurSDetail
            | Lt::ApiJsonDurSError
            | Lt::ProdJson
            | Lt::ProdJsonDetail
            | Lt::ProdJsonError
            | Lt::ProdJsonDurS
            | Lt::ProdJsonDurSDetail
            | Lt::ProdJsonDurSError
            | Lt::SidekiqText
            | Lt::SidekiqJson
            | Lt::SidekiqJsonDetail
            | Lt::SidekiqJsonError
            | Lt::SidekiqJsonDurS
            | Lt::SidekiqJsonDurSDetail
            | Lt::SidekiqJsonDurSError => AppType::Rails,
            Lt::GitalyText | Lt::GitalyJson | Lt::GitalyJsonDetail | Lt::GitalyJsonError => {
                AppType::Gitaly
            }
            Lt::ShellJson => AppType::Shell,
        }
    }
}
