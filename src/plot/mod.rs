use crate::data::RawOutput;
use crate::time_chunks::Interval;

mod gitaly;
mod plotting;
mod rails;
mod shell;

pub use gitaly::GitalyPlot;
pub use plotting::{plot_gitaly, plot_rails, plot_shell};
pub use rails::RailsPlot;
pub use shell::ShellPlot;

pub trait PlotStats: RawOutput {
    type Field: Copy;

    fn percentile(&self, field: Self::Field, pctile: u32) -> f64;
    fn sum_field(&self, field: Self::Field) -> f64;
    fn failure_percentage(&self) -> f64;
    fn sort(&mut self);
    fn len(&self) -> usize;

    fn rps(&self, interval: Interval) -> f64 {
        self.len() as f64 / interval.seconds() as f64
    }
}
