use super::PlotStats;
use crate::data::{RawArgs, RawOutput};
use crate::event::Event;
use crate::field::GitalyField;
use crate::util;

#[derive(Default, Clone, PartialEq, Debug)]
pub struct GitalyPlot {
    pub count: usize,
    pub dur: Vec<f64>,
    pub cpu_s: Vec<f64>,
    pub response_bytes: Vec<u64>,
    pub disk_read: Vec<u64>,
    pub disk_write: Vec<u64>,
    pub rss_kb: Vec<u64>,
    pub fails: usize,
}

impl RawOutput for GitalyPlot {
    fn new(_args: RawArgs) -> GitalyPlot {
        GitalyPlot::default()
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        // Gitaly logs events with command rusage details separately from RPC completion.
        // Capturing both here would double-count, only increment count for events that
        // cover the end of an RPC.
        if !event.status().is_info() {
            self.count += 1;
        }
        self.dur.push(event.dur_ms() / 1000.0);
        if let Some(cpu) = event.cpu_s() {
            self.cpu_s.push(cpu);
        }
        if let Some(bytes) = event.bytes_written() {
            self.response_bytes.push(bytes);
        }
        if let Some(read) = event.disk_read() {
            self.disk_read.push(read);
        }
        if let Some(write) = event.disk_write() {
            self.disk_write.push(write);
        }
        if let Some(rss) = event.mem() {
            self.rss_kb.push(rss);
        }
        if event.status().is_fail() {
            self.fails += 1;
        }
    }

    fn coalesce(&mut self, other: GitalyPlot) {
        self.count += other.count;
        self.dur.extend(&other.dur);
        self.cpu_s.extend(&other.cpu_s);
        self.response_bytes.extend(&other.response_bytes);
        self.disk_read.extend(&other.disk_read);
        self.disk_write.extend(&other.disk_write);
        self.rss_kb.extend(&other.rss_kb);
        self.fails += other.fails;

        self.sort();
    }

    fn finalize(&mut self) {
        self.sort();
    }
}

impl PlotStats for GitalyPlot {
    type Field = GitalyField;

    fn percentile(&self, field: GitalyField, pctile: u32) -> f64 {
        use GitalyField as Gf;

        match field {
            Gf::Duration => util::percentile(&self.dur, pctile),
            Gf::CpuS => util::percentile(&self.cpu_s, pctile),
            Gf::ResponseBytes => util::percentile_int(&self.response_bytes, pctile),
            Gf::DiskRead => util::percentile_int(&self.disk_read, pctile),
            Gf::DiskWrite => util::percentile_int(&self.disk_write, pctile),
            Gf::Rss => util::percentile_int(&self.rss_kb, pctile),
            _ => unreachable!("unable to take percentile of scalar field"),
        }
    }

    fn sum_field(&self, field: GitalyField) -> f64 {
        use GitalyField as Gf;

        match field {
            Gf::Duration => self.dur.iter().sum(),
            Gf::CpuS => self.cpu_s.iter().sum(),
            // MiB
            Gf::ResponseBytes => self
                .response_bytes
                .iter()
                .map(|&bytes| bytes as f64 / 1024.0 / 1024.0)
                .sum(),
            Gf::DiskRead => self.disk_read.iter().sum::<u64>() as f64,
            Gf::DiskWrite => self.disk_write.iter().sum::<u64>() as f64,
            // MiB
            Gf::Rss => self.rss_kb.iter().map(|&bytes| bytes as f64 / 1024.0).sum(),
            _ => unreachable!("unable to sum scalar field"),
        }
    }

    fn failure_percentage(&self) -> f64 {
        (self.fails as f64 / self.count as f64) * 100.0
    }

    fn sort(&mut self) {
        self.dur.sort_unstable_by(|x, y| util::cmp(x, y));
        self.cpu_s.sort_unstable_by(|x, y| util::cmp(x, y));
        self.response_bytes.sort_unstable();
        self.disk_read.sort_unstable();
        self.disk_write.sort_unstable();
        self.rss_kb.sort_unstable();
    }

    #[inline]
    fn len(&self) -> usize {
        self.count
    }
}
