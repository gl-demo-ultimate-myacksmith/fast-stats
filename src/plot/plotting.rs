use super::{GitalyPlot, PlotStats, RailsPlot, ShellPlot};
use crate::field::{GitalyField, RailsField, ShellField};
use crate::time_chunks::Interval;
use crate::time_chunks::SortedTimeMap;

use chrono::prelude::*;
use plotters::coord::{types::RangedCoordf64, Shift};
use plotters::prelude::*;
use plotters::style::RGBAColor;
use std::collections::BTreeMap;
use std::ops::Range;
use std::path::Path;

const PNG_WIDTH: u32 = 2560;
const PNG_HEIGHT: u32 = 1440;

const P99_LABEL: &str = "P99 DUR";
const P90_LABEL: &str = "P90 DUR";
const MEDIAN_LABEL: &str = "MEDIAN DUR";

type Cart2d = Cartesian2d<RangedDateTime<DateTime<FixedOffset>>, RangedCoordf64>;

struct PlotArgs<'a, DB: DrawingBackend, T: PlotStats> {
    area: &'a DrawingArea<DB, Shift>,
    time_map: &'a SortedTimeMap<T>,
    time_range: Range<DateTime<FixedOffset>>,
    title: &'a str,
    y_desc: &'a str,
    x_label_ct: usize,
}

pub fn plot_rails(
    mut time_map: SortedTimeMap<RailsPlot>,
    output_path: &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    time_map.sort_stats();

    let time_range = time_map.start_time..time_map.end_time;

    let root = BitMapBackend::new(&output_path, (PNG_WIDTH, PNG_HEIGHT)).into_drawing_area();
    root.fill(&WHITE)?;

    let main_split = root.split_evenly((1, 2));
    let right_area = &main_split[1];

    let (main_area, bottom_left_area) =
        main_split[0].split_vertically(PNG_HEIGHT - (PNG_HEIGHT / 3));

    draw_pctl_chart(
        PlotArgs {
            area: &main_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Request Duration",
            y_desc: "Duration (sec)",
            x_label_ct: 8,
        },
        RailsField::Duration,
        full_ts(),
    )?;

    let bottom_left_split = bottom_left_area.split_evenly((1, 2));
    let rate_area = &bottom_left_split[0];
    let queue_area = &bottom_left_split[1];

    let other_areas = right_area.split_evenly((3, 2));

    let db_area = &other_areas[0];
    let redis_area = &other_areas[1];
    let gitaly_area = &other_areas[2];
    let rugged_area = &other_areas[3];
    let cpu_area = &other_areas[4];
    let fail_area = &other_areas[5];

    draw_rps_chart(
        PlotArgs {
            area: rate_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Request Rate",
            y_desc: "Requests per Second",
            x_label_ct: 5,
        },
        time_map.interval,
        basic_ts(),
    )?;

    draw_pctl_chart(
        PlotArgs {
            area: queue_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Queue Duration",
            y_desc: "Duration (sec)",
            x_label_ct: 5,
        },
        RailsField::Queue,
        basic_ts(),
    )?;

    draw_pctl_chart(
        PlotArgs {
            area: db_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "DB Duration",
            y_desc: "Duration (sec)",
            x_label_ct: 5,
        },
        RailsField::Db,
        basic_ts(),
    )?;

    draw_pctl_chart(
        PlotArgs {
            area: redis_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Redis Duration",
            y_desc: "Duration (sec)",
            x_label_ct: 5,
        },
        RailsField::Redis,
        basic_ts(),
    )?;

    draw_pctl_chart(
        PlotArgs {
            area: gitaly_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Gitaly Duration",
            y_desc: "Duration (sec)",
            x_label_ct: 5,
        },
        RailsField::Gitaly,
        basic_ts(),
    )?;

    draw_pctl_chart(
        PlotArgs {
            area: rugged_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Rugged Duration",
            y_desc: "Duration (sec)",
            x_label_ct: 5,
        },
        RailsField::Rugged,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: cpu_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "CPU Time",
            y_desc: "CPU Seconds",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.sum_field(RailsField::CpuS)),
        BLACK,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: fail_area,
            time_map: &time_map,
            time_range,
            title: "Failure Rate",
            y_desc: "% Failure",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.failure_percentage()),
        RED,
        basic_ts(),
    )?;

    Ok(())
}

pub fn plot_gitaly(
    mut time_map: SortedTimeMap<GitalyPlot>,
    output_path: &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    time_map.sort_stats();

    let time_range = time_map.start_time..time_map.end_time;

    let root = BitMapBackend::new(&output_path, (PNG_WIDTH, PNG_HEIGHT)).into_drawing_area();
    root.fill(&WHITE)?;

    let main_split = root.split_evenly((1, 2));
    let right_area = &main_split[1];

    let (main_area, rate_area) = main_split[0].split_vertically(PNG_HEIGHT - (PNG_HEIGHT / 3));

    draw_pctl_chart(
        PlotArgs {
            area: &main_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Request Duration",
            y_desc: "Duration (sec)",
            x_label_ct: 8,
        },
        GitalyField::Duration,
        full_ts(),
    )?;

    let other_areas = right_area.split_evenly((3, 2));

    let cpu_area = &other_areas[0];
    let bytes_area = &other_areas[1];
    let read_area = &other_areas[2];
    let write_area = &other_areas[3];
    let rss_area = &other_areas[4];
    let fail_area = &other_areas[5];

    draw_rps_chart(
        PlotArgs {
            area: &rate_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Request Rate",
            y_desc: "Requests per Second",
            x_label_ct: 5,
        },
        time_map.interval,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: cpu_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "CPU Seconds",
            y_desc: "CPU Seconds",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.sum_field(GitalyField::CpuS)),
        BLACK,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: bytes_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Upload Pack Tx",
            y_desc: "MiB Sent",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.sum_field(GitalyField::ResponseBytes)),
        BLACK,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: read_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Disk Read",
            y_desc: "Read Operations",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.sum_field(GitalyField::DiskRead)),
        BLACK,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: write_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Disk Write",
            y_desc: "Write Operations",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.sum_field(GitalyField::DiskWrite)),
        BLACK,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: rss_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Git RSS",
            y_desc: "MiB",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.sum_field(GitalyField::Rss)),
        BLACK,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: fail_area,
            time_map: &time_map,
            time_range,
            title: "Failure Rate",
            y_desc: "% Failure",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.failure_percentage()),
        RED,
        basic_ts(),
    )?;

    Ok(())
}

pub fn plot_shell(
    mut time_map: SortedTimeMap<ShellPlot>,
    output_path: &Path,
) -> Result<(), Box<dyn std::error::Error>> {
    time_map.sort_stats();

    let time_range = time_map.start_time..time_map.end_time;

    let root = BitMapBackend::new(&output_path, (PNG_WIDTH, PNG_HEIGHT)).into_drawing_area();
    root.fill(&WHITE)?;

    let main_split = root.split_evenly((1, 2));
    let right_area = &main_split[1];

    let (rate_area, fail_area) = right_area.split_vertically(PNG_HEIGHT / 2);

    draw_pctl_chart(
        PlotArgs {
            area: &main_split[0],
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Request Duration",
            y_desc: "Duration (sec)",
            x_label_ct: 8,
        },
        ShellField::Duration,
        full_ts(),
    )?;

    draw_rps_chart(
        PlotArgs {
            area: &rate_area,
            time_map: &time_map,
            time_range: time_range.clone(),
            title: "Request Rate",
            y_desc: "Requests per Second",
            x_label_ct: 5,
        },
        time_map.interval,
        basic_ts(),
    )?;

    draw_sum_chart(
        PlotArgs {
            area: &fail_area,
            time_map: &time_map,
            time_range,
            title: "Failure Rate",
            y_desc: "% Failure",
            x_label_ct: 5,
        },
        |(ts, vals)| (*ts, vals.failure_percentage()),
        RED,
        basic_ts(),
    )?;

    Ok(())
}

fn draw_pctl_chart<DB, T>(
    args: PlotArgs<DB, T>,
    field: T::Field,
    time_fmt: impl Fn(&DateTime<FixedOffset>) -> String,
) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>>
where
    DB: DrawingBackend,
    T: PlotStats,
{
    let median = percentiles(50, field, &args.time_map.data);
    let p90 = percentiles(90, field, &args.time_map.data);
    let p99 = percentiles(99, field, &args.time_map.data);

    let y_range = range(p99.values());

    let mut chart = build_chart(args, y_range, time_fmt)?;

    chart
        .draw_series(AreaSeries::new(p99.into_iter(), 0.0, colors()[0]))?
        .label(P99_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], colors()[0]));

    chart
        .draw_series(AreaSeries::new(p90.into_iter(), 0.0, colors()[1]))?
        .label(P90_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], colors()[1]));

    chart
        .draw_series(AreaSeries::new(median.into_iter(), 0.0, colors()[2]))?
        .label(MEDIAN_LABEL)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], colors()[2]));

    chart
        .configure_series_labels()
        .position(SeriesLabelPosition::UpperRight)
        .background_style(WHITE.mix(0.8))
        .border_style(BLACK)
        .draw()?;

    Ok(())
}

fn draw_rps_chart<DB, T>(
    args: PlotArgs<DB, T>,
    interval: Interval,
    time_fmt: impl Fn(&DateTime<FixedOffset>) -> String,
) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>>
where
    DB: DrawingBackend,
    T: PlotStats,
{
    let cts: BTreeMap<DateTime<FixedOffset>, f64> = args
        .time_map
        .data
        .iter()
        .map(|(idx, vals)| (*idx, vals.rps(interval)))
        .collect();
    let y_range = range(cts.values());

    let mut chart = build_chart(args, y_range, time_fmt)?;

    chart.draw_series(LineSeries::new(cts.into_iter(), BLACK))?;

    Ok(())
}

fn draw_sum_chart<DB, T, F>(
    args: PlotArgs<DB, T>,
    func: F,
    line_color: RGBColor,
    time_fmt: impl Fn(&DateTime<FixedOffset>) -> String,
) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>>
where
    DB: DrawingBackend,
    T: PlotStats,
    F: FnMut((&DateTime<FixedOffset>, &T)) -> (DateTime<FixedOffset>, f64),
{
    let out_data: BTreeMap<DateTime<FixedOffset>, f64> =
        args.time_map.data.iter().map(func).collect();
    let y_range = range(out_data.values());

    let mut chart = build_chart(args, y_range, time_fmt)?;

    chart.draw_series(LineSeries::new(out_data.into_iter(), line_color))?;

    Ok(())
}

fn build_chart<DB, T>(
    args: PlotArgs<DB, T>,
    y_range: Range<f64>,
    time_fmt: impl Fn(&DateTime<FixedOffset>) -> String,
) -> Result<ChartContext<DB, Cart2d>, DrawingAreaErrorKind<DB::ErrorType>>
where
    DB: DrawingBackend,
    T: PlotStats,
{
    let mut chart = ChartBuilder::on(args.area)
        .caption(args.title, ("sans-serif", 30u32).into_font())
        .margin_left(20u32)
        .margin_right(20u32)
        .x_label_area_size(30u32)
        .set_label_area_size(LabelAreaPosition::Left, 70u32)
        .build_cartesian_2d(args.time_range, y_range)
        .unwrap();

    chart
        .configure_mesh()
        .disable_mesh()
        .y_desc(args.y_desc)
        .axis_desc_style(("sans-serif", 12).into_font())
        .x_labels(args.x_label_ct)
        .x_label_formatter(&time_fmt)
        .y_labels(10)
        .draw()
        .unwrap();

    Ok(chart)
}

fn percentiles<T: PlotStats>(
    perc: u32,
    field: T::Field,
    chunks: &BTreeMap<DateTime<FixedOffset>, T>,
) -> BTreeMap<DateTime<FixedOffset>, f64> {
    chunks
        .iter()
        .map(|(idx, vals)| (*idx, vals.percentile(field, perc)))
        .collect()
}

fn range<'a>(values: impl IntoIterator<Item = &'a f64>) -> Range<f64> {
    let max = *values
        .into_iter()
        .max_by(|x, y| x.partial_cmp(y).expect("Unexpected NaN duration"))
        .unwrap_or(&0.0);

    // Handle ranges composed entirely of zeros
    if max == 0.0 {
        return 0.0..1.0;
    }

    0.0..max
}

fn colors() -> [RGBAColor; 3] {
    [
        Palette99::pick(0).mix(0.5),
        Palette99::pick(8).mix(0.7),
        BLUE.mix(0.7),
    ]
}

fn basic_ts() -> impl Fn(&DateTime<FixedOffset>) -> String {
    |t| t.time().to_string()
}

fn full_ts() -> impl Fn(&DateTime<FixedOffset>) -> String {
    |t| t.to_rfc3339_opts(SecondsFormat::Secs, true)
}
