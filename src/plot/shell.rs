use super::PlotStats;
use crate::data::{RawArgs, RawOutput};
use crate::event::Event;
use crate::field::ShellField;
use crate::util;

#[derive(Default, Clone, PartialEq, Debug)]
pub struct ShellPlot {
    pub count: usize,
    pub dur: Vec<f64>,
    pub fails: usize,
}

impl RawOutput for ShellPlot {
    fn new(_args: RawArgs) -> ShellPlot {
        ShellPlot::default()
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        // Shell logs events with command rusage details separately from request completion.
        // Capturing both here would double-count, only increment count for events that
        // cover the end of an RPC.
        if !event.status().is_info() {
            self.count += 1;
        }
        self.dur.push(event.dur_ms() / 1000.0);
        if event.status().is_fail() {
            self.fails += 1;
        }
    }

    fn coalesce(&mut self, other: ShellPlot) {
        self.count += other.count;
        self.dur.extend(&other.dur);
        self.fails += other.fails;
    }

    fn finalize(&mut self) {}
}

impl PlotStats for ShellPlot {
    type Field = ShellField;

    fn percentile(&self, field: ShellField, pctile: u32) -> f64 {
        use ShellField as Sf;

        match field {
            Sf::Duration => util::percentile(&self.dur, pctile),
            _ => unreachable!("unable to take percentile of scalar field"),
        }
    }

    fn sum_field(&self, field: ShellField) -> f64 {
        use ShellField as Sf;

        match field {
            Sf::Duration => self.dur.iter().sum(),
            _ => unreachable!("unable to sum scalar field"),
        }
    }

    fn failure_percentage(&self) -> f64 {
        (self.fails as f64 / self.count as f64) * 100.0
    }

    fn sort(&mut self) {
        self.dur
            .sort_by(|x, y| x.partial_cmp(y).expect("failed to sort durations"));
    }

    fn len(&self) -> usize {
        self.count
    }
}
