use super::PlotStats;
use crate::data::{RawArgs, RawOutput};
use crate::event::Event;
use crate::field::RailsField;
use crate::util;

#[derive(Default, Clone, PartialEq, Debug)]
pub struct RailsPlot {
    pub dur: Vec<f64>,
    pub db: Vec<f64>,
    pub gitaly: Vec<f64>,
    pub rugged: Vec<f64>,
    pub redis: Vec<f64>,
    pub queue: Vec<f64>,
    pub cpu_s: Vec<f64>,
    pub mem_b: Vec<u64>,
    pub fails: usize,
}

impl RawOutput for RailsPlot {
    fn new(_args: RawArgs) -> RailsPlot {
        RailsPlot::default()
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        self.dur.push(event.dur_ms() / 1000.0);

        if let Some(db) = event.db() {
            self.db.push(db);
        }

        if let Some(gitaly) = event.gitaly() {
            self.gitaly.push(gitaly);
        }

        if let Some(rugged) = event.rugged() {
            self.rugged.push(rugged);
        }

        if let Some(redis) = event.redis() {
            self.redis.push(redis);
        }

        if let Some(queue) = event.queue() {
            self.queue.push(queue);
        }

        if let Some(cpu_s) = event.cpu_s() {
            self.cpu_s.push(cpu_s);
        }
        if event.status().is_fail() {
            self.fails += 1;
        }
    }

    fn coalesce(&mut self, other: RailsPlot) {
        self.dur.extend(&other.dur);
        self.db.extend(&other.db);
        self.gitaly.extend(&other.gitaly);
        self.rugged.extend(&other.rugged);
        self.redis.extend(&other.redis);
        self.queue.extend(&other.queue);
        self.cpu_s.extend(&other.cpu_s);
        self.mem_b.extend(&other.mem_b);
        self.fails += other.fails;
    }

    fn finalize(&mut self) {}
}

impl PlotStats for RailsPlot {
    type Field = RailsField;

    fn percentile(&self, field: RailsField, pctile: u32) -> f64 {
        use RailsField as Rf;

        match field {
            Rf::Duration => util::percentile(&self.dur, pctile),
            Rf::Db => util::percentile(&self.db, pctile),
            Rf::Gitaly => util::percentile(&self.gitaly, pctile),
            Rf::Rugged => util::percentile(&self.rugged, pctile),
            Rf::Redis => util::percentile(&self.redis, pctile),
            Rf::Queue => util::percentile(&self.queue, pctile),
            Rf::CpuS => util::percentile(&self.cpu_s, pctile),
            Rf::Mem => util::percentile_int(&self.mem_b, pctile),
            _ => unreachable!("unable to take percentile of scalar field"),
        }
    }

    fn sum_field(&self, field: RailsField) -> f64 {
        use RailsField as Rf;

        match field {
            Rf::Duration => self.dur.iter().sum(),
            Rf::Db => self.db.iter().sum(),
            Rf::Gitaly => self.gitaly.iter().sum(),
            Rf::Rugged => self.rugged.iter().sum(),
            Rf::Redis => self.redis.iter().sum(),
            Rf::Queue => self.queue.iter().sum(),
            Rf::CpuS => self.cpu_s.iter().sum(),
            // MiB
            Rf::Mem => self.mem_b.iter().sum::<u64>() as f64 / 1024.0 / 1024.0,
            _ => unreachable!("unable to sum scalar field"),
        }
    }

    fn failure_percentage(&self) -> f64 {
        (self.fails as f64 / self.dur.len() as f64) * 100.0
    }

    fn sort(&mut self) {
        for vals in [
            &mut self.dur,
            &mut self.db,
            &mut self.gitaly,
            &mut self.rugged,
            &mut self.redis,
            &mut self.queue,
            &mut self.cpu_s,
        ] {
            vals.sort_unstable_by(|x, y| util::cmp(x, y));
        }

        self.mem_b.sort_unstable();
    }

    fn len(&self) -> usize {
        self.dur.len()
    }
}
