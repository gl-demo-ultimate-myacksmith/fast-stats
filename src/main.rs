use cli::{Command, ErrorArgs, TopArgs};
use data::raw::RawData;
use execute::Executor;
use field::StatField;
use log_type::{LogType, LogType::*};
use stats_vec::PrintFormat;
use time_chunks::Interval;

use clap::Parser;
use std::io::{self, ErrorKind, IsTerminal, Write};
use std::path::Path;

mod bench;
mod check_type;
mod cli;
mod data;
mod error;
mod event;
mod execute;
mod field;
mod log_type;
mod parse;
mod print;
mod stats;
mod stats_vec;
mod time_chunks;
mod top;
mod util;

#[cfg(feature = "plot")]
mod plot;

pub type HashMap<K, V> = hashbrown::HashMap<K, V>;
pub type HashSet<T> = hashbrown::HashSet<T>;

#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[derive(Clone, Debug)]
pub enum Input {
    File(String),
    Stdin,
}

impl Input {
    fn file_name(&self) -> String {
        match &self {
            Input::File(file) => String::from(
                // Remove leading path if present
                Path::new(file)
                    .file_name()
                    .unwrap_or_default()
                    .to_string_lossy(),
            ),
            Input::Stdin => String::from("stdin"),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Args {
    input: Input,
    sort_by: StatField,
    search_for: Option<String>,
    print_fields: Vec<StatField>,
    print_format: PrintFormat,
    requested_log_type: Option<LogType>,
    thread_ct: u32,
    limit: Option<usize>,
    interval: Option<Interval>,
    color: bool,
    no_border: bool,
}

fn main() {
    let cli_args = cli::Cli::parse();

    let input = match cli_args.file {
        Some(ref f) if f != "-" => Input::File(f.to_owned()),
        _ => Input::Stdin,
    };

    let requested_log_type = if let Some(log_type) = cli_args.log_type.as_deref() {
        match log_type {
            "api" => Some(ApiJsonDurS),
            "api_dur_ms" => Some(ApiJson),
            "gitaly" => Some(GitalyJson),
            "gitaly_unstructured" => Some(GitalyText),
            "production" => Some(ProdJsonDurS),
            "production_dur_ms" => Some(ProdJson),
            "shell" => Some(ShellJson),
            "sidekiq" => Some(SidekiqJsonDurS),
            "sidekiq_dur_ms" => Some(SidekiqJson),
            "sidekiq_unstructured" => Some(SidekiqText),
            _ => unreachable!(),
        }
    } else {
        None
    };

    let compare_file = cli_args.compare.map(|comp| Input::File(comp.to_owned()));

    let thread_ct = match cli_args.thread_ct {
        Some(ct) => ct,
        None => num_cpus::get_physical() as u32,
    };

    let parsed_args = Args {
        input,
        sort_by: cli_args.sort_by,
        search_for: cli_args.search.map(|s| s.to_ascii_lowercase()),
        print_fields: cli_args.print_fields,
        print_format: cli_args.format,
        requested_log_type,
        thread_ct,
        limit: cli_args.limit,
        interval: cli_args.interval,
        color: io::stdout().is_terminal() || cli_args.force_color,
        no_border: bool::default(),
    };

    let executor = match Executor::new(parsed_args.clone(), io::stdout().lock()) {
        Ok(exec) => exec,
        Err(err) => {
            eprintln!("Error - {}", err);
            std::process::exit(1);
        }
    };

    #[cfg(feature = "plot")]
    {
        if let Some(Command::Plot(plot_args)) = cli_args.command {
            if let Err(e) = executor.plot(plot_args.output_file.as_deref()) {
                eprintln!("Error - {}", e);
                std::process::exit(1);
            }

            return;
        }
    }

    #[cfg(feature = "compress")]
    if let Some(Command::Compress(compress_args)) = cli_args.command {
        if let Err(e) = executor.compress(&compress_args.output_file) {
            eprintln!("Error - {}", e);
            std::process::exit(1);
        }
        return;
    }

    let result = match cli_args.command {
        Some(Command::Top(top_args)) => run_top(executor, top_args),
        Some(Command::Errors(error_args)) => run_errors(executor, error_args),
        _ => match (compare_file, cli_args.benchmark, cli_args.verbose) {
            (Some(comp), _, _) => executor.compare(comp),
            (_, Some(bench_vers), _) => executor.bench(bench_vers),
            (None, None, true) => executor.stats_verbose(),
            (None, None, false) => executor.stats(),
        },
    };

    if let Err(e) = result {
        match e.kind() {
            ErrorKind::BrokenPipe => {} //ignore errors caused by piping
            _ => {
                eprintln!("Error - {}", e);
                std::process::exit(1);
            }
        }
    }
}

fn run_top<W: Write>(mut executor: Executor<W>, top_args: TopArgs) -> io::Result<()> {
    if top_args.limit.is_some() {
        executor.args.limit = top_args.limit;
    };

    if top_args.interval.is_some() {
        executor.args.interval = top_args.interval;
    }

    executor.args.print_format = top_args.format.into();

    executor.top(top_args.sort_by, top_args.display)
}

fn run_errors<W: Write>(mut executor: Executor<W>, err_args: ErrorArgs) -> io::Result<()> {
    if executor.args.color {
        executor.args.color = err_args.force_color;
    }

    executor.args.no_border = err_args.no_border;

    executor.errors()
}
