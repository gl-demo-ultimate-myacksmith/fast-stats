use crate::field::{EventField, StatField};
use crate::stats_vec::PrintFormat;
use crate::time_chunks::Interval;
use crate::top::{TopPrintFormat, ValueFmt};

use clap::{Args, Parser, Subcommand};
use std::str::FromStr;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    /// File to be parsed. If this is not provided then stdin is used
    #[arg(global = true, value_name = "FILE_NAME")]
    pub file: Option<String>,

    /// Number of threads to spawn. Defaults to 'nproc'
    #[arg(global = true, long, short = 'j', value_parser = clap::value_parser!(u32).range(1..))]
    pub thread_ct: Option<u32>,

    /// Compare performance to GitLab.com
    #[arg(long, short, value_parser = ["14.0", "14.1", "14.2", "14.3",
        "14.4", "14.5", "14.6", "14.7", "14.8", "14.9", "14.10",
        "15.0", "15.1", "15.2", "15.3", "15.4", "15.5"],
        value_name = "BENCH_VERSION")]
    pub benchmark: Option<String>,

    /// Calculate performance changes between two files
    #[arg(long, short, value_name = "COMPARE_FILE")]
    pub compare: Option<String>,

    /// Output colored text even if writing to a pipe or file
    #[arg(long = "color-output", short = 'C')]
    pub force_color: bool,

    /// Format to print in
    #[arg(long, short, value_enum, default_value_t = PrintFormat::default())]
    pub format: PrintFormat,

    /// Split results into INTERVAL_LEN time slices. INTERVAL_LEN must be in <LEN>[h|m|s] format
    #[arg(long, short, value_parser = Interval::from_str, value_name = "INTERVAL_LEN")]
    pub interval: Option<Interval>,

    /// Number of results to display
    #[arg(long, short, value_name = "LIMIT_CT")]
    pub limit: Option<usize>,

    /// Manually specify log type of <INPUT> if log type cannot be deduced automatically
    #[arg(long = "type", short = 't', value_parser = ["api", "api_dur_ms",
        "gitaly", "gitaly_unstructured", "production", "production_dur_ms",
        "shell", "sidekiq", "sidekiq_dur_ms", "sidekiq_unstructured"])]
    pub log_type: Option<String>,

    /// List of fields to be printed
    #[arg(long, short, num_args = 0.., value_delimiter = ',', default_values_t = StatField::all_fields(), value_enum)]
    pub print_fields: Vec<StatField>,

    /// Case-insensitive search of controller/method/worker field
    #[arg(long, short = 'S', value_name = "SEARCH_FOR")]
    pub search: Option<String>,

    /// Field to sort by descending value
    #[arg(long, short, value_enum, default_value_t=StatField::default())]
    pub sort_by: StatField,

    /// Print details of p99, p95, and median events
    #[arg(long, short)]
    pub verbose: bool,

    #[command(subcommand)]
    pub command: Option<Command>,
}

#[derive(Subcommand, Debug)]
#[command(about = "Show top path/project/user by <SORT_BY>")]
pub enum Command {
    /// Show summary of errors captured in log
    Errors(ErrorArgs),
    /// Show top path/project/user by [SORT_BY]
    Top(TopArgs),
    /// Generate a PNG file with graphs of relevant metrics
    #[cfg(feature = "plot")]
    Plot(PlotArgs),
    /// Create lz4 compressed archive of bincode serialized stats_vec
    #[cfg(feature = "compress")]
    Compress(CompressArgs),
}

#[derive(Args, Debug, Clone)]
pub struct ErrorArgs {
    /// Output colored text even if writing to a pipe or file
    #[arg(long = "color-output", short = 'C')]
    pub force_color: bool,

    /// Format to print in
    #[arg(long, short, value_enum, default_value_t = TopPrintFormat::default())]
    pub format: TopPrintFormat,

    /// Don't print borders around results
    #[arg(long, short = 'b')]
    pub no_border: bool,
}

#[cfg(feature = "compress")]
#[derive(Args, Debug, Clone)]
pub struct CompressArgs {
    /// Name of file to write to
    #[arg(long = "outfile", short)]
    pub output_file: String,
}

#[cfg(feature = "plot")]
#[derive(Args, Debug, Clone)]
pub struct PlotArgs {
    /// Name of file to write to
    #[arg(long = "outfile", short)]
    pub output_file: Option<String>,
}

#[derive(Args, Debug, Clone)]
pub struct TopArgs {
    /// Show percentage, values, or both
    #[arg(long, short, value_enum, default_value_t = ValueFmt::default(), value_name = "DISPLAY_FMT")]
    pub display: ValueFmt,

    /// Format to print in
    #[arg(long, short, value_enum, default_value_t = TopPrintFormat::default())]
    pub format: TopPrintFormat,

    /// Split results into INTERVAL_LEN time slices. INTERVAL_LEN must be in <LEN>[h|m|s] format
    #[arg(long, short, value_parser = Interval::from_str, value_name = "INTERVAL_LEN")]
    pub interval: Option<Interval>,

    /// Number of results to display
    #[arg(long, short, value_name = "LIMIT_CT")]
    pub limit: Option<usize>,

    /// Field to sort by descending value
    #[arg(long, short, value_enum, default_value_t = EventField::default())]
    pub sort_by: EventField,
}
