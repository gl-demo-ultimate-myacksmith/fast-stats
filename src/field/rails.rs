use super::EventField;

use std::fmt;

#[derive(Default, Clone, Copy, Eq, Hash, PartialEq, Debug)]
pub enum RailsField {
    Count,
    Rps,
    #[default]
    Duration,
    Db,
    Redis,
    Gitaly,
    Rugged,
    Queue,
    CpuS,
    Mem,
    FailCt,
}

impl fmt::Display for RailsField {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use RailsField as Rf;

        match self {
            Rf::Count => "Count".fmt(f),
            Rf::Rps => "Requests per Second".fmt(f),
            Rf::Duration => "Duration".fmt(f),
            Rf::Db => "Database Duration".fmt(f),
            Rf::Redis => "Redis Duration".fmt(f),
            Rf::Gitaly => "Gitaly Duration".fmt(f),
            Rf::Rugged => "Rugged Duration".fmt(f),
            Rf::Queue => "Queue Duration".fmt(f),
            Rf::CpuS => "CPU Time".fmt(f),
            Rf::Mem => "Memory Allocated".fmt(f),
            Rf::FailCt => "Failure Count".fmt(f),
        }
    }
}

impl From<EventField> for RailsField {
    fn from(efield: EventField) -> Self {
        use EventField as Ef;
        use RailsField as Rf;
        match efield {
            Ef::Count => Rf::Count,
            Ef::Rps => Rf::Rps,
            Ef::Duration => Rf::Duration,
            Ef::Db => Rf::Db,
            Ef::Redis => Rf::Redis,
            Ef::Gitaly => Rf::Gitaly,
            Ef::Rugged => Rf::Rugged,
            Ef::Queue => Rf::Queue,
            Ef::CpuS => Rf::CpuS,
            Ef::Mem => Rf::Mem,
            Ef::FailCt => Rf::FailCt,
            _ => Self::default(),
        }
    }
}
