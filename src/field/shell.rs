use super::EventField;

use std::fmt;

#[derive(Default, Clone, Copy, Eq, Hash, PartialEq, Debug)]
pub enum ShellField {
    Count,
    Rps,
    #[default]
    Duration,
    FailCt,
}

impl fmt::Display for ShellField {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ShellField as Sf;

        match self {
            Sf::Count => "Count".fmt(f),
            Sf::Rps => "Requests per Second".fmt(f),
            Sf::Duration => "Duration".fmt(f),
            Sf::FailCt => "Failure Count".fmt(f),
        }
    }
}

impl From<EventField> for ShellField {
    fn from(efield: EventField) -> Self {
        use EventField as Ef;
        use ShellField as Sf;
        match efield {
            Ef::Count => Sf::Count,
            Ef::Rps => Sf::Rps,
            Ef::Duration => Sf::Duration,
            Ef::FailCt => Sf::FailCt,
            _ => Self::default(),
        }
    }
}
