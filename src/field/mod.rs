use clap::ValueEnum;
use std::fmt;

use StatField as Sf;

mod gitaly;
mod rails;
mod shell;

pub use gitaly::GitalyField;
pub use rails::RailsField;
pub use shell::ShellField;

#[derive(ValueEnum, Default, Clone, Copy, Eq, PartialEq, Debug)]
pub enum StatField {
    Count,
    Rps,
    P99,
    P95,
    Median,
    Max,
    Min,
    #[default]
    Score,
    Fail,
    StdDev,
}

impl fmt::Display for StatField {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Sf::Count => "COUNT".fmt(f),
            Sf::Rps => "RPS".fmt(f),
            Sf::P99 => "P99_ms".fmt(f),
            Sf::P95 => "P95_ms".fmt(f),
            Sf::Median => "MEDIAN_ms".fmt(f),
            Sf::Max => "MAX_ms".fmt(f),
            Sf::Min => "MIN_ms".fmt(f),
            Sf::StdDev => "STDDEV".fmt(f),
            Sf::Score => "SCORE".fmt(f),
            Sf::Fail => "%FAIL".fmt(f),
        }
    }
}

impl StatField {
    pub fn all_fields() -> &'static [StatField] {
        &[
            Sf::Count,
            Sf::Rps,
            Sf::P99,
            Sf::P95,
            Sf::Median,
            Sf::Max,
            Sf::Min,
            Sf::StdDev,
            Sf::Score,
            Sf::Fail,
        ]
    }

    #[inline]
    pub const fn len(&self, lens: &[usize]) -> usize {
        match self {
            Sf::Count => lens[1],
            Sf::Rps => lens[2],
            Sf::P99 => lens[3],
            Sf::P95 => lens[4],
            Sf::Median => lens[5],
            Sf::Max => lens[6],
            Sf::Min => lens[7],
            Sf::StdDev => lens[8],
            Sf::Score => lens[9],
            Sf::Fail => lens[10],
        }
    }

    #[inline]
    pub const fn precision(&self) -> usize {
        match self {
            Sf::Count => 0,
            Sf::Rps | Sf::Fail => 2,
            _ => 1,
        }
    }
}

impl From<&str> for StatField {
    fn from(s: &str) -> StatField {
        match s {
            "count" => Sf::Count,
            "rps" => Sf::Rps,
            "max" => Sf::Max,
            "median" => Sf::Median,
            "min" => Sf::Min,
            "p99" => Sf::P99,
            "p95" => Sf::P95,
            "stddev" => Sf::StdDev,
            "fail" => Sf::Fail,
            "score" => Sf::Score,
            _ => unreachable!(),
        }
    }
}

#[derive(ValueEnum, Default, Clone, Copy, Eq, Hash, PartialEq, Debug)]
pub enum EventField {
    Count,
    Rps,
    #[default]
    Duration,
    Db,
    Redis,
    Gitaly,
    Rugged,
    Queue,
    CpuS,
    ResponseBytes,
    DiskRead,
    DiskWrite,
    Mem,
    FailCt,
}

impl fmt::Display for EventField {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use EventField as Ef;

        match self {
            Ef::Count => "Count".fmt(f),
            Ef::Rps => "Requests per Second".fmt(f),
            Ef::Duration => "Duration".fmt(f),
            Ef::Db => "Database Duration".fmt(f),
            Ef::Redis => "Redis Duration".fmt(f),
            Ef::Gitaly => "Gitaly Duration".fmt(f),
            Ef::Rugged => "Rugged Duration".fmt(f),
            Ef::Queue => "Queue Duration".fmt(f),
            Ef::CpuS => "CPU Time".fmt(f),
            Ef::ResponseBytes => "Response Bytes".fmt(f),
            Ef::DiskRead => "Disk Reads".fmt(f),
            Ef::DiskWrite => "Disk Writes".fmt(f),
            Ef::Mem => "Memory Usage".fmt(f),
            Ef::FailCt => "Failure Count".fmt(f),
        }
    }
}

impl From<&str> for EventField {
    fn from(s: &str) -> EventField {
        use EventField as Ef;

        match s {
            "count" => Ef::Count,
            "rps" => Ef::Rps,
            "duration" => Ef::Duration,
            "db" => Ef::Db,
            "redis" => Ef::Redis,
            "gitaly" => Ef::Gitaly,
            "rugged" => Ef::Rugged,
            "queue" => Ef::Queue,
            "cpu" => Ef::CpuS,
            "mem" => Ef::Mem,
            "response_bytes" => Ef::ResponseBytes,
            "disk_r" => Ef::DiskRead,
            "disk_w" => Ef::DiskWrite,
            "fail" => Ef::FailCt,
            _ => Self::default(),
        }
    }
}
