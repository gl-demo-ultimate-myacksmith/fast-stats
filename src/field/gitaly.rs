use super::EventField;

use std::fmt;

#[derive(Default, Clone, Copy, Eq, Hash, PartialEq, Debug)]
pub enum GitalyField {
    Count,
    Rps,
    #[default]
    Duration,
    CpuS,
    ResponseBytes,
    DiskRead,
    DiskWrite,
    Rss,
    FailCt,
}

impl fmt::Display for GitalyField {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use GitalyField as Gf;

        match self {
            Gf::Count => "Count".fmt(f),
            Gf::Rps => "Requests per Second".fmt(f),
            Gf::Duration => "Duration".fmt(f),
            Gf::CpuS => "CPU Time".fmt(f),
            Gf::ResponseBytes => "Response Bytes".fmt(f),
            Gf::DiskRead => "Disk Reads".fmt(f),
            Gf::DiskWrite => "Disk Writes".fmt(f),
            Gf::Rss => "Git RSS".fmt(f),
            Gf::FailCt => "Failure Count".fmt(f),
        }
    }
}

impl From<EventField> for GitalyField {
    fn from(efield: EventField) -> Self {
        use EventField as Ef;
        use GitalyField as Gf;
        match efield {
            Ef::Count => Gf::Count,
            Ef::Rps => Gf::Rps,
            Ef::Duration => Gf::Duration,
            Ef::CpuS => Gf::CpuS,
            Ef::ResponseBytes => Gf::ResponseBytes,
            Ef::DiskRead => Gf::DiskRead,
            Ef::DiskWrite => Gf::DiskWrite,
            Ef::Mem => Gf::Rss,
            Ef::FailCt => Gf::FailCt,
            _ => Self::default(),
        }
    }
}
