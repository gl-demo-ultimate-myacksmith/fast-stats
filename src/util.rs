pub const BLUE: &str = "\x1b[34m";
pub const BRIGHT_GREEN: &str = "\x1b[32;1m";
pub const CYAN: &str = "\x1b[36m";
pub const GREEN: &str = "\x1b[32m";
pub const MAGENTA: &str = "\x1b[35m";
pub const RED: &str = "\x1b[31;1m";
pub const RESET: &str = "\x1b[0m";
pub const YELLOW: &str = "\x1b[33m";
pub const UNDERLINE: &str = "\x1b[4m";

pub fn cmp<T: PartialOrd>(x: T, y: T) -> std::cmp::Ordering {
    x.partial_cmp(&y).expect("Invalid comparison when sorting")
}

pub fn percentile(vals: &[f64], perc: u32) -> f64 {
    if vals.is_empty() {
        return 0.0;
    } else if vals.len() == 1 {
        return *vals.first().unwrap_or(&0.0);
    } else if perc == 100 {
        return *vals.last().unwrap_or(&0.0);
    }

    let idx = f64::from(perc) / 100.0 * (vals.len() - 1) as f64;

    let lower = vals.get(idx.floor() as usize).unwrap_or(&0.0);
    let upper = vals.get(idx.ceil() as usize).unwrap_or(&0.0);

    lower + (upper - lower) * (idx - idx.floor())
}

pub fn percentile_int(vals: &[u64], perc: u32) -> f64 {
    if vals.is_empty() {
        return 0.0;
    } else if vals.len() == 1 {
        return *vals.first().unwrap_or(&0) as f64;
    } else if perc == 100 {
        return *vals.last().unwrap_or(&0) as f64;
    }

    let idx = f64::from(perc) / 100.0 * (vals.len() - 1) as f64;

    let lower = vals.get(idx.floor() as usize).unwrap_or(&0);
    let upper = vals.get(idx.ceil() as usize).unwrap_or(&0);

    (lower + (upper - lower)) as f64 * (idx - idx.floor())
}

pub fn percentile_obj<T>(vals: &[T], perc: u32) -> (Option<&T>, u32) {
    if vals.is_empty() {
        return (None, 0);
    } else if vals.len() == 1 {
        return (vals.first(), 100);
    } else if perc == 100 {
        return (vals.last(), 100);
    }

    let idx = (f64::from(perc) / 100.0 * (vals.len() - 1) as f64).round() as usize;
    let actual_pctl = (idx as f64 / (vals.len() - 1) as f64 * 100.0).round() as u32;
    (vals.get(idx), actual_pctl)
}

pub fn stddev(vals: &[f64]) -> f64 {
    if vals.is_empty() {
        return 0.0;
    }

    let sum: f64 = vals.iter().sum();
    let mean = sum / vals.len() as f64;

    let sd_sq = vals.iter().fold(0.0, |acc, x| {
        let diff = x - mean;
        acc + diff * diff
    }) / vals.len() as f64;

    sd_sq.sqrt()
}
