use crate::data::{BasicData, RawArgs, RawOutput};
use crate::event::basic::BasicEvent;
use crate::event::*;
use crate::execute::DynBatcher;
use crate::log_type::LogType;
use batcher::LineBatch;

use crossbeam_channel::bounded as bounded_channel;
use crossbeam_channel::{Receiver, Sender};
use rayon::prelude::*;
use std::marker::PhantomData;
use std::thread::{self, JoinHandle};

pub const BATCH_SIZE: usize = 50;
const QUEUE_DEPTH: usize = 128;

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct ParseArgs {
    pub search_for: Option<String>,
    pub thread_ct: u32,
    pub line_cache: Vec<LineBatch>,
}

impl ParseArgs {
    pub fn new(
        search_for: Option<String>,
        thread_ct: u32,
        line_cache: Vec<LineBatch>,
    ) -> ParseArgs {
        ParseArgs {
            search_for,
            thread_ct,
            line_cache,
        }
    }
}

/// A trait for the ability to parse logs. It is generic
/// over output type O, allowing a parser to insert events
/// into several container types, e.g. RawDataMap or UnsortedTimeMap.
///
/// Associated type ParseFn is a function that returns a function
/// pointer that will be used to parse the raw log.
pub trait Parser<O>: Clone + Send + Sync {
    type ParseFn;

    /// Parse a log line and insert the object into map.
    fn parse_and_insert(&self, map: &mut O, line: &str);
}

/// Parses any log type into BasicEvent objects for stats
/// on duration and failure rate only.
#[derive(Clone, Debug)]
pub struct BasicParser<O> {
    parse_fn: fn(&str) -> Option<BasicEvent>,
    raw_args: RawArgs,
    _marker: PhantomData<O>,
}

impl<O: BasicData + Clone> Parser<O> for BasicParser<O> {
    type ParseFn = fn(&str) -> Option<BasicEvent>;

    fn parse_and_insert(&self, map: &mut O, line: &str) {
        if let Some(basic) = (self.parse_fn)(line) {
            map.insert_basic(basic);
        }
    }
}

impl<O: BasicData + Clone> BasicParser<O> {
    pub fn new(raw_args: RawArgs) -> Self {
        Self {
            parse_fn: BasicEvent::parse_fn(raw_args.log_t),
            raw_args,
            _marker: PhantomData,
        }
    }

    pub fn parse(self, batcher: DynBatcher, args: ParseArgs) -> O
    where
        O: BasicData + 'static,
    {
        if args.thread_ct > 1 {
            parse_fn_mt(self.raw_args, batcher, args, self)
        } else {
            parse_fn_st(self.raw_args, batcher, args, self)
        }
    }
}

fn parse_fn_st<O, P>(out_args: RawArgs, mut batcher: DynBatcher, args: ParseArgs, parser: P) -> O
where
    O: RawOutput + 'static,
    P: Parser<O>,
{
    let mut data_map = O::new(out_args);

    for batch in args.line_cache {
        for line in &batch {
            parser.parse_and_insert(&mut data_map, line)
        }
    }

    for batch in batcher.iter() {
        let batch = match batch {
            Ok(b) => b,
            Err(e) => panic!("Failed to read: {e}"),
        };

        for line in &batch {
            parser.parse_and_insert(&mut data_map, line)
        }
    }

    data_map.finalize();

    data_map
}

fn parse_fn_mt<O, P>(out_args: RawArgs, mut batcher: DynBatcher, args: ParseArgs, parser: P) -> O
where
    O: RawOutput + 'static,
    P: Parser<O> + 'static,
{
    let (tx, rx): (Sender<LineBatch>, Receiver<_>) = bounded_channel(QUEUE_DEPTH);

    let mut handles = Vec::with_capacity(args.thread_ct as usize);

    for _ in 0..args.thread_ct {
        let rx = rx.clone();
        let parser = parser.clone();

        let handle = thread::spawn(move || {
            let mut local_map = O::new(out_args);

            rx.iter().for_each(|batch| {
                for line in &batch {
                    parser.parse_and_insert(&mut local_map, line)
                }
            });

            local_map
        });

        handles.push(handle);
    }

    for batch in args.line_cache {
        tx.send(batch).expect("Failed to send to input channel");
    }

    for batch in batcher.iter() {
        let b = match batch {
            Ok(b) => b,
            Err(e) => panic!("Failed to read: {}", e),
        };
        tx.send(b).expect("Failed to send to input channel");
    }

    drop(tx);

    handles
        .into_par_iter()
        .filter_map(|h| h.join().ok())
        .reduce(
            || O::new(out_args),
            |mut map, thd_map| {
                map.coalesce(thd_map);
                map
            },
        )
}

pub fn parse<O>(out_args: RawArgs, batcher: DynBatcher, args: ParseArgs) -> O
where
    O: RawOutput + 'static,
{
    if args.thread_ct <= 1 {
        parse_st::<O>(out_args, batcher, args)
    } else {
        parse_mt::<O>(out_args, batcher, args)
    }
}

fn parse_st<O>(out_args: RawArgs, mut batcher: DynBatcher, args: ParseArgs) -> O
where
    O: RawOutput,
{
    let mut data_map = O::new(out_args);

    for batch in args.line_cache {
        for line in &batch {
            parse_line(out_args, line, &mut data_map);
        }
    }

    for batch in batcher.iter() {
        let batch = match batch {
            Ok(b) => b,
            Err(e) => panic!("Failed to read: {e}"),
        };

        for line in &batch {
            parse_line(out_args, line, &mut data_map);
        }
    }

    data_map.finalize();

    data_map
}

fn parse_mt<O>(out_args: RawArgs, mut batcher: DynBatcher, args: ParseArgs) -> O
where
    O: RawOutput + 'static,
{
    let (tx, rx): (Sender<LineBatch>, Receiver<_>) = bounded_channel(QUEUE_DEPTH);
    let handles = spawn_threads::<O>(out_args, args.thread_ct, rx);

    for batch in args.line_cache {
        tx.send(batch).expect("Failed to send to input channel");
    }

    for batch in batcher.iter() {
        let b = match batch {
            Ok(b) => b,
            Err(e) => panic!("Failed to read: {}", e),
        };
        tx.send(b).expect("Failed to send to input channel");
    }

    drop(tx);

    handles
        .into_par_iter()
        .filter_map(|h| h.join().ok())
        .reduce(
            || O::new(out_args),
            |mut map, thd_map| {
                map.coalesce(thd_map);
                map
            },
        )
}

fn spawn_threads<O>(out_args: RawArgs, ct: u32, rx: Receiver<LineBatch>) -> Vec<JoinHandle<O>>
where
    O: RawOutput + 'static,
{
    (0..ct - 1)
        .map(|_| {
            let rx_clone = rx.clone();
            thread::spawn(move || receive_lines::<O>(out_args, rx_clone))
        })
        .collect()
}

fn receive_lines<O>(out_args: RawArgs, rx_input: Receiver<LineBatch>) -> O
where
    O: RawOutput,
{
    let mut local_map = O::new(out_args);

    rx_input.iter().for_each(|batch| {
        for line in &batch {
            parse_line(out_args, line, &mut local_map);
        }
    });

    local_map
}

fn parse_line<O: RawOutput>(out_args: RawArgs, s: &str, map: &mut O) {
    use LogType as Lt;

    match out_args.log_t {
        Lt::ApiJson => {
            if let Some(event) = ApiJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ApiJsonDetail => {
            if let Some(event) = ApiJsonEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ApiJsonError => {
            if let Some(event) = ApiJsonEventError::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ApiJsonDurS => {
            if let Some(event) = ApiJsonDurSEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ApiJsonDurSDetail => {
            if let Some(event) = ApiJsonDurSEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ApiJsonDurSError => {
            if let Some(event) = ApiJsonDurSEventError::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::GitalyText => {
            if let Some(event) = GitalyEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::GitalyJson => {
            if let Some(event) = GitalyJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::GitalyJsonDetail => {
            if let Some(event) = GitalyJsonEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::GitalyJsonError => {
            if let Some(event) = GitalyJsonEventError::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ProdJson => {
            if let Some(event) = ProdJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ProdJsonDetail => {
            if let Some(event) = ProdJsonEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ProdJsonError => {
            if let Some(event) = ProdJsonEventError::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ProdJsonDurS => {
            if let Some(event) = ProdJsonDurSEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ProdJsonDurSDetail => {
            if let Some(event) = ProdJsonDurSEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ProdJsonDurSError => {
            if let Some(event) = ProdJsonDurSEventError::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::ShellJson => {
            if let Some(event) = ShellJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::SidekiqText => {
            if let Some(event) = SidekiqEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::SidekiqJson => {
            if let Some(event) = SidekiqJsonEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::SidekiqJsonDetail => {
            if let Some(event) = SidekiqJsonEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::SidekiqJsonError => {
            if let Some(event) = SidekiqJsonEventError::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::SidekiqJsonDurS => {
            if let Some(event) = SidekiqJsonDurSEvent::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::SidekiqJsonDurSDetail => {
            if let Some(event) = SidekiqJsonDurSEventDetail::parse(s) {
                map.insert_event(event);
            }
        }
        Lt::SidekiqJsonDurSError => {
            if let Some(event) = SidekiqJsonDurSEventError::parse(s) {
                map.insert_event(event);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::data::raw::RawDataMap;
    use crate::field::StatField;
    use crate::stats::*;

    use std::fs::File;

    #[test]
    fn parser_calc_st() {
        let input_file = File::open("tests/data/api.log").unwrap();
        let batcher = DynBatcher::new(50, Box::new(input_file));
        let args = ParseArgs::new(None, 1, Vec::new());

        let stats_v = BasicParser::<RawDataMap>::new(RawArgs::new(LogType::ApiJsonDurS, None))
            .parse(batcher, args)
            .into_stats_vec()
            .sort_by(StatField::Count);

        assert_eq!(stats_v.stats_t, StatsType::Api);
        assert_eq!(stats_v.stats.len(), 2);
        assert_eq!(
            stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec!["/api/:version/projects", "/api/:version/users"]
        );
        assert_eq!(
            stats_v.stats.iter().map(|s| s.count).collect::<Vec<_>>(),
            vec![6, 1],
        );
        assert_eq!(
            stats_v.stats.iter().map(|s| s.max).collect::<Vec<_>>(),
            vec![1560.0, 550.0],
        );
        assert_eq!(
            stats_v
                .stats
                .iter()
                .map(|s| s.perc_failed)
                .collect::<Vec<_>>(),
            vec![0.0, 0.0],
        );
    }

    #[test]
    fn parser_calc_mt() {
        let input_file = File::open("tests/data/api.log").unwrap();
        let batcher = DynBatcher::new(50, Box::new(input_file));
        let args = ParseArgs::new(None, 4, Vec::new());

        let stats_v = BasicParser::<RawDataMap>::new(RawArgs::new(LogType::ApiJsonDurS, None))
            .parse(batcher, args)
            .into_stats_vec()
            .sort_by(StatField::Count);

        assert_eq!(stats_v.stats_t, StatsType::Api);
        assert_eq!(stats_v.stats.len(), 2);
        assert_eq!(
            stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec!["/api/:version/projects", "/api/:version/users"]
        );
        assert_eq!(
            stats_v.stats.iter().map(|s| s.count).collect::<Vec<_>>(),
            vec![6, 1],
        );
        assert_eq!(
            stats_v.stats.iter().map(|s| s.max).collect::<Vec<_>>(),
            vec![1560.0, 550.0],
        );
        assert_eq!(
            stats_v
                .stats
                .iter()
                .map(|s| s.perc_failed)
                .collect::<Vec<_>>(),
            vec![0.0, 0.0],
        );
    }

    #[test]
    fn parser_calc_st_eq_calc_mt() {
        let st_input_file = File::open("tests/data/api.log").unwrap();
        let st_batcher = DynBatcher::new(50, Box::new(st_input_file));
        let st_args = ParseArgs::new(None, 1, Vec::new());

        let st_stats_v = BasicParser::<RawDataMap>::new(RawArgs::new(LogType::ApiJsonDurS, None))
            .parse(st_batcher, st_args)
            .into_stats_vec()
            .sort_by(StatField::Count);

        let mt_input_file = File::open("tests/data/api.log").unwrap();
        let mt_batcher = DynBatcher::new(50, Box::new(mt_input_file));
        let mt_args = ParseArgs::new(None, 4, Vec::new());

        let mt_stats_v = BasicParser::<RawDataMap>::new(RawArgs::new(LogType::ApiJsonDurS, None))
            .parse(mt_batcher, mt_args)
            .into_stats_vec()
            .sort_by(StatField::Count);

        assert_eq!(st_stats_v, mt_stats_v);
    }
}
