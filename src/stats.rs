use crate::field::StatField;
use crate::util;
use crate::RawData;

use chrono::Duration;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::fmt;

#[derive(Clone, Copy, Eq, PartialEq, Serialize, Deserialize, Debug)]
pub enum StatsType {
    Api,
    Gitaly,
    Prod,
    Shell,
    Sidekiq,
}

#[derive(Clone, Copy, Debug)]
pub enum PrimField {
    Blank,
    Print,
}

const BLANK_PRIM: &str = "";

impl StatsType {
    pub fn header_title(self) -> &'static str {
        use StatsType as St;
        match &self {
            St::Api => "ROUTE",
            St::Gitaly => "METHOD",
            St::Prod => "CONTROLLER",
            St::Shell => "ENDPOINT",
            St::Sidekiq => "WORKER",
        }
    }
}

impl fmt::Display for StatsType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use StatsType as St;
        match self {
            St::Api => "api_json".fmt(f),
            St::Gitaly => "gitaly".fmt(f),
            St::Prod => "production_json".fmt(f),
            St::Shell => "gitlab_shell".fmt(f),
            St::Sidekiq => "sidekiq".fmt(f),
        }
    }
}

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct Stats {
    pub prim_field: String,
    pub count: usize,
    pub rps: f64,
    pub p99: f64,
    pub p95: f64,
    pub median: f64,
    pub max: f64,
    pub min: f64,
    pub score: f64,
    pub perc_failed: f64,
    pub stddev: f64,
}

impl Stats {
    pub fn from_raw_data(prim_field: String, mut raw_data: RawData, dur: Duration) -> Stats {
        raw_data.data.par_sort_unstable_by(|x, y| {
            (x).partial_cmp(y)
                .expect("Invalid comparison when sorting durations")
        });

        let count = raw_data.data.len();
        let rps = if dur.num_seconds() == 0 {
            f64::NAN
        } else {
            count as f64 / dur.num_seconds() as f64
        };
        let p99 = util::percentile(&raw_data.data, 99);
        let p95 = util::percentile(&raw_data.data, 95);
        let median = util::percentile(&raw_data.data, 50);
        let max = *raw_data.data.last().unwrap_or(&0.0);
        let min = *raw_data.data.first().unwrap_or(&0.0);
        let score = p99 * count as f64;
        let perc_failed = (raw_data.fails as f64 / count as f64) * 100.0;
        let stddev = util::stddev(&raw_data.data);

        Stats {
            prim_field,
            count,
            rps,
            p99,
            p95,
            median,
            max,
            min,
            score,
            perc_failed,
            stddev,
        }
    }

    pub fn csv(&self, fields: &[StatField]) -> String {
        let mut output = self.prim_field.to_string();

        for field in fields {
            output += &format!(
                ",{:.precision$}",
                self.val(field),
                precision = field.precision()
            );
        }

        output
    }

    pub fn md(&self, fields: &[StatField]) -> String {
        let mut output = format!("|{}", self.prim_field);

        for field in fields {
            output += &format!(
                "|{:.precision$}",
                self.val(field),
                precision = field.precision()
            );
        }

        format!("{}|", output)
    }

    pub fn text(&self, print_prim: PrimField, fields: &[StatField], lens: &[usize]) -> String {
        let mut output = format!("{:<len$}", self.prim_str(print_prim), len = lens[0]);

        for field in fields {
            output += &format!(
                "  {:width$.precision$}",
                self.val(field),
                width = field.len(lens),
                precision = field.precision()
            );
        }

        output
    }

    /// Sort `Stats` in descending order by the field requested. In cases where
    /// the value matches, sort in ascending order by the action name. The latter
    /// is always unique and ensures stable ordering even when multi-threaded.
    pub fn sort_by_field(&self, other: &Stats, sort_by: StatField) -> Ordering {
        use StatField as Sf;

        match sort_by {
            Sf::Count => {
                let ord = other.count.cmp(&self.count);
                if let Ordering::Equal = ord {
                    self.prim_field.cmp(&other.prim_field)
                } else {
                    ord
                }
            }
            Sf::Rps => Self::sort(other.rps, &other.prim_field, self.rps, &self.prim_field),
            Sf::Fail => Self::sort(
                other.perc_failed,
                &other.prim_field,
                self.perc_failed,
                &self.prim_field,
            ),
            Sf::Max => Self::sort(other.max, &other.prim_field, self.max, &self.prim_field),
            Sf::Median => Self::sort(
                other.median,
                &other.prim_field,
                self.median,
                &self.prim_field,
            ),
            Sf::Min => Self::sort(other.min, &other.prim_field, self.min, &self.prim_field),
            Sf::P99 => Self::sort(other.p99, &other.prim_field, self.p99, &self.prim_field),
            Sf::P95 => Self::sort(other.p95, &other.prim_field, self.p95, &self.prim_field),
            Sf::Score => Self::sort(other.score, &other.prim_field, self.score, &self.prim_field),
            Sf::StdDev => Self::sort(
                other.stddev,
                &other.prim_field,
                self.stddev,
                &self.prim_field,
            ),
        }
    }

    #[inline]
    fn sort(x_val: f64, x_name: &str, y_val: f64, y_name: &str) -> Ordering {
        let p_ord = util::cmp(x_val, y_val);

        if let Ordering::Equal = p_ord {
            y_name.cmp(x_name)
        } else {
            p_ord
        }
    }

    #[inline]
    fn val(&self, field: &StatField) -> f64 {
        use StatField as Sf;
        match field {
            Sf::Count => self.count as f64,
            Sf::Rps => self.rps,
            Sf::P99 => self.p99,
            Sf::P95 => self.p95,
            Sf::Median => self.median,
            Sf::Max => self.max,
            Sf::Min => self.min,
            Sf::Score => self.score,
            Sf::Fail => self.perc_failed,
            Sf::StdDev => self.stddev,
        }
    }

    fn prim_str(&self, prim: PrimField) -> &str {
        use PrimField as Pf;
        match prim {
            Pf::Blank => BLANK_PRIM,
            Pf::Print => &self.prim_field,
        }
    }
}
