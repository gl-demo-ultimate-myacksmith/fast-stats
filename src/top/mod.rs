use crate::data::RawOutput;
use crate::event::Event;
use crate::stats_vec::PrintFormat;
use crate::HashSet;
pub use gitaly::GitalySummary;
pub use rails::RailsSummary;
pub use shell::ShellSummary;
use stats_map::TopStatsMap;

use clap::ValueEnum;
use rayon::prelude::*;
use serde::Serialize;
use std::cmp::Ordering;
use std::fmt::Display;
use std::hash::Hash;
use std::io::{self, Write};

mod fmt;
mod gitaly;
mod rails;
mod shell;
mod stats_map;

pub const DEFAULT_LIMIT: usize = 10;
pub const DEFAULT_LEN: usize = 10;

const SPACER: &str = "   ";

#[derive(ValueEnum, Default, Clone, Copy, Debug)]
pub enum TopPrintFormat {
    #[default]
    Text,
    Json,
}

impl std::fmt::Display for TopPrintFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Text => "text".fmt(f),
            Self::Json => "json".fmt(f),
        }
    }
}

impl From<&str> for TopPrintFormat {
    fn from(s: &str) -> Self {
        match s {
            "json" => Self::Json,
            "text" => Self::Text,
            _ => Self::default(),
        }
    }
}

#[derive(ValueEnum, Default, Clone, Copy, PartialEq, Eq, Debug)]
pub enum ValueFmt {
    Percentage,
    Value,
    #[default]
    Both,
}

impl std::fmt::Display for ValueFmt {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use ValueFmt as Vf;

        match self {
            Vf::Percentage => "percentage".fmt(f),
            Vf::Value => "value".fmt(f),
            Vf::Both => "both".fmt(f),
        }
    }
}

impl ValueFmt {
    pub fn description(&self) -> &'static str {
        use ValueFmt as Vf;

        match self {
            Vf::Percentage => "Percentages",
            Vf::Value => "Values",
            Vf::Both => "Values / Percentages",
        }
    }
}

impl From<&str> for ValueFmt {
    fn from(s: &str) -> ValueFmt {
        use ValueFmt as Vf;

        match s {
            "perc" => Vf::Percentage,
            "value" => Vf::Value,
            "both" => Vf::Both,
            _ => unreachable!(),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum FieldFmt {
    Int,
    Float,
    Time,
    Bytes,
}

pub trait TopSummary: RawOutput + Serialize {
    type Stats: TopStats<Field = Self::Field, Lengths = Self::Lengths>;
    type Field: TopField;
    type Lengths: TopLengths<Field = Self::Field>;

    fn print(
        &self,
        out: impl Write,
        sort_by: Self::Field,
        fmt: ValueFmt,
        print_fmt: PrintFormat,
        limit: Option<usize>,
    ) -> io::Result<()> {
        match print_fmt {
            PrintFormat::Json => self.print_json(out),
            _ => self.print_text(out, sort_by, fmt, limit),
        }
    }

    fn print_json(&self, mut out: impl Write) -> io::Result<()> {
        let serialized = serde_json::to_string(&self).expect("failed to serialize top data");
        writeln!(out, "{}", serialized)
    }

    fn print_text(
        &self,
        mut out: impl Write,
        sort_by: Self::Field,
        fmt: ValueFmt,
        limit: Option<usize>,
    ) -> io::Result<()> {
        let sorted_stats: Vec<_> = self
            .stats()
            .into_iter()
            .map(|s| s.as_sorted(sort_by))
            .collect();

        let ct = limit.unwrap_or(DEFAULT_LIMIT);
        let item_len = Self::max_item_len(sorted_stats.as_slice(), ct);
        let all_lens = self.calc_dur_lens(sorted_stats.as_slice(), ct);
        let args = PrintArgs::new(fmt, item_len, ct, sort_by, all_lens);

        self.print_totals(&mut out)?;

        for (stats, items) in self.stats().into_iter().zip(&sorted_stats) {
            stats.print(&mut out, &args, self.totals(), items)?;
        }

        Ok(())
    }

    fn print_totals(&self, out: impl Write) -> io::Result<()>;

    fn max_field_len(
        maps: &[Vec<(&String, &Self::Stats)>],
        ct: usize,
        field: <Self as TopSummary>::Field,
    ) -> usize {
        maps.iter()
            .flat_map(|i| i.iter().take(ct))
            .map(|(_, s)| s.get_field(field))
            .map(|v| field.formatted(v).len())
            .max_by(|x, y| x.cmp(y))
            .unwrap_or(DEFAULT_LEN)
    }

    fn max_item_len(maps: &[Vec<(&String, &Self::Stats)>], ct: usize) -> usize {
        maps.iter()
            .flat_map(|i| i.iter().take(ct))
            .map(|s| s.0.len())
            .max()
            .unwrap_or(DEFAULT_LEN)
    }

    fn all_present_fields(&self) -> HashSet<Self::Field> {
        let mut all_present_fields = HashSet::new();

        for stats in self.stats() {
            all_present_fields.extend(stats.present_fields());
        }

        all_present_fields
    }

    fn calc_dur_lens(&self, maps: &[Vec<(&String, &Self::Stats)>], ct: usize) -> Self::Lengths {
        let mut lens = Self::Lengths::default();

        for field in self.all_present_fields() {
            let len = Self::max_field_len(maps, ct, field);
            lens.insert(field, len);
        }

        lens
    }

    fn stats(&self) -> Vec<&TopStatsMap<Self::Stats>>;
    fn stats_mut(&mut self) -> Vec<&mut TopStatsMap<Self::Stats>>;
    fn totals(&self) -> &Self::Stats;
}

pub trait TopStats: Default + Send + Sync {
    type Field: TopField;
    type Column: TopColumn;
    type Lengths: TopLengths<Field = Self::Field>;

    fn add_event<T: Event>(&mut self, event: &T);
    fn add(&mut self, other: &Self);
    fn get_field(&self, field: Self::Field) -> f64;

    /// Sort in descending order by the field requested. In cases where
    /// the value matches, sort in ascending order by the item name. The latter
    /// is always unique and ensures stable ordering even when multi-threaded.
    fn sorted<'a>(
        field: Self::Field,
        stats_iter: impl Iterator<Item = (&'a String, &'a Self)>,
    ) -> Vec<(&'a String, &'a Self)>
    where
        &'a Self: Send,
    {
        let mut to_sort: Vec<_> = stats_iter.collect();

        to_sort.par_sort_unstable_by(|x, y| {
            let p_ord =
                y.1.get_field(field)
                    .partial_cmp(&x.1.get_field(field))
                    .expect("unable to sort stats");

            if let Ordering::Equal = p_ord {
                x.0.cmp(y.0)
            } else {
                p_ord
            }
        });

        to_sort
    }

    fn field_is_present<'a>(field: Self::Field, stats_iter: impl Iterator<Item = &'a Self>) -> bool
    where
        Self: 'a,
    {
        stats_iter.map(|s| s.get_field(field)).any(|v| v > 0.0)
    }
}

pub trait TopField: Display + Hash + Eq + Copy + Clone + Send + Sync {
    fn header(&self) -> &'static str;
    fn field_fmt(&self) -> FieldFmt;
    fn mandatory_fields_start<'a>() -> &'a [Self];
    fn mandatory_fields_end<'a>() -> &'a [Self];
    fn optional_fields<'a>() -> &'a [Self];

    fn min_len(&self) -> usize {
        self.header().len()
    }

    fn formatted(&self, val: f64) -> String {
        use FieldFmt as Ff;
        match self.field_fmt() {
            Ff::Int => (val as usize).to_string(),
            Ff::Float => format!("{val:.2}"),
            Ff::Bytes => fmt::fmt_bytes(val as u64),
            Ff::Time => fmt::fmt_dur(val),
        }
    }
}

pub trait TopLengths: Default {
    type Field: TopField;

    const PERC_LEN: usize = 3; // "100"
    const SEPARATOR_LEN: usize = 3; // " / "

    fn fmt_len(&self, field: Self::Field, fmt: ValueFmt) -> usize {
        use ValueFmt as Vf;
        match fmt {
            Vf::Percentage => Self::PERC_LEN,
            Vf::Value => self.get(field),
            Vf::Both => self.get(field) + Self::SEPARATOR_LEN + Self::PERC_LEN,
        }
    }

    fn min_fmt_len(&self, field: Self::Field, fmt: ValueFmt) -> usize {
        self.fmt_len(field, fmt).max(field.min_len())
    }

    fn insert(&mut self, field: Self::Field, len: usize);
    fn get(&self, field: Self::Field) -> usize;
}

pub trait TopColumn: Clone + Display {
    fn title(&self) -> String {
        format!("{}", self).to_uppercase()
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct PrintArgs<L: TopLengths> {
    pub fmt: ValueFmt,
    pub title_len: usize,
    pub ct: usize,
    pub sort_by: L::Field,
    pub lens: L,
}

impl<L: TopLengths> PrintArgs<L> {
    pub fn new(
        fmt: ValueFmt,
        item_len: usize,
        ct: usize,
        sort_by: L::Field,
        lens: L,
    ) -> PrintArgs<L> {
        PrintArgs {
            fmt,
            title_len: item_len,
            ct,
            sort_by,
            lens,
        }
    }
}
