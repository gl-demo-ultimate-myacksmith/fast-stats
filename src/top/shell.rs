use super::fmt::fmt_dur;
use super::stats_map::TopStatsMap;
use super::{FieldFmt, TopColumn, TopField, TopLengths, TopStats, TopSummary};
use crate::data::{FilterOutput, RawArgs, RawOutput};
use crate::event::Event;
use crate::field::ShellField;

use rayon::prelude::*;
use serde::Serialize;
use std::fmt;
use std::io::{self, prelude::*};

#[derive(Clone, PartialEq, Serialize, Debug)]
pub struct ShellSummary {
    pub project_stats: TopStatsMap<ShellStats>,
    pub user_stats: TopStatsMap<ShellStats>,
    pub totals: ShellStats,
    pub start_time: i64,
    pub end_time: i64,
}

#[derive(Clone, Default, PartialEq, Serialize, Debug)]
pub struct ShellStats {
    pub count: usize,
    pub duration: f64,
    pub fails: u32,
    pub seconds: i64,
}

#[derive(Clone, Default, PartialEq, Eq, Debug)]
pub struct ShellLengths {
    count: usize,
    rps: usize,
    dur: usize,
    fails: usize,
}

#[derive(Clone, PartialEq, Eq, Serialize, Debug)]
pub enum ShellColumn {
    Project,
    User,
}

impl RawOutput for ShellSummary {
    fn new(_args: RawArgs) -> Self {
        ShellSummary {
            project_stats: TopStatsMap::new(ShellColumn::Project),
            user_stats: TopStatsMap::new(ShellColumn::User),
            totals: ShellStats::default(),
            start_time: i64::MAX,
            end_time: i64::MIN,
        }
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        self.project_stats.insert(&event, event.project());
        self.user_stats.insert(&event, event.user());
        self.update_total(&event);

        if let Some(ts) = T::parse_time(event.raw_time()).map(|t| t.timestamp()) {
            self.update_times(ts);
        }
    }

    fn coalesce(&mut self, other: Self) {
        self.project_stats.extend(other.project_stats);
        self.user_stats.extend(other.user_stats);
        self.totals.add(&other.totals);

        if other.start_time < self.start_time {
            self.start_time = other.start_time;
        }

        if other.end_time > self.end_time {
            self.end_time = other.end_time;
        }

        // update data with total duration found so far
        self.insert_session_duration();
    }

    fn finalize(&mut self) {
        self.insert_session_duration();
    }
}

impl FilterOutput for ShellSummary {
    fn filter(mut self, term: Option<&str>) -> ShellSummary {
        if let Some(s) = term {
            [&mut self.project_stats.stats, &mut self.user_stats.stats]
                .par_iter_mut()
                .for_each(|stats| {
                    stats.retain(|key, _| key.to_ascii_lowercase().contains(s));
                });
        }

        self
    }
}

impl TopSummary for ShellSummary {
    type Stats = ShellStats;
    type Field = ShellField;
    type Lengths = ShellLengths;

    fn print_totals(&self, mut out: impl Write) -> io::Result<()> {
        writeln!(&mut out, "Totals\n")?;

        writeln!(&mut out, "COUNT:   {}", self.totals.count)?;
        writeln!(
            &mut out,
            "RPS:     {:.2}",
            self.totals.count as f64 / (self.end_time - self.start_time) as f64
        )?;
        writeln!(&mut out, "DUR:     {}", fmt_dur(self.totals.duration))?;

        if self.totals.fails > 0 {
            writeln!(&mut out, "FAIL_CT: {}", self.totals.fails)?;
        }

        Ok(())
    }

    fn stats(&self) -> Vec<&TopStatsMap<ShellStats>> {
        vec![&self.project_stats, &self.user_stats]
    }

    fn stats_mut(&mut self) -> Vec<&mut TopStatsMap<ShellStats>> {
        vec![&mut self.project_stats, &mut self.user_stats]
    }

    fn totals(&self) -> &ShellStats {
        &self.totals
    }
}

impl ShellSummary {
    fn update_total<T: Event>(&mut self, event: &T) {
        self.totals.add_event(event);
    }

    fn update_times(&mut self, ts: i64) {
        if ts < self.start_time {
            self.start_time = ts;
        }

        if ts > self.end_time {
            self.end_time = ts;
        }
    }

    // We don't know the total duration until after all events have been collected.
    fn insert_session_duration(&mut self) {
        if self.totals.count == 0 {
            return;
        }

        let dur = self.end_time - self.start_time;

        for stats_map in self.stats_mut() {
            for stats in stats_map.stats.values_mut() {
                stats.seconds = dur;
            }
        }
        self.totals.seconds = dur;
    }
}

impl TopStats for ShellStats {
    type Field = ShellField;
    type Column = ShellColumn;
    type Lengths = ShellLengths;

    fn add_event<T: Event>(&mut self, event: &T) {
        // Gitaly logs events with command rusage details separately from RPC completion.
        // Capturing both here would double-count, only increment count for events that
        // cover the end of an RPC.
        if !event.status().is_info() {
            self.count += 1;
        }
        self.duration += event.dur_ms() / 1000.0;
        if event.status().is_fail() {
            self.fails += 1;
        }
    }

    fn add(&mut self, other: &ShellStats) {
        self.count += other.count;
        self.duration += other.duration;
        self.fails += other.fails;
    }

    fn get_field(&self, field: ShellField) -> f64 {
        use ShellField as Sf;

        match field {
            Sf::Count => self.count as f64,
            Sf::Rps => self.count as f64 / self.seconds as f64,
            Sf::Duration => self.duration,
            Sf::FailCt => self.fails as f64,
        }
    }
}

impl TopField for ShellField {
    fn header(&self) -> &'static str {
        use ShellField as Sf;
        match self {
            Sf::Count => "COUNT",
            Sf::Rps => "RPS",
            Sf::Duration => "DUR",
            Sf::FailCt => "FAIL_CT",
        }
    }

    fn field_fmt(&self) -> FieldFmt {
        use ShellField as Sf;
        match self {
            Sf::Count | Sf::FailCt => FieldFmt::Int,
            Sf::Rps => FieldFmt::Float,
            Sf::Duration => FieldFmt::Time,
        }
    }

    fn mandatory_fields_start<'a>() -> &'a [ShellField] {
        use ShellField as Sf;
        &[Sf::Count, Sf::Rps, Sf::Duration]
    }

    fn mandatory_fields_end<'a>() -> &'a [ShellField] {
        use ShellField as Sf;
        &[Sf::FailCt]
    }
    fn optional_fields<'a>() -> &'a [ShellField] {
        &[]
    }
}

impl TopLengths for ShellLengths {
    type Field = ShellField;

    fn insert(&mut self, field: ShellField, len: usize) {
        use ShellField as Sf;
        match field {
            Sf::Count => self.count = len,
            Sf::Rps => self.rps = len,
            Sf::Duration => self.dur = len,
            Sf::FailCt => self.fails = len,
        }
    }

    fn get(&self, field: ShellField) -> usize {
        use ShellField as Sf;
        match field {
            Sf::Count => self.count,
            Sf::Rps => self.rps,
            Sf::Duration => self.dur,
            Sf::FailCt => self.fails,
        }
    }
}

impl fmt::Display for ShellColumn {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ShellColumn as Sc;
        write!(
            f,
            "{}",
            match self {
                Sc::Project => "Project",
                Sc::User => "User",
            }
        )
    }
}

impl TopColumn for ShellColumn {}
