use super::{PrintArgs, TopColumn, TopField, TopLengths, TopStats, SPACER};
use crate::event::Event;
use crate::HashMap;

use serde::Serialize;
use std::io::{self, prelude::*};

#[derive(Clone, PartialEq, Serialize, Debug)]
pub struct TopStatsMap<S: TopStats> {
    pub stats: HashMap<String, S>,
    pub col_t: S::Column,
}

impl<S: TopStats> TopStatsMap<S> {
    pub fn new(col_t: S::Column) -> TopStatsMap<S> {
        TopStatsMap {
            stats: HashMap::new(),
            col_t,
        }
    }

    pub fn extend(&mut self, other: TopStatsMap<S>) {
        for (key, data) in other.stats.into_iter() {
            self.stats.entry(key).or_insert_with(S::default).add(&data);
        }
    }

    pub fn insert<T: Event>(&mut self, event: &T, field: Option<&str>) {
        if let Some(item) = field {
            self.stats
                .entry(item.to_string())
                .or_insert_with(S::default)
                .add_event(event);
        }
    }

    pub fn as_sorted(&self, sort_by: S::Field) -> Vec<(&String, &S)> {
        S::sorted(sort_by, self.stats.iter())
    }

    pub fn present_fields(&self) -> Vec<S::Field> {
        let mut fields: Vec<S::Field> = Vec::new();
        fields.extend_from_slice(S::Field::mandatory_fields_start());
        for &field in S::Field::optional_fields() {
            if S::field_is_present(field, self.stats.values()) {
                fields.push(field);
            }
        }
        fields.extend_from_slice(S::Field::mandatory_fields_end());

        fields
    }

    pub fn print(
        &self,
        mut out: impl Write,
        args: &PrintArgs<S::Lengths>,
        totals: &S,
        items: &[(&String, &S)],
    ) -> io::Result<()> {
        if self.stats.is_empty() {
            return Ok(());
        }

        self.print_header(&mut out, args, &self.present_fields())?;
        for (item, stats) in items.iter().take(args.ct) {
            writeln!(
                out,
                "{:<len$}{}",
                item,
                super::fmt::fmt(args.fmt, &self.present_fields(), *stats, totals, &args.lens),
                len = args.title_len
            )?;
        }

        Ok(())
    }

    fn print_header(
        &self,
        mut out: impl Write,
        args: &PrintArgs<<S as TopStats>::Lengths>,
        fields: &[<S as TopStats>::Field],
    ) -> io::Result<()> {
        writeln!(
            out,
            "\n\nTop {} {} by {} -- {}\n",
            args.ct,
            self.col_t,
            args.sort_by,
            args.fmt.description(),
        )?;
        let mut hdr = format!("{:<len$}", self.col_t.title(), len = args.title_len);
        for field in fields {
            hdr += &format!(
                "{}{:>len$}",
                SPACER,
                field.header(),
                len = args.lens.min_fmt_len(*field, args.fmt)
            );
        }

        writeln!(out, "{}", hdr)
    }
}
