use super::fmt::fmt_dur;
use super::stats_map::TopStatsMap;
use super::FieldFmt;
use super::{TopColumn, TopField, TopLengths, TopStats, TopSummary};
use crate::data::{FilterOutput, RawArgs, RawOutput};
use crate::event::Event;
use crate::field::RailsField;

use rayon::prelude::*;
use serde::Serialize;
use std::fmt;
use std::io::{self, prelude::*};

#[derive(Clone, PartialEq, Serialize, Debug)]
pub struct RailsSummary {
    pub path_stats: TopStatsMap<RailsStats>,
    pub project_stats: TopStatsMap<RailsStats>,
    pub user_stats: TopStatsMap<RailsStats>,
    pub totals: RailsStats,
    pub start_time: i64,
    pub end_time: i64,
}

#[derive(Clone, Default, PartialEq, Serialize, Debug)]
pub struct RailsStats {
    pub count: usize,
    pub duration: f64,
    pub db_duration: f64,
    pub rugged_duration: f64,
    pub gitaly_duration: f64,
    pub redis_duration: f64,
    pub queue_duration: f64,
    pub cpu_s: f64,
    pub mem_b: u64,
    pub fails: u32,
    pub seconds: i64,
}

#[derive(Clone, Default, PartialEq, Eq, Debug)]
pub struct RailsLengths {
    count: usize,
    rps: usize,
    dur: usize,
    db: usize,
    redis: usize,
    gitaly: usize,
    rugged: usize,
    queue: usize,
    cpu: usize,
    mem: usize,
    fails: usize,
}

#[derive(Clone, PartialEq, Eq, Serialize, Debug)]
pub enum RailsColumn {
    Path,
    Project,
    User,
}

impl RawOutput for RailsSummary {
    fn new(_args: RawArgs) -> Self {
        RailsSummary {
            path_stats: TopStatsMap::new(RailsColumn::Path),
            project_stats: TopStatsMap::new(RailsColumn::Project),
            user_stats: TopStatsMap::new(RailsColumn::User),
            totals: RailsStats::default(),
            start_time: i64::MAX,
            end_time: i64::MIN,
        }
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        self.path_stats.insert(&event, event.path());
        self.project_stats.insert(&event, event.project());
        self.user_stats.insert(&event, event.user());
        self.update_total(&event);

        if let Some(ts) = T::parse_time(event.raw_time()).map(|t| t.timestamp()) {
            self.update_times(ts);
        }
    }

    fn coalesce(&mut self, other: Self) {
        self.path_stats.extend(other.path_stats);
        self.project_stats.extend(other.project_stats);
        self.user_stats.extend(other.user_stats);
        self.totals.add(&other.totals);

        if other.start_time < self.start_time {
            self.start_time = other.start_time;
        }

        if other.end_time > self.end_time {
            self.end_time = other.end_time;
        }

        // update data with total duration found so far
        self.insert_session_duration();
    }

    fn finalize(&mut self) {
        self.insert_session_duration();
    }
}

impl FilterOutput for RailsSummary {
    fn filter(mut self, term: Option<&str>) -> RailsSummary {
        if let Some(s) = term {
            [
                &mut self.path_stats.stats,
                &mut self.project_stats.stats,
                &mut self.user_stats.stats,
            ]
            .par_iter_mut()
            .for_each(|stats| {
                stats.retain(|key, _| key.to_ascii_lowercase().contains(s));
            });
        }

        self
    }
}

impl TopSummary for RailsSummary {
    type Stats = RailsStats;
    type Field = RailsField;
    type Lengths = RailsLengths;

    fn print_totals(&self, mut out: impl Write) -> io::Result<()> {
        writeln!(&mut out, "Totals\n")?;

        writeln!(&mut out, "COUNT:   {}", self.totals.count)?;
        writeln!(
            &mut out,
            "RPS:     {:.2}",
            self.totals.count as f64 / (self.end_time - self.start_time) as f64
        )?;
        writeln!(&mut out, "DUR:     {}", fmt_dur(self.totals.duration))?;

        if self.totals.db_duration > 0.0 {
            writeln!(&mut out, "DB:      {}", fmt_dur(self.totals.db_duration))?;
        }
        if self.totals.redis_duration > 0.0 {
            writeln!(&mut out, "REDIS:   {}", fmt_dur(self.totals.redis_duration))?;
        }
        if self.totals.gitaly_duration > 0.0 {
            writeln!(
                &mut out,
                "GITALY:  {}",
                fmt_dur(self.totals.gitaly_duration)
            )?;
        }
        if self.totals.rugged_duration > 0.0 {
            writeln!(
                &mut out,
                "RUGGED:  {}",
                fmt_dur(self.totals.rugged_duration)
            )?;
        }
        if self.totals.queue_duration > 0.0 {
            writeln!(&mut out, "QUEUE:   {}", fmt_dur(self.totals.queue_duration))?;
        }
        if self.totals.cpu_s > 0.0 {
            writeln!(&mut out, "CPU:     {}", fmt_dur(self.totals.cpu_s))?;
        }
        if self.totals.mem_b > 0 {
            writeln!(
                &mut out,
                "MEM:     {}",
                super::fmt::fmt_bytes(self.totals.mem_b)
            )?;
        }
        if self.totals.fails > 0 {
            writeln!(&mut out, "FAIL_CT: {}", self.totals.fails)?;
        }

        Ok(())
    }

    fn stats(&self) -> Vec<&TopStatsMap<RailsStats>> {
        vec![&self.path_stats, &self.project_stats, &self.user_stats]
    }

    fn stats_mut(&mut self) -> Vec<&mut TopStatsMap<RailsStats>> {
        vec![
            &mut self.path_stats,
            &mut self.project_stats,
            &mut self.user_stats,
        ]
    }

    fn totals(&self) -> &RailsStats {
        &self.totals
    }
}

impl RailsSummary {
    fn update_total<T: Event>(&mut self, event: &T) {
        self.totals.add_event(event);
    }

    fn update_times(&mut self, ts: i64) {
        if ts < self.start_time {
            self.start_time = ts;
        }

        if ts > self.end_time {
            self.end_time = ts;
        }
    }

    // We don't know the total duration until after all events have been collected.
    fn insert_session_duration(&mut self) {
        if self.totals.count == 0 {
            return;
        }

        let dur = self.end_time - self.start_time;

        for stats_map in self.stats_mut() {
            for stats in stats_map.stats.values_mut() {
                stats.seconds = dur;
            }
        }
        self.totals.seconds = dur;
    }
}

impl TopStats for RailsStats {
    type Field = RailsField;
    type Column = RailsColumn;
    type Lengths = RailsLengths;

    fn add_event<T: Event>(&mut self, event: &T) {
        self.count += 1;
        self.duration += event.dur_ms() / 1000.0;
        self.db_duration += event.db().unwrap_or_default();
        self.redis_duration += event.redis().unwrap_or_default();
        self.gitaly_duration += event.gitaly().unwrap_or_default();
        self.rugged_duration += event.rugged().unwrap_or_default();
        self.queue_duration += event.queue().unwrap_or_default();
        self.cpu_s += event.cpu_s().unwrap_or_default();
        self.mem_b += event.mem().unwrap_or_default();
        if event.status().is_fail() {
            self.fails += 1;
        }
    }

    fn add(&mut self, other: &RailsStats) {
        self.count += other.count;
        self.duration += other.duration;
        self.db_duration += other.db_duration;
        self.redis_duration += other.redis_duration;
        self.gitaly_duration += other.gitaly_duration;
        self.rugged_duration += other.rugged_duration;
        self.queue_duration += other.queue_duration;
        self.cpu_s += other.cpu_s;
        self.mem_b += other.mem_b;
        self.fails += other.fails;
    }

    fn get_field(&self, field: RailsField) -> f64 {
        use RailsField as Rf;

        match field {
            Rf::Count => self.count as f64,
            Rf::Rps => self.count as f64 / self.seconds as f64,
            Rf::Duration => self.duration,
            Rf::Db => self.db_duration,
            Rf::Redis => self.redis_duration,
            Rf::Gitaly => self.gitaly_duration,
            Rf::Rugged => self.rugged_duration,
            Rf::Queue => self.queue_duration,
            Rf::CpuS => self.cpu_s,
            Rf::Mem => self.mem_b as f64,
            Rf::FailCt => self.fails as f64,
        }
    }
}

impl TopField for RailsField {
    fn header(&self) -> &'static str {
        use RailsField as Rf;
        match self {
            Rf::Count => "COUNT",
            Rf::Rps => "RPS",
            Rf::Duration => "DUR",
            Rf::Db => "DB",
            Rf::Redis => "REDIS",
            Rf::Gitaly => "GITALY",
            Rf::Rugged => "RUGGED",
            Rf::Queue => "QUEUE",
            Rf::CpuS => "CPU",
            Rf::Mem => "MEM",
            Rf::FailCt => "FAIL_CT",
        }
    }

    fn field_fmt(&self) -> FieldFmt {
        use RailsField as Rf;
        match self {
            Rf::Count | Rf::FailCt => FieldFmt::Int,
            Rf::Rps => FieldFmt::Float,
            Rf::Duration | Rf::Db | Rf::Redis | Rf::Gitaly | Rf::Rugged | Rf::Queue | Rf::CpuS => {
                FieldFmt::Time
            }
            Rf::Mem => FieldFmt::Bytes,
        }
    }

    fn mandatory_fields_start<'a>() -> &'a [RailsField] {
        use RailsField as Rf;
        &[Rf::Count, Rf::Rps, Rf::Duration]
    }

    fn mandatory_fields_end<'a>() -> &'a [RailsField] {
        use RailsField as Rf;
        &[Rf::FailCt]
    }
    fn optional_fields<'a>() -> &'a [RailsField] {
        use RailsField as Rf;
        &[
            Rf::Db,
            Rf::Redis,
            Rf::Gitaly,
            Rf::Rugged,
            Rf::Queue,
            Rf::CpuS,
            Rf::Mem,
        ]
    }
}

impl TopLengths for RailsLengths {
    type Field = RailsField;

    fn insert(&mut self, field: RailsField, len: usize) {
        use RailsField as Rf;
        match field {
            Rf::Count => self.count = len,
            Rf::Rps => self.rps = len,
            Rf::Duration => self.dur = len,
            Rf::Db => self.db = len,
            Rf::Redis => self.redis = len,
            Rf::Gitaly => self.gitaly = len,
            Rf::Rugged => self.rugged = len,
            Rf::Queue => self.queue = len,
            Rf::CpuS => self.cpu = len,
            Rf::Mem => self.mem = len,
            Rf::FailCt => self.fails = len,
        }
    }

    fn get(&self, field: RailsField) -> usize {
        use RailsField as Rf;
        match field {
            Rf::Count => self.count,
            Rf::Rps => self.rps,
            Rf::Duration => self.dur,
            Rf::Db => self.db,
            Rf::Redis => self.redis,
            Rf::Gitaly => self.gitaly,
            Rf::Rugged => self.rugged,
            Rf::Queue => self.queue,
            Rf::CpuS => self.cpu,
            Rf::Mem => self.mem,
            Rf::FailCt => self.fails,
        }
    }
}

impl fmt::Display for RailsColumn {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use RailsColumn as Rc;
        write!(
            f,
            "{}",
            match self {
                Rc::Path => "Path",
                Rc::Project => "Project",
                Rc::User => "User",
            }
        )
    }
}

impl TopColumn for RailsColumn {}
