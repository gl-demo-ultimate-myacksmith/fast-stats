use super::fmt::fmt_dur;
use super::stats_map::TopStatsMap;
use super::{FieldFmt, TopColumn, TopField, TopLengths, TopStats, TopSummary};
use crate::data::{FilterOutput, RawArgs, RawOutput};
use crate::event::Event;
use crate::field::GitalyField;

use rayon::prelude::*;
use serde::Serialize;
use std::fmt;
use std::io::{self, prelude::*};

#[derive(Clone, PartialEq, Serialize, Debug)]
pub struct GitalySummary {
    pub project_stats: TopStatsMap<GitalyStats>,
    pub user_stats: TopStatsMap<GitalyStats>,
    pub client_stats: TopStatsMap<GitalyStats>,
    pub totals: GitalyStats,
    pub start_time: i64,
    pub end_time: i64,
}

#[derive(Clone, Default, PartialEq, Serialize, Debug)]
pub struct GitalyStats {
    pub count: usize,
    pub duration: f64,
    pub cpu_s: f64,
    pub response_bytes: u64,
    pub disk_read: u64,
    pub disk_write: u64,
    pub rss_kb: u64,
    pub fails: u32,
    pub seconds: i64,
}

#[derive(Clone, Default, PartialEq, Eq, Debug)]
pub struct GitalyLengths {
    count: usize,
    rps: usize,
    dur: usize,
    cpu: usize,
    resp_bytes: usize,
    disk_read: usize,
    disk_write: usize,
    rss: usize,
    fails: usize,
}

#[derive(Clone, PartialEq, Eq, Serialize, Debug)]
pub enum GitalyColumn {
    Client,
    Project,
    User,
}

impl RawOutput for GitalySummary {
    fn new(_args: RawArgs) -> Self {
        GitalySummary {
            project_stats: TopStatsMap::new(GitalyColumn::Project),
            user_stats: TopStatsMap::new(GitalyColumn::User),
            client_stats: TopStatsMap::new(GitalyColumn::Client),
            totals: GitalyStats::default(),
            start_time: i64::MAX,
            end_time: i64::MIN,
        }
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        self.project_stats.insert(&event, event.project());
        self.user_stats.insert(&event, event.user());
        self.client_stats.insert(&event, event.client_type());
        self.update_total(&event);

        if let Some(ts) = T::parse_time(event.raw_time()).map(|t| t.timestamp()) {
            self.update_times(ts);
        }
    }

    fn coalesce(&mut self, other: Self) {
        self.project_stats.extend(other.project_stats);
        self.user_stats.extend(other.user_stats);
        self.client_stats.extend(other.client_stats);
        self.totals.add(&other.totals);

        if other.start_time < self.start_time {
            self.start_time = other.start_time;
        }

        if other.end_time > self.end_time {
            self.end_time = other.end_time;
        }

        // update data with total duration found so far
        self.insert_session_duration();
    }

    fn finalize(&mut self) {
        self.insert_session_duration();
    }
}

impl FilterOutput for GitalySummary {
    fn filter(mut self, term: Option<&str>) -> GitalySummary {
        if let Some(s) = term {
            [
                &mut self.project_stats.stats,
                &mut self.user_stats.stats,
                &mut self.client_stats.stats,
            ]
            .par_iter_mut()
            .for_each(|stats| {
                stats.retain(|key, _| key.to_ascii_lowercase().contains(s));
            });
        }

        self
    }
}

impl TopSummary for GitalySummary {
    type Stats = GitalyStats;
    type Field = GitalyField;
    type Lengths = GitalyLengths;

    fn print_totals(&self, mut out: impl Write) -> io::Result<()> {
        writeln!(&mut out, "Totals\n")?;

        writeln!(&mut out, "COUNT:   {}", self.totals.count)?;
        writeln!(
            &mut out,
            "RPS:     {:.2}",
            self.totals.count as f64 / (self.end_time - self.start_time) as f64
        )?;
        writeln!(&mut out, "DUR:     {}", fmt_dur(self.totals.duration))?;

        if self.totals.cpu_s > 0.0 {
            writeln!(&mut out, "CPU:     {}", fmt_dur(self.totals.cpu_s))?;
        }
        if self.totals.rss_kb > 0 {
            writeln!(
                &mut out,
                "GIT_RSS: {}",
                super::fmt::fmt_bytes(self.totals.rss_kb * 1024)
            )?;
        }
        if self.totals.response_bytes > 0 {
            writeln!(
                &mut out,
                "BYTES:   {}",
                super::fmt::fmt_bytes(self.totals.response_bytes)
            )?;
        }
        if self.totals.disk_read > 0 {
            writeln!(&mut out, "DISK_R:  {}", self.totals.disk_read)?;
        }
        if self.totals.disk_write > 0 {
            writeln!(&mut out, "DISK_W:  {}", self.totals.disk_write)?;
        }
        if self.totals.fails > 0 {
            writeln!(&mut out, "FAIL_CT: {}", self.totals.fails)?;
        }

        Ok(())
    }

    fn stats(&self) -> Vec<&TopStatsMap<GitalyStats>> {
        vec![&self.project_stats, &self.user_stats, &self.client_stats]
    }

    fn stats_mut(&mut self) -> Vec<&mut TopStatsMap<GitalyStats>> {
        vec![
            &mut self.project_stats,
            &mut self.user_stats,
            &mut self.client_stats,
        ]
    }

    fn totals(&self) -> &GitalyStats {
        &self.totals
    }
}

impl GitalySummary {
    fn update_total<T: Event>(&mut self, event: &T) {
        self.totals.add_event(event);
    }

    fn update_times(&mut self, ts: i64) {
        if ts < self.start_time {
            self.start_time = ts;
        }

        if ts > self.end_time {
            self.end_time = ts;
        }
    }

    // We don't know the total duration until after all events have been collected.
    fn insert_session_duration(&mut self) {
        if self.totals.count == 0 {
            return;
        }

        let dur = self.end_time - self.start_time;

        for stats_map in self.stats_mut() {
            for stats in stats_map.stats.values_mut() {
                stats.seconds = dur;
            }
        }
        self.totals.seconds = dur;
    }
}

impl TopStats for GitalyStats {
    type Field = GitalyField;
    type Column = GitalyColumn;
    type Lengths = GitalyLengths;

    fn add_event<T: Event>(&mut self, event: &T) {
        // Gitaly logs events with command rusage details separately from RPC completion.
        // Capturing both here would double-count, only increment count for events that
        // cover the end of an RPC.
        if !event.status().is_info() {
            self.count += 1;
        }
        self.duration += event.dur_ms() / 1000.0;
        self.cpu_s += event.cpu_s().unwrap_or_default();
        self.response_bytes += event.bytes_written().unwrap_or_default();
        self.disk_read += event.disk_read().unwrap_or_default();
        self.disk_write += event.disk_write().unwrap_or_default();
        self.rss_kb += event.mem().unwrap_or_default();
        if event.status().is_fail() {
            self.fails += 1;
        }
    }

    fn add(&mut self, other: &GitalyStats) {
        self.count += other.count;
        self.duration += other.duration;
        self.cpu_s += other.cpu_s;
        self.response_bytes += other.response_bytes;
        self.disk_read += other.disk_read;
        self.disk_write += other.disk_write;
        self.rss_kb += other.rss_kb;
        self.fails += other.fails;
    }

    fn get_field(&self, field: GitalyField) -> f64 {
        use GitalyField as Gf;

        match field {
            Gf::Count => self.count as f64,
            Gf::Rps => self.count as f64 / self.seconds as f64,
            Gf::Duration => self.duration,
            Gf::CpuS => self.cpu_s,
            Gf::ResponseBytes => self.response_bytes as f64,
            Gf::DiskRead => self.disk_read as f64,
            Gf::DiskWrite => self.disk_write as f64,
            Gf::Rss => self.rss_kb as f64,
            Gf::FailCt => self.fails as f64,
        }
    }
}

impl TopField for GitalyField {
    fn header(&self) -> &'static str {
        use GitalyField as Gf;
        match self {
            Gf::Count => "COUNT",
            Gf::Rps => "RPS",
            Gf::Duration => "DUR",
            Gf::CpuS => "CPU",
            Gf::ResponseBytes => "RESP_BYTES",
            Gf::DiskRead => "DISK_R",
            Gf::DiskWrite => "DISK_W",
            Gf::Rss => "GIT_RSS",
            Gf::FailCt => "FAIL_CT",
        }
    }

    fn field_fmt(&self) -> FieldFmt {
        use GitalyField as Gf;
        match self {
            Gf::Count | Gf::FailCt | Gf::DiskRead | Gf::DiskWrite => FieldFmt::Int,
            Gf::Rps => FieldFmt::Float,
            Gf::Duration | Gf::CpuS => FieldFmt::Time,
            Gf::ResponseBytes | Gf::Rss => FieldFmt::Bytes,
        }
    }

    fn mandatory_fields_start<'a>() -> &'a [GitalyField] {
        use GitalyField as Gf;
        &[Gf::Count, Gf::Rps, Gf::Duration]
    }

    fn mandatory_fields_end<'a>() -> &'a [GitalyField] {
        use GitalyField as Gf;
        &[Gf::FailCt]
    }
    fn optional_fields<'a>() -> &'a [GitalyField] {
        use GitalyField as Gf;
        &[
            Gf::CpuS,
            Gf::Rss,
            Gf::ResponseBytes,
            Gf::DiskRead,
            Gf::DiskWrite,
        ]
    }
}

impl TopLengths for GitalyLengths {
    type Field = GitalyField;

    fn insert(&mut self, field: GitalyField, len: usize) {
        use GitalyField as Gf;
        match field {
            Gf::Count => self.count = len,
            Gf::Rps => self.rps = len,
            Gf::Duration => self.dur = len,
            Gf::CpuS => self.cpu = len,
            Gf::ResponseBytes => self.resp_bytes = len,
            Gf::DiskRead => self.disk_read = len,
            Gf::DiskWrite => self.disk_write = len,
            Gf::Rss => self.rss = len,
            Gf::FailCt => self.fails = len,
        }
    }

    fn get(&self, field: GitalyField) -> usize {
        use GitalyField as Gf;
        match field {
            Gf::Count => self.count,
            Gf::Rps => self.rps,
            Gf::Duration => self.dur,
            Gf::CpuS => self.cpu,
            Gf::ResponseBytes => self.resp_bytes,
            Gf::DiskRead => self.disk_read,
            Gf::DiskWrite => self.disk_write,
            Gf::Rss => self.rss,
            Gf::FailCt => self.fails,
        }
    }
}

impl fmt::Display for GitalyColumn {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use GitalyColumn as Gf;
        write!(
            f,
            "{}",
            match self {
                Gf::Client => "Client",
                Gf::Project => "Project",
                Gf::User => "User",
            }
        )
    }
}

impl TopColumn for GitalyColumn {}
