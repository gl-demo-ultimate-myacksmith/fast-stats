use super::{TopField, TopLengths, TopStats, ValueFmt};

use byte_unit::Byte;
use chrono::Duration;

const SPACER: &str = "   ";

pub fn fmt_dur(dur_s: f64) -> String {
    let mut dur = Duration::milliseconds((dur_s * 1000.0) as i64);
    let mut out = String::new();

    if dur.num_hours() > 0 {
        out += &format!("{}h", dur.num_hours());
    }
    dur = dur - Duration::hours(dur.num_hours());

    if !out.is_empty() {
        out += &format!("{:02}m", dur.num_minutes());
    } else if dur.num_minutes() > 0 {
        out += &format!("{}m", dur.num_minutes());
    }
    dur = dur - Duration::minutes(dur.num_minutes());

    let secs = dur.num_milliseconds() as f64 / 1000.0;
    if out.is_empty() {
        out += &format!("{:03.1}s", secs);
    } else {
        out += &format!("{:04.1}s", secs);
    }

    out
}

pub fn fmt_bytes(bytes: u64) -> String {
    let byte = Byte::from_bytes(bytes.into());
    byte.get_appropriate_unit(true).to_string()
}

pub fn fmt<S, L>(fmt: ValueFmt, fields: &[S::Field], stats: &S, totals: &S, lens: &L) -> String
where
    S: TopStats,
    L: TopLengths<Field = S::Field>,
{
    use ValueFmt as Vf;
    match fmt {
        Vf::Percentage => fmt_percs(fields, stats, totals, lens),
        Vf::Value => fmt_values(fields, stats, lens),
        Vf::Both => fmt_both(fields, stats, totals, lens),
    }
}

fn fmt_both<S, L>(fields: &[S::Field], stats: &S, totals: &S, lens: &L) -> String
where
    S: TopStats,
    L: TopLengths<Field = S::Field>,
{
    let mut out = String::new();
    for field in fields {
        out += &format!(
            "{}{:>val_len$} / {:>perc_len$}",
            SPACER,
            field.formatted(stats.get_field(*field)),
            fmt_perc(*field, stats, totals),
            val_len = lens.fmt_len(*field, ValueFmt::Value),
            perc_len = lens.fmt_len(*field, ValueFmt::Percentage),
        );
    }

    out
}

fn fmt_percs<S, L>(fields: &[S::Field], stats: &S, totals: &S, lens: &L) -> String
where
    S: TopStats,
    L: TopLengths<Field = S::Field>,
{
    let mut out = String::new();
    for field in fields {
        out += &format!(
            "{}{:>perc_len$}",
            SPACER,
            fmt_perc(*field, stats, totals),
            perc_len = lens.min_fmt_len(*field, ValueFmt::Percentage)
        );
    }

    out
}

fn fmt_values<S, L>(fields: &[S::Field], stats: &S, lens: &L) -> String
where
    S: TopStats,
    L: TopLengths<Field = S::Field>,
{
    let mut out = String::new();
    for field in fields {
        out += &format!(
            "{}{:>len$}",
            SPACER,
            field.formatted(stats.get_field(*field)),
            len = lens.min_fmt_len(*field, ValueFmt::Value)
        );
    }

    out
}

fn fmt_perc<S: TopStats>(field: S::Field, stats: &S, totals: &S) -> String {
    format!(
        "{:>3.0}",
        perc(stats.get_field(field), totals.get_field(field))
    )
}

fn perc(event: f64, total: f64) -> f64 {
    if total == 0.0 {
        return 0.0;
    }

    event / total * 100.0
}
