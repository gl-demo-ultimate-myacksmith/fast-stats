use crate::field::StatField;
use crate::stats::StatsType;

pub mod wrap;

pub fn csv_header(stats_t: StatsType, fields: &[StatField], prefix: Option<&str>) -> String {
    let mut output = match prefix {
        Some(pfx) => format!("{},{}", pfx, stats_t.header_title()),
        None => stats_t.header_title().to_string(),
    };

    for field in fields {
        output += &format!(",{}", field);
    }

    output
}

pub fn md_header(stats_t: StatsType, fields: &[StatField], prefix: Option<&str>) -> String {
    let mut output = match prefix {
        Some(pfx) => format!("|{}|{}", pfx, stats_t.header_title()),
        None => format!("|{}", stats_t.header_title()),
    };

    for field in fields {
        output += &format!("|{}", field);
    }

    let mut spacer_len = fields.len() + 1; // +1 for header title
    if prefix.is_some() {
        spacer_len += 1;
    }

    let spacers = "--|".repeat(spacer_len);
    format!("{}|\n|{}", output, spacers)
}

pub fn text_header(
    stats_t: StatsType,
    fields: &[StatField],
    lens: &[usize],
    prefix: Option<(&str, usize)>,
) -> String {
    let mut output = match prefix {
        Some((pfx, p_len)) => format!(
            "{:pfx_len$}  {:<len$}",
            pfx,
            stats_t.header_title(),
            pfx_len = p_len,
            len = lens[0]
        ),
        None => format!("{:<len$}", stats_t.header_title(), len = lens[0]),
    };

    for field in fields {
        let len = field.len(lens);
        output += &format!("  {:>width$}", field, width = len);
    }

    output
}
