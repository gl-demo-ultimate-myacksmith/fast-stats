use crate::data::{FilterOutput, RawArgs, RawOutput};
use crate::event::Event;
use crate::stats_vec::PrintFormat;
use crate::util::*;
use crate::{HashMap, HashSet};

use rayon::prelude::*;
use serde::Serialize;
use std::io::{self, Write};

const BACKTRACE_LEN: usize = 3;
const TIME_LEN: usize = 24;
const DEFAULT_CORR_LEN: usize = 12;
const DEFAULT_ACTION_LEN: usize = 10;
const DEFAULT_USER_LEN: usize = 12;
const DEFAULT_PROJ_LEN: usize = 8;

const ERR_PREFIX: &str = "Error: ";

#[derive(Serialize, Debug)]
pub struct ErrorMap {
    data: HashMap<String, ErrorSummary>,
}

impl RawOutput for ErrorMap {
    fn new(_args: RawArgs) -> ErrorMap {
        ErrorMap {
            data: HashMap::default(),
        }
    }

    fn insert_event<'a, T: Event>(&mut self, event: T) {
        if let Some(err) = event.error() {
            let (_key, entry) = self
                .data
                .raw_entry_mut()
                .from_key(err.as_ref())
                .or_insert_with(|| (err.to_string(), ErrorSummary::new()));

            entry.push(&event);
        }
    }

    fn coalesce(&mut self, local_map: ErrorMap) {
        for (err, local_data) in local_map.data {
            let main_entry = self.data.entry(err).or_insert_with(ErrorSummary::new);
            main_entry.events.extend(local_data.events);
            main_entry.backtraces.extend(local_data.backtraces);
        }
    }

    fn finalize(&mut self) {}
}

impl FilterOutput for ErrorMap {
    fn filter(mut self, term: Option<&str>) -> ErrorMap {
        if let Some(s) = term {
            self.data
                .retain(|key, _| key.to_ascii_lowercase().contains(s));
        }

        self
    }
}

impl ErrorMap {
    pub fn sort(&mut self) {
        self.data.values_mut().for_each(|e| e.sort());
    }

    pub fn print(
        &self,
        out: impl Write,
        width: usize,
        color: bool,
        no_border: bool,
        fmt: PrintFormat,
    ) -> io::Result<()> {
        match fmt {
            PrintFormat::Json => self.print_json(out),
            _ => self.print_text(out, width, color, no_border),
        }
    }

    fn print_json(&self, mut out: impl Write) -> io::Result<()> {
        let serialized = serde_json::to_string(&self).expect("failed to serialize error data");
        writeln!(out, "{}", serialized)
    }

    fn print_text(
        &self,
        mut out: impl Write,
        width: usize,
        color: bool,
        no_border: bool,
    ) -> io::Result<()> {
        let mut sorted: Vec<_> = self.data.iter().collect();
        sorted.sort_by(|x, y| y.1.events.len().cmp(&x.1.events.len()));

        let mut iter = sorted.into_iter().peekable();
        while let Some((error, summary)) = iter.next() {
            let mut lines = Vec::new();

            let mut error_out = if color {
                format!("{BLUE}{ERR_PREFIX}{RESET}")
            } else {
                ERR_PREFIX.to_string()
            };

            for (i, line) in error.lines().enumerate() {
                if line.len() <= width {
                    error_out.push_str(line);
                    continue;
                }

                let prefix_len = if i == 0 { ERR_PREFIX.len() } else { 0 };
                let folded = ErrorMap::fold_lines(line, width, prefix_len);
                error_out.push_str(&folded);
            }

            lines.push(error_out);

            lines.extend_from_slice(&summary.text_vec(color));

            let mut joined = match no_border {
                true => crate::print::wrap::bare(&lines),
                false => crate::print::wrap::wrap_lines(&lines),
            };

            if iter.peek().is_some() {
                joined.push('\n');
            }

            writeln!(out, "{}", joined)?;
        }

        Ok(())
    }

    fn fold_lines(line: &str, width: usize, prefix_len: usize) -> String {
        let mut out = String::new();

        let mut slice = line;

        // Shorten first line to make space for ERR_PREFIX
        let mut len = width - prefix_len;

        while let Some((i, _)) = slice.char_indices().take(len).last() {
            out.push_str(&slice[..=i]);
            if i == slice.len() - 1 {
                break;
            }

            out.push('\n');
            slice = &slice[i + 1..];
            len = width;
        }

        out
    }
}

#[derive(Serialize, Debug)]
pub struct ErrorEvent {
    time: String,
    action: String,
    correlation_id: Option<String>,
    project: Option<String>,
    user: Option<String>,
}

impl ErrorEvent {
    pub fn from_event(event: &impl Event) -> ErrorEvent {
        ErrorEvent {
            time: event.raw_time().to_string(),
            correlation_id: event.correlation_id().map(String::from),
            action: event.action().to_string(),
            project: event.project().map(String::from),
            user: event.user().map(String::from),
        }
    }

    pub fn text(
        &self,
        color: bool,
        corr_len: usize,
        action_len: usize,
        user_len: usize,
        proj_len: usize,
    ) -> String {
        if color {
            format!(
                "{GREEN}{:t_len$}{RESET}  {MAGENTA}{:<c_len$}{RESET}  {BLUE}{:<a_len$}{RESET}  {YELLOW}{:<u_len$}{RESET}  {CYAN}{:<p_len$}{RESET}",
                self.time,
                self.correlation_id.as_deref().unwrap_or("-"),
                self.action,
                self.user.as_deref().unwrap_or("-"),
                self.project.as_deref().unwrap_or("-"),
                t_len = TIME_LEN,
                c_len = corr_len,
                a_len = action_len,
                u_len = user_len,
                p_len = proj_len,
            )
        } else {
            format!(
                "{:t_len$}  {:<c_len$}  {:<a_len$}  {:<u_len$}  {:<p_len$}",
                self.time,
                self.correlation_id.as_deref().unwrap_or("-"),
                self.action,
                self.user.as_deref().unwrap_or("-"),
                self.project.as_deref().unwrap_or("-"),
                t_len = TIME_LEN,
                c_len = corr_len,
                a_len = action_len,
                u_len = user_len,
                p_len = proj_len,
            )
        }
    }
}

#[derive(Serialize, Debug)]
pub struct ErrorSummary {
    events: Vec<ErrorEvent>,
    backtraces: HashSet<String>,
}

impl ErrorSummary {
    pub fn new() -> ErrorSummary {
        ErrorSummary {
            events: Vec::new(),
            backtraces: HashSet::new(),
        }
    }

    pub fn push(&mut self, event: &impl Event) {
        self.events.push(ErrorEvent::from_event(event));

        if let Some(bt) = event.backtrace(BACKTRACE_LEN, 2) {
            self.backtraces.insert(bt);
        }
    }

    pub fn sort(&mut self) {
        self.events.par_sort_by(|x, y| x.time.cmp(&y.time));
    }

    pub fn text_vec(&self, color: bool) -> Vec<String> {
        let corr_len = self.field_len(|e| e.correlation_id.as_deref(), DEFAULT_CORR_LEN);
        let action_len = self.field_len(|e| Some(&e.action), DEFAULT_ACTION_LEN);
        let user_len = self.field_len(|e| e.user.as_deref(), DEFAULT_USER_LEN);
        let proj_len = self.field_len(|e| e.project.as_deref(), DEFAULT_PROJ_LEN);

        let mut out = Vec::new();
        if color {
            let ct = format!("{BLUE}Count{RESET}: {}", self.events.len());
            out.push(ct);
        } else {
            out.push(format!("Count: {}", self.events.len()))
        };

        match self.backtraces.len() {
            0 => {}
            1 => {
                let bt = if color {
                    format!(
                        "{}Backtrace{}:\n{}",
                        BLUE,
                        RESET,
                        self.backtraces.iter().next().unwrap()
                    )
                } else {
                    format!("Backtrace:\n{}", self.backtraces.iter().next().unwrap())
                };
                out.push(bt);
            }
            _ => {
                if color {
                    out.push(format!(
                        "{}Backtrace{}:\n{}\n",
                        BLUE,
                        RESET,
                        self.backtraces.iter().next().unwrap()
                    ));
                } else {
                    out.push(format!(
                        "Backtrace:\n{}\n",
                        self.backtraces.iter().next().unwrap()
                    ));
                }

                for bt in self.backtraces.iter().skip(1) {
                    out.push(bt.clone());
                }
            }
        };

        let mut event_str = String::new();
        let event_title = match self.events.len() {
            0..=10 => {
                if color {
                    format!("{}Events{}:", BLUE, RESET)
                } else {
                    "Events:".to_string()
                }
            }
            _ => {
                if color {
                    format!("{}Last 10 Events{}:", BLUE, RESET)
                } else {
                    "Last 10 Events:".to_string()
                }
            }
        };

        event_str.push_str(&event_title);
        event_str.push_str("\n  ");
        event_str.push_str(&self.header(color, corr_len, action_len, user_len, proj_len));

        let mut iter = self.events.iter().rev().take(10).rev().peekable();
        while let Some(event) = iter.next() {
            event_str.push_str("  ");
            event_str.push_str(&event.text(color, corr_len, action_len, user_len, proj_len));
            if iter.peek().is_some() {
                event_str.push('\n');
            }
        }

        out.push(event_str);

        out
    }

    pub fn header(
        &self,
        color: bool,
        corr_len: usize,
        action_len: usize,
        user_len: usize,
        proj_len: usize,
    ) -> String {
        if color {
            format!(
                "{}{:<t_len$}{}  {}{:<c_len$}{}  {}{:<a_len$}{}  {}{:<u_len$}{}  {}{:<p_len$}{}\n",
                UNDERLINE,
                "TIME",
                RESET,
                UNDERLINE,
                "CORR_ID",
                RESET,
                UNDERLINE,
                "ACTION",
                RESET,
                UNDERLINE,
                "USER",
                RESET,
                UNDERLINE,
                "PROJECT",
                RESET,
                t_len = TIME_LEN,
                c_len = corr_len,
                a_len = action_len,
                u_len = user_len,
                p_len = proj_len
            )
        } else {
            format!(
                "{:<t_len$}  {:<c_len$}  {:<a_len$}  {:<u_len$}  {:<p_len$}\n",
                "TIME",
                "CORR_ID",
                "ACTION",
                "USER",
                "PROJECT",
                t_len = TIME_LEN,
                c_len = corr_len,
                a_len = action_len,
                u_len = user_len,
                p_len = proj_len
            )
        }
    }

    fn field_len(&self, f: impl Fn(&ErrorEvent) -> Option<&str>, default_len: usize) -> usize {
        self.events
            .iter()
            .map(|e| f(e).unwrap_or_default().len())
            .max()
            .unwrap_or(default_len)
            .max(default_len)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::data::RawArgs;
    use crate::event::ProdJsonDurSEventError;
    use crate::log_type::LogType;

    #[test]
    fn fold_error() {
        let input = r#"{"method":"GET","path":"/search/count","format":"json","controller":"SearchController","action":"count","status":500,"time":"2022-02-03T18:12:31.854Z","params":[{"key":"group_id","value":"2011"},{"key":"project_id","value":"2368"},{"key":"repository_ref","value":"master"},{"key":"scope","value":"milestones"},{"key":"search","value":"[FILTERED]"},{"key":"snippets","value":"false"}],"remote_ip":"10.0.0.1","user_id":1,"username":"myuser","ua":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36","correlation_id":"01FV0CKDC663C4XSEN0JJ6MQ7W","meta.user":"myuser","meta.caller_id":"SearchController#count","meta.remote_ip":"10.0.0.1","meta.feature_category":"global_search","meta.client_id":"user/1","meta.search.group_id":"2011","meta.search.project_id":"2368","meta.search.scope":"milestones","meta.search.filters.confidential":null,"meta.search.filters.state":null,"meta.search.force_search_results":null,"redis_calls":2,"redis_duration_s":0.004817999999999999,"redis_read_bytes":543,"redis_write_bytes":836,"redis_cache_calls":1,"redis_cache_duration_s":0.00323,"redis_cache_read_bytes":202,"redis_cache_write_bytes":72,"redis_shared_state_calls":1,"redis_shared_state_duration_s":0.001588,"redis_shared_state_read_bytes":341,"redis_shared_state_write_bytes":764,"elasticsearch_calls":1,"elasticsearch_duration_s":0.047696,"elasticsearch_timed_out_count":0,"db_count":9,"db_write_count":3,"db_cached_count":0,"cpu_s":0.038985,"mem_objects":12620,"mem_bytes":1392936,"mem_mallocs":3761,"mem_total_bytes":1897736,"pid":4259,"exception.class":"Elasticsearch::Transport::Transport::Errors::BadRequest","exception.message":"[400] {\"error\":{\"root_cause\":[{\"type\":\"query_shard_exception\",\"reason\":\"[has_parent] no join field has been configured\",\"index_uuid\":\"aaaaaaa-aaaaaaaaaaaaaa\",\"index\":\"gitlab-production\"}],\"type\":\"search_phase_execution_exception\",\"reason\":\"all shards failed\",\"phase\":\"query\",\"grouped\":true,\"failed_shards\":[{\"shard\":1,\"index\":\"gitlab-production\",\"node\":\"aaaaaaaaaaaaa_aaaaaaaa\",\"reason\":{\"type\":\"query_shard_exception\",\"reason\":\"[has_parent] no join field has been configured\",\"index_uuid\":\"aaaaaaa-aaaaaaaaaaaaaa\",\"index\":\"gitlab-production\"}}]},\"status\":400}","exception.backtrace":["lib/gitlab/instrumentation/elasticsearch_transport.rb:12:in `perform_request'","config/initializers/elastic_client_setup.rb:37:in `total'","ee/lib/gitlab/elastic/search_results.rb:153:in `milestones_count'","ee/lib/gitlab/elastic/search_results.rb:89:in `formatted_count'","app/controllers/search_controller.rb:58:in `block in count'","app/models/application_record.rb:61:in `block (2 levels) in with_fast_read_statement_timeout'","lib/gitlab/database.rb:260:in `block in transaction'","lib/gitlab/database.rb:259:in `transaction'","app/models/application_record.rb:58:in `block in with_fast_read_statement_timeout'","lib/gitlab/database/load_balancing/session.rb:95:in `fallback_to_replicas_for_ambiguous_queries'","app/models/application_record.rb:57:in `with_fast_read_statement_timeout'","app/controllers/search_controller.rb:57:in `count'","app/controllers/application_controller.rb:536:in `block in allow_gitaly_ref_name_caching'","lib/gitlab/gitaly_client.rb:341:in `allow_ref_name_caching'","app/controllers/application_controller.rb:535:in `allow_gitaly_ref_name_caching'","ee/lib/gitlab/ip_address_state.rb:10:in `with'","ee/app/controllers/ee/application_controller.rb:44:in `set_current_ip_address'","app/controllers/application_controller.rb:485:in `set_current_admin'","lib/gitlab/session.rb:11:in `with_session'","app/controllers/application_controller.rb:476:in `set_session_storage'","lib/gitlab/i18n.rb:105:in `with_locale'","lib/gitlab/i18n.rb:111:in `with_user_locale'","app/controllers/application_controller.rb:470:in `set_locale'","app/controllers/application_controller.rb:464:in `set_current_context'","lib/gitlab/middleware/speedscope.rb:13:in `call'","lib/gitlab/request_profiler/middleware.rb:17:in `call'","lib/gitlab/jira/middleware.rb:19:in `call'","lib/gitlab/middleware/go.rb:20:in `call'","lib/gitlab/etag_caching/middleware.rb:21:in `call'","lib/gitlab/middleware/multipart.rb:172:in `call'","lib/gitlab/middleware/read_only/controller.rb:50:in `call'","lib/gitlab/middleware/read_only.rb:18:in `call'","lib/gitlab/middleware/same_site_cookies.rb:27:in `call'","lib/gitlab/middleware/handle_malformed_strings.rb:21:in `call'","lib/gitlab/middleware/basic_health_check.rb:25:in `call'","lib/gitlab/middleware/handle_ip_spoof_attack_error.rb:25:in `call'","lib/gitlab/middleware/request_context.rb:21:in `call'","config/initializers/fix_local_cache_middleware.rb:11:in `call'","lib/gitlab/middleware/rack_multipart_tempfile_factory.rb:19:in `call'","lib/gitlab/middleware/sidekiq_web_static.rb:20:in `call'","lib/gitlab/metrics/requests_rack_middleware.rb:74:in `call'","lib/gitlab/middleware/release_env.rb:12:in `call'"],"db_duration_s":0.01333,"view_duration_s":0.0,"duration_s":0.09665}"#;
        let event = ProdJsonDurSEventError::parse(input).unwrap();

        let mut errors = ErrorMap::new(RawArgs::new(LogType::ProdJsonDurS, None));
        errors.insert_event(event);

        assert_eq!(1, errors.data.len());
        let err = errors.data.keys().next().unwrap();

        let folded = r#"Elasticsearch::Trans
port::Transport::Err
ors::BadRequest [400
] {"error":{"root_ca
use":[{"type":"query
_shard_exception","r
eason":"[has_parent]
 no join field has b
een configured","ind
ex_uuid":"aaaaaaa-aa
aaaaaaaaaaaa","index
":"gitlab-production
"}],"type":"search_p
hase_execution_excep
tion","reason":"all 
shards failed","phas
e":"query","grouped"
:true,"failed_shards
":[{"shard":1,"index
":"gitlab-production
","node":"aaaaaaaaaa
aaa_aaaaaaaa","reaso
n":{"type":"query_sh
ard_exception","reas
on":"[has_parent] no
 join field has been
 configured","index_
uuid":"aaaaaaa-aaaaa
aaaaaaaaa","index":"
gitlab-production"}}
]},"status":400}"#;
        assert_eq!(folded, ErrorMap::fold_lines(err, 20, 0));
    }
}
