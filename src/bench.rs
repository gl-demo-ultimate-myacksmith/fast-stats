use crate::stats::StatsType;
use crate::stats_vec::StatsVec;

use std::io::Read;

const COMPRESSED_DATA: &[(&str, &[u8])] = &include!(concat!(env!("OUT_DIR"), "/lz4_bench.rs"));

pub fn bench_stats(
    version_str: &str,
    stats_t: StatsType,
) -> Option<Result<StatsVec, Box<dyn std::error::Error>>> {
    let bench_name = version_str.replace('.', "_") + "_" + &stats_t.to_string();

    COMPRESSED_DATA
        .iter()
        .find(|(ver, _)| &bench_name.as_str() == ver)
        .map(|(_ver, compressed)| decompress(compressed))
}

fn decompress(data: &[u8]) -> Result<StatsVec, Box<dyn std::error::Error>> {
    let mut decompressor = lz4::Decoder::new(data)?;

    let mut encoded = Vec::with_capacity(data.len());
    decompressor.read_to_end(&mut encoded)?;

    let stats = bincode::deserialize(&encoded)?;
    Ok(stats)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn all_benchmarks_deserialize() {
        for (ver, data) in COMPRESSED_DATA.iter() {
            assert!(decompress(data).is_ok(), "Failed to deserialize {}", ver);
        }
    }
}
