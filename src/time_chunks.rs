use crate::data::detail::{DetailTimeEvent, DetailTimeMap};
use crate::data::raw::RawDataMap;
use crate::data::BasicData;
use crate::data::{FilterOutput, RawArgs, RawOutput};
use crate::event::basic::BasicEvent;
use crate::event::Event;
use crate::field::{EventField, StatField};
use crate::log_type::LogType;
use crate::stats_vec::{PrintFormat, StatsVec, VerboseStatsVec};
use crate::top::{TopSummary, ValueFmt};
use crate::util::{self, BRIGHT_GREEN, RESET};
use crate::HashMap;

use chrono::{prelude::*, Duration, DurationRound};
use rayon::prelude::*;
use std::collections::BTreeMap;
use std::convert::TryFrom;
use std::fmt;
use std::io::{self, Write};
use std::str::FromStr;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Interval {
    length: u64,
    unit: TimeUnit,
}

impl Interval {
    pub fn new(length: u64, unit: TimeUnit) -> Interval {
        Interval { length, unit }
    }

    pub fn new_secs(length: u64) -> Interval {
        Interval {
            length,
            unit: TimeUnit::Second,
        }
    }

    pub fn seconds(&self) -> i64 {
        (self.length as i64) * self.unit.seconds()
    }

    pub fn as_duration(&self) -> Duration {
        Duration::seconds(self.seconds())
    }
}

impl FromStr for Interval {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::try_from(s)
    }
}

impl TryFrom<&str> for Interval {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let err_msg = "WINDOW_LEN must be in <LEN>[h|m|s] format where LEN is a positive 64-bit integer, ex. 30h or 1h";

        if s.len() < 2 || !s.is_ascii() {
            return Err(err_msg);
        }

        let (ct_s, unit_s) = s.split_at(s.len() - 1);

        let unit = TimeUnit::try_from(unit_s)?;

        let ct = ct_s.parse::<u64>().map_err(|_| err_msg)?;

        Ok(Interval::new(ct, unit))
    }
}

impl fmt::Display for Interval {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.length, self.unit)
    }
}

impl Default for Interval {
    fn default() -> Interval {
        Interval {
            length: 5,
            unit: TimeUnit::Minute,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum TimeUnit {
    Hour,
    Minute,
    Second,
}

impl TimeUnit {
    pub fn seconds(&self) -> i64 {
        use TimeUnit as Tu;
        match self {
            Tu::Hour => 3_600,
            Tu::Minute => 60,
            Tu::Second => 1,
        }
    }
}

impl fmt::Display for TimeUnit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use TimeUnit as Tu;
        let s = match self {
            Tu::Hour => "hour",
            Tu::Minute => "minute",
            Tu::Second => "second",
        };
        write!(f, "{}", s)
    }
}

impl TryFrom<&str> for TimeUnit {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        use TimeUnit as Tu;
        match s {
            "h" | "H" => Ok(Tu::Hour),
            "m" | "M" => Ok(Tu::Minute),
            "s" | "S" => Ok(Tu::Second),
            _ => Err("Time unit must be (h)ours, (m)inutes, or (s)econds"),
        }
    }
}

/// A hashmap keyed on unix timestamps.
/// Used for splitting parsed events into buckets by time.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct UnsortedTimeMap<T: RawOutput> {
    pub data: HashMap<i64, T>,
    pub interval: Interval,
    pub interval_dur: Duration,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub log_t: LogType,
}

impl<T: BasicData> BasicData for UnsortedTimeMap<T> {
    fn insert_basic(&mut self, event: BasicEvent) {
        // Don't count info events towards totals
        if event.status.is_info() {
            return;
        }

        if let Some(ts) = event.parse_time() {
            self.update_time(&ts);

            let idx = match ts.duration_trunc(self.interval_dur) {
                Ok(rnd) => rnd.timestamp(),
                Err(_) => return,
            };

            let log_t = self.log_t; // Copy so we don't hold a ref to self
            let time_map = self.data.entry(idx).or_insert_with(|| T::new(log_t.into()));

            time_map.insert_basic(event);
        }
    }
}

impl<T: RawOutput> RawOutput for UnsortedTimeMap<T> {
    fn new(args: RawArgs) -> UnsortedTimeMap<T> {
        UnsortedTimeMap {
            data: HashMap::default(),
            interval: args.interval.unwrap_or_default(),
            interval_dur: args.interval.unwrap_or_default().as_duration(),
            start_time: chrono::DateTime::<FixedOffset>::MAX_UTC.into(),
            end_time: chrono::DateTime::<FixedOffset>::MIN_UTC.into(),
            log_t: args.log_t,
        }
    }

    fn insert_event<'a, E: Event>(&mut self, event: E) {
        // Don't count info events towards totals
        if event.status().is_info() {
            return;
        }

        if let Some(ts) = E::parse_time(event.raw_time()) {
            self.update_time(&ts);

            let idx = match ts.duration_trunc(self.interval_dur) {
                Ok(rnd) => rnd.timestamp(),
                Err(_) => return,
            };

            let log_t = self.log_t; // Copy so we don't hold a ref to self
            let time_map = self.data.entry(idx).or_insert_with(|| T::new(log_t.into()));

            time_map.insert_event(event);
        }
    }

    fn coalesce(&mut self, other_map: Self) {
        let log_t = self.log_t; // Copy so we don't hold a ref to self

        for (idx, other_data) in other_map.data {
            let time_map = self.data.entry(idx).or_insert_with(|| T::new(log_t.into()));

            time_map.coalesce(other_data);
        }

        self.start_time = self.start_time.min(other_map.start_time);
        self.end_time = self.end_time.max(other_map.end_time);
    }

    fn finalize(&mut self) {
        for item in self.data.values_mut() {
            item.finalize();
        }
    }
}

impl<T: RawOutput> UnsortedTimeMap<T> {
    pub fn with_map(
        data: HashMap<i64, T>,
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
        log_t: LogType,
    ) -> UnsortedTimeMap<T> {
        UnsortedTimeMap {
            data,
            interval,
            interval_dur: interval.as_duration(),
            start_time,
            end_time,
            log_t,
        }
    }

    pub fn attr(
        &self,
    ) -> (
        Interval,
        DateTime<FixedOffset>,
        DateTime<FixedOffset>,
        LogType,
    ) {
        (self.interval, self.start_time, self.end_time, self.log_t)
    }

    pub fn total_dur(&self) -> Duration {
        self.end_time - self.start_time
    }

    fn update_time(&mut self, time: &DateTime<FixedOffset>) {
        if time < &self.start_time {
            self.start_time = *time;
        }

        if time > &self.end_time {
            self.end_time = *time;
        }
    }

    pub fn into_sorted(self) -> SortedTimeMap<T> {
        let (interval, start_time, end_time, _) = self.attr();
        let tz = self.start_time.offset();

        let time_map: BTreeMap<_, _> = self
            .data
            .into_par_iter()
            .map(|(ts, data)| {
                let time = tz.timestamp_opt(ts, 0).unwrap();

                (time, data)
            })
            .collect();
        SortedTimeMap::with_map(time_map, interval, start_time, end_time)
    }
}

impl UnsortedTimeMap<RawDataMap> {
    pub fn into_sorted_stats_vec(self, sort_by: StatField) -> SortedTimeMap<StatsVec> {
        let (interval, start_time, end_time, _) = self.attr();

        let interval_dur = self.interval_dur;
        let stats_t = self.log_t.stats_t();
        let tz = self.start_time.offset();

        let time_map: BTreeMap<_, _> = self
            .data
            .into_par_iter()
            .map(|(ts, data)| {
                let time = tz.timestamp_opt(ts, 0).unwrap();

                let stats_v = StatsVec::from_raw_data(data, interval_dur, stats_t).sort_by(sort_by);
                (time, stats_v)
            })
            .collect();

        SortedTimeMap::with_map(time_map, interval, start_time, end_time)
    }
}

impl<T: FilterOutput> FilterOutput for UnsortedTimeMap<T> {
    fn filter(self, term: Option<&str>) -> UnsortedTimeMap<T> {
        if term.is_some() {
            let (interval, start_time, end_time, log_t) = self.attr();
            let filtered: HashMap<_, _> = self
                .data
                .into_par_iter()
                .map(|(ts, stats)| (ts, stats.filter(term)))
                .collect();

            return UnsortedTimeMap::with_map(filtered, interval, start_time, end_time, log_t);
        }

        self
    }
}

impl<T: RawOutput> UnsortedTimeMap<T> {
    // We use the smallest interval possible during the initial chunking as we don't
    // know the total duration yet. Once we have that, we can combine the buckets into
    // the required size. This requires us to make the target interval a multiple
    // of the default.
    pub fn adjust_interval(mut self, target_interval: Interval) -> UnsortedTimeMap<T> {
        let ratio = target_interval.seconds() as f64 / self.interval.seconds() as f64;

        if ratio.round() <= 1.0 {
            return self;
        }
        let new_interval = ratio.round() as i64 * self.interval.seconds();

        let mut out = UnsortedTimeMap::new(RawArgs::new(
            self.log_t,
            Some(Interval::new_secs(new_interval as u64)),
        ));
        out.start_time = self.start_time;
        out.end_time = self.end_time;

        let log_t = self.log_t;

        for (ts, data) in self.data.drain() {
            // Find next ts down that divides evenly by interval
            let join_ts = if ts % new_interval == 0 {
                ts
            } else {
                ts / new_interval * new_interval
            };

            let entry = out
                .data
                .entry(join_ts)
                .or_insert_with(|| T::new(log_t.into()));
            entry.coalesce(data);
        }

        out
    }
}

/// A BTreeMap keyed on DateTime<FixedOffset>.
/// Data should be collected into UnsortedTimeMap<T> first for fast insertion.
/// Used for printing data sorted by time when `--interval` is passed.
#[derive(PartialEq, Eq, Debug)]
pub struct SortedTimeMap<T> {
    pub data: BTreeMap<DateTime<FixedOffset>, T>,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub interval: Interval,
}

impl<T> SortedTimeMap<T> {
    pub fn with_map(
        data: BTreeMap<DateTime<FixedOffset>, T>,
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
    ) -> SortedTimeMap<T> {
        SortedTimeMap {
            data,
            interval,
            start_time,
            end_time,
        }
    }
}

impl SortedTimeMap<StatsVec> {
    pub fn print(
        &self,
        mut out: impl Write,
        format: PrintFormat,
        fields: &[StatField],
        limit: Option<usize>,
        color: bool,
    ) -> io::Result<()> {
        if matches!(format, PrintFormat::Text) {
            writeln!(
                out,
                "** Splitting input into {} increments **",
                self.interval
            )?;
            writeln!(
                out,
                "\nFirst event: {}\nLast event:  {}\n",
                self.start_time.naive_local(),
                self.end_time.naive_local(),
            )?;
        }

        for (time, stats) in &self.data {
            if color {
                writeln!(
                    &mut out,
                    "\n   {}>>> {} <<<{}\n",
                    BRIGHT_GREEN,
                    time.naive_local(),
                    RESET
                )?;
            } else {
                writeln!(&mut out, "\n   >>> {} <<<\n", time.naive_local())?;
            }

            stats.print(
                &mut out,
                format,
                fields,
                limit,
                self.max_col_len(limit),
                None,
            )?;
        }
        Ok(())
    }

    fn max_col_len(&self, limit: Option<usize>) -> Option<usize> {
        self.data.values().map(|v| v.primary_col_len(limit)).max()
    }
}

impl<T: TopSummary> SortedTimeMap<T> {
    pub fn print(
        &self,
        mut out: impl Write,
        sort_by: T::Field,
        display: ValueFmt,
        format: PrintFormat,
        limit: Option<usize>,
        color: bool,
    ) -> io::Result<()> {
        if matches!(format, PrintFormat::Text) {
            writeln!(
                out,
                "** Splitting input into {} increments **",
                self.interval
            )?;
            writeln!(
                out,
                "\nFirst event: {}\nLast event:  {}\n",
                self.start_time.naive_local(),
                self.end_time.naive_local(),
            )?;
        }

        for (time, stats) in &self.data {
            if color {
                writeln!(
                    &mut out,
                    "\n   {}>>> {} <<<{}\n",
                    BRIGHT_GREEN,
                    time.naive_local(),
                    RESET
                )?;
            } else {
                writeln!(&mut out, "\n   >>> {} <<<\n", time.naive_local())?;
            }

            stats.print(&mut out, sort_by, display, format, limit)?;
        }
        Ok(())
    }
}

#[cfg(feature = "plot")]
impl<T: crate::plot::PlotStats> SortedTimeMap<T> {
    pub fn sort_stats(&mut self) {
        self.data.par_iter_mut().for_each(|(_, stats)| {
            stats.sort();
        });
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct DetailEventTimeChunks {
    pub data: HashMap<i64, DetailTimeMap>,
    pub interval: Interval,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub seconds: i64,
    pub log_t: LogType,
}

impl DetailEventTimeChunks {
    pub fn new(
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
        log_t: LogType,
    ) -> DetailEventTimeChunks {
        DetailEventTimeChunks {
            data: HashMap::default(),
            interval,
            start_time,
            end_time,
            seconds: interval.seconds(),
            log_t,
        }
    }

    pub fn from_time_data(
        data_map: DetailTimeMap,
        interval: Interval,
        log_t: LogType,
        sort_by: EventField,
    ) -> DetailEventTimeChunks {
        let mut chunks =
            DetailEventTimeChunks::new(interval, data_map.start_time, data_map.end_time, log_t);

        for (action, events) in data_map.data.into_iter() {
            for event in events {
                chunks.insert(&action, event);
            }
        }

        use EventField as Ef;
        chunks.data.par_iter_mut().for_each(|(_idx, chunk)| {
            chunk.data.iter_mut().for_each(|(_action, items)| {
                items.sort_unstable_by(|x, y| match sort_by {
                    Ef::Duration => util::cmp(x.dur, y.dur),
                    Ef::Db => util::cmp(x.db, y.db),
                    Ef::Redis => util::cmp(x.redis, y.redis),
                    Ef::Gitaly => util::cmp(x.gitaly, y.gitaly),
                    Ef::Rugged => util::cmp(x.rugged, y.rugged),
                    Ef::CpuS => util::cmp(x.cpu_s, y.cpu_s),
                    Ef::Queue => util::cmp(x.queue, y.queue),
                    _ => std::cmp::Ordering::Equal, // Don't sort.
                })
            })
        });
        chunks
    }

    pub fn into_verbose(self, sort_by: StatField, color: bool) -> TimeVerboseStatsMap {
        let mut verbose = crate::time_chunks::TimeVerboseStatsMap::new(
            self.interval,
            self.start_time,
            self.end_time,
        );

        for (idx, mut chunk) in self.data.into_iter() {
            let offset = self.seconds * idx;
            let time = self.start_time + Duration::seconds(offset);
            chunk.set_times(time, time + Duration::seconds(self.interval.seconds()));

            let stats = chunk.into_verbose_stats_vec(self.log_t.stats_t(), sort_by, color);

            verbose.data.insert(time, stats);
        }

        verbose
    }

    #[inline]
    fn calc_idx(&self, time: DateTime<FixedOffset>) -> i64 {
        let diff = time - self.start_time;
        diff.num_seconds() / self.seconds
    }

    pub fn insert(&mut self, action: &str, event: DetailTimeEvent) {
        let log_t = self.log_t;

        let idx = self.calc_idx(event.time);
        let time_entry = self
            .data
            .entry(idx)
            .or_insert_with(|| DetailTimeMap::new(log_t.into()));
        let action_entry = time_entry
            .data
            .entry(action.to_string())
            .or_insert_with(Vec::new);
        action_entry.push(event);
    }
}

#[derive(Debug)]
pub struct TimeVerboseStatsMap {
    pub data: BTreeMap<DateTime<FixedOffset>, VerboseStatsVec>,
    pub start_time: DateTime<FixedOffset>,
    pub end_time: DateTime<FixedOffset>,
    pub interval: Interval,
}

impl TimeVerboseStatsMap {
    pub fn new(
        interval: Interval,
        start_time: DateTime<FixedOffset>,
        end_time: DateTime<FixedOffset>,
    ) -> Self {
        TimeVerboseStatsMap {
            data: BTreeMap::new(),
            interval,
            start_time,
            end_time,
        }
    }

    pub fn print(
        &self,
        mut out: impl Write,
        fmt: PrintFormat,
        fields: &[StatField],
        limit: Option<usize>,
        color: bool,
    ) -> io::Result<()> {
        if matches!(fmt, PrintFormat::Text) {
            writeln!(
                out,
                "** Splitting input into {} increments **",
                self.interval
            )?;
            writeln!(
                out,
                "\nStart time: {}\nEnd time:   {}\n",
                self.start_time.naive_local(),
                self.end_time.naive_local(),
            )?;
        }

        for (time, stats) in &self.data {
            if color {
                writeln!(
                    &mut out,
                    "\n   {}>>> {} <<<{}\n",
                    BRIGHT_GREEN,
                    time.naive_local(),
                    RESET
                )?;
            } else {
                writeln!(&mut out, "\n   >>> {} <<<\n", time.naive_local())?;
            }

            stats.print(&mut out, fmt, fields, limit)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::execute::DynBatcher;
    use crate::field::StatField;
    use crate::parse::ParseArgs;
    use crate::plot::{GitalyPlot, PlotStats};

    use float_cmp::approx_eq;
    use std::fs::File;

    #[test]
    fn calc_time_st() {
        let input_file = File::open("tests/data/prod.log").unwrap();
        let batcher = DynBatcher::new(50, Box::new(input_file));
        let args = ParseArgs::new(None, 1, Vec::new());
        let int = Interval::new(10, TimeUnit::Minute);

        let time_stats = LogType::ProdJsonDurS
            .calculate_unsorted_time_stats(int, batcher, args)
            .into_sorted_stats_vec(StatField::Count);

        assert_eq!(time_stats.interval, Interval::new(10, TimeUnit::Minute));
        assert_eq!(time_stats.data.len(), 2);

        let first_time = DateTime::parse_from_rfc3339("2020-04-24T07:20:08.004Z")
            .unwrap()
            .duration_trunc(int.as_duration())
            .unwrap();
        let first_stats_v = time_stats.data.get(&first_time).unwrap();
        let last_time = DateTime::parse_from_rfc3339("2020-04-24T07:30:08.004Z")
            .unwrap()
            .duration_trunc(int.as_duration())
            .unwrap();
        let last_stats_v = time_stats.data.get(&last_time).unwrap();

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec![
                "Projects::BlobController#show",
                "Projects::TreeController#show",
            ]
        );

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| s.count)
                .collect::<Vec<_>>(),
            vec![4, 1]
        );

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| s.max)
                .collect::<Vec<_>>(),
            vec![9980.00, 9980.0]
        );

        assert_eq!(
            last_stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec![
                "Projects::BlobController#show",
                "Projects::TreeController#show",
                "Groups::MilestonesController#index",
            ]
        );

        assert_eq!(
            last_stats_v
                .stats
                .iter()
                .map(|s| s.count)
                .collect::<Vec<_>>(),
            vec![3, 2, 1]
        );

        assert_eq!(
            last_stats_v.stats.iter().map(|s| s.max).collect::<Vec<_>>(),
            vec![9980.00, 9980.0, 290.0]
        );
    }

    #[test]
    fn calc_time_mt() {
        let input_file = File::open("tests/data/prod.log").unwrap();
        let batcher = DynBatcher::new(50, Box::new(input_file));
        let args = ParseArgs::new(None, 4, Vec::new());
        let int = Interval::new(10, TimeUnit::Minute);

        let time_stats = LogType::ProdJsonDurS
            .calculate_unsorted_time_stats(int, batcher, args)
            .into_sorted_stats_vec(StatField::Count);

        assert_eq!(time_stats.interval, Interval::new(10, TimeUnit::Minute));
        assert_eq!(time_stats.data.len(), 2);

        let first_time = DateTime::parse_from_rfc3339("2020-04-24T07:20:08.004Z")
            .unwrap()
            .duration_trunc(int.as_duration())
            .unwrap();
        let first_stats_v = time_stats.data.get(&first_time).unwrap();
        let last_time = DateTime::parse_from_rfc3339("2020-04-24T07:30:08.004Z")
            .unwrap()
            .duration_trunc(int.as_duration())
            .unwrap();
        let last_stats_v = time_stats.data.get(&last_time).unwrap();

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec![
                "Projects::BlobController#show",
                "Projects::TreeController#show",
            ]
        );

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| s.count)
                .collect::<Vec<_>>(),
            vec![4, 1]
        );

        assert_eq!(
            first_stats_v
                .stats
                .iter()
                .map(|s| s.max)
                .collect::<Vec<_>>(),
            vec![9980.00, 9980.0]
        );

        assert_eq!(
            last_stats_v
                .stats
                .iter()
                .map(|s| &s.prim_field)
                .collect::<Vec<_>>(),
            vec![
                "Projects::BlobController#show",
                "Projects::TreeController#show",
                "Groups::MilestonesController#index",
            ]
        );

        assert_eq!(
            last_stats_v
                .stats
                .iter()
                .map(|s| s.count)
                .collect::<Vec<_>>(),
            vec![3, 2, 1]
        );

        assert_eq!(
            last_stats_v.stats.iter().map(|s| s.max).collect::<Vec<_>>(),
            vec![9980.00, 9980.0, 290.0]
        );
    }

    #[test]
    fn calc_time_st_eq_calc_time_mt() {
        let st_input_file = File::open("tests/data/prod.log").unwrap();
        let st_batcher = DynBatcher::new(50, Box::new(st_input_file));
        let st_args = ParseArgs::new(None, 1, Vec::new());
        let st_int = Interval::new(10, TimeUnit::Minute);

        let mt_input_file = File::open("tests/data/prod.log").unwrap();
        let mt_batcher = DynBatcher::new(50, Box::new(mt_input_file));
        let mt_args = ParseArgs::new(None, 4, Vec::new());
        let mt_int = Interval::new(10, TimeUnit::Minute);

        assert_eq!(
            LogType::ProdJsonDurS
                .calculate_unsorted_time_stats(st_int, st_batcher, st_args)
                .into_sorted_stats_vec(StatField::Count),
            LogType::ProdJsonDurS
                .calculate_unsorted_time_stats(mt_int, mt_batcher, mt_args)
                .into_sorted_stats_vec(StatField::Count),
        );
    }

    #[test]
    fn unsorted_map() {
        let input_file = File::open("tests/data/interval.log").unwrap();
        let batcher = DynBatcher::new(50, Box::new(input_file));
        let args = ParseArgs::new(None, 4, Vec::new());
        let int = Interval::new(5, TimeUnit::Second);

        let stats: UnsortedTimeMap<GitalyPlot> =
            LogType::GitalyJsonDetail.calculate_unsorted_time_stats(int, batcher, args);

        let mut stats_ts: Vec<_> = stats.data.keys().collect();

        assert!(approx_eq!(
            f64,
            0.12,
            stats.data.values().flat_map(|v| &v.dur).sum::<f64>()
        ));
        assert_eq!(15, stats.data.values().map(|v| v.len()).sum::<usize>());

        stats_ts.sort();
        assert_eq!(
            vec![
                &1641038400,
                &1641038700,
                &1641039000,
                &1641039300,
                &1641039600,
            ],
            stats_ts,
        );
    }

    #[test]
    fn adjust_interval() {
        let input_file = File::open("tests/data/interval.log").unwrap();
        let batcher = DynBatcher::new(50, Box::new(input_file));
        let args = ParseArgs::new(None, 4, Vec::new());
        let int = Interval::new(5, TimeUnit::Second);

        let stats: UnsortedTimeMap<GitalyPlot> =
            LogType::GitalyJsonDetail.calculate_unsorted_time_stats(int, batcher, args);

        let target_interval = Interval::new(15, TimeUnit::Minute);
        let unadjusted = stats.clone();
        let adjusted = stats.adjust_interval(target_interval);

        let mut adjusted_ts: Vec<_> = adjusted.data.keys().collect();
        adjusted_ts.sort();

        assert_eq!(vec![&1641038400, &1641039300], adjusted_ts,);
        assert_eq!(
            unadjusted.data.values().map(|v| v.len()).sum::<usize>(),
            adjusted.data.values().map(|v| v.len()).sum::<usize>()
        );
    }

    #[test]
    fn sorted_map() {
        let input_file = File::open("tests/data/interval.log").unwrap();
        let batcher = DynBatcher::new(50, Box::new(input_file));
        let args = ParseArgs::new(None, 4, Vec::new());
        let int = Interval::new(5, TimeUnit::Second);

        let unsorted: UnsortedTimeMap<GitalyPlot> =
            LogType::GitalyJsonDetail.calculate_unsorted_time_stats(int, batcher, args);
        let sorted = unsorted.into_sorted();

        assert!(approx_eq!(
            f64,
            0.12,
            sorted.data.values().flat_map(|v| &v.dur).sum::<f64>()
        ));
        assert_eq!(15, sorted.data.values().map(|v| v.len()).sum::<usize>());

        assert_eq!(
            vec![
                &sorted
                    .start_time
                    .offset()
                    .timestamp_opt(1641038400, 0)
                    .unwrap(),
                &sorted
                    .start_time
                    .offset()
                    .timestamp_opt(1641038700, 0)
                    .unwrap(),
                &sorted
                    .start_time
                    .offset()
                    .timestamp_opt(1641039000, 0)
                    .unwrap(),
                &sorted
                    .start_time
                    .offset()
                    .timestamp_opt(1641039300, 0)
                    .unwrap(),
                &sorted
                    .start_time
                    .offset()
                    .timestamp_opt(1641039600, 0)
                    .unwrap(),
            ],
            sorted.data.keys().collect::<Vec<_>>()
        );
    }
}
