use crate::check_type;
use crate::data::raw::RawDataMap;
use crate::data::{FilterOutput, RawArgs};
use crate::field::EventField;
use crate::log_type::{AppType, OutputType};
use crate::parse::{BasicParser, ParseArgs, BATCH_SIZE};
use crate::stats_vec::StatsDiffVec;
use crate::time_chunks::{DetailEventTimeChunks, UnsortedTimeMap};
use crate::top::{GitalySummary, RailsSummary, ShellSummary, TopSummary};
use crate::{top, Args, Input, LogType};
use batcher::Batcher;

use std::fs::File;
use std::io::{self, ErrorKind, Read, Write};
use std::thread;
use terminal_size::{terminal_size, Width};

const MIN_PLOT_INTERVAL: u64 = 5;
const DEFAULT_TERM_WIDTH: usize = 120;

pub type DynBatcher = Batcher<Box<dyn Read + Send>>;

pub struct Executor<W> {
    pub args: Args,
    pub parse_args: ParseArgs,
    pub log_t: LogType,
    pub batcher: DynBatcher,
    pub out: W,
}

impl<W: Write> Executor<W> {
    pub fn new(args: Args, out: W) -> Result<Executor<W>, io::Error> {
        let mut batcher = match &args.input {
            Input::File(file_name) => {
                let f = File::open(file_name)?;
                DynBatcher::new(BATCH_SIZE, Box::new(f))
            }
            Input::Stdin => DynBatcher::new(BATCH_SIZE, Box::new(io::stdin())),
        };

        let (log_t, line_cache) = if let Some(log_t) = args.requested_log_type {
            (log_t, Vec::new())
        } else {
            check_type::check_log_type(&mut batcher)?
        };

        let parse_args = ParseArgs::new(args.search_for.clone(), args.thread_ct, line_cache);

        Ok(Executor {
            args,
            parse_args,
            log_t,
            batcher,
            out,
        })
    }

    pub fn stats(self) -> Result<(), io::Error> {
        if self.args.interval.is_some() {
            let time_stats = BasicParser::<UnsortedTimeMap<RawDataMap>>::new(RawArgs::new(
                self.log_t,
                self.args.interval,
            ))
            .parse(self.batcher, self.parse_args)
            .filter(self.args.search_for.as_deref())
            .into_sorted_stats_vec(self.args.sort_by);

            time_stats.print(
                self.out,
                self.args.print_format,
                &self.args.print_fields,
                self.args.limit,
                self.args.color,
            )
        } else {
            let stats =
                BasicParser::<RawDataMap>::new(RawArgs::new(self.log_t, self.args.interval))
                    .parse(self.batcher, self.parse_args)
                    .filter(self.args.search_for.as_deref())
                    .into_stats_vec()
                    .sort_by(self.args.sort_by);

            stats.print(
                self.out,
                self.args.print_format,
                &self.args.print_fields,
                self.args.limit,
                None,
                None,
            )
        }
    }

    pub fn stats_verbose(self) -> Result<(), io::Error> {
        let log_t = self.log_t.specify(OutputType::Detail);

        let mut data =
            log_t.calculate_detailed_time_data(self.args.interval, self.batcher, self.parse_args);

        if let Some(interval) = self.args.interval {
            let chunked_data =
                DetailEventTimeChunks::from_time_data(data, interval, log_t, EventField::Duration);

            let chunked_stats = chunked_data.into_verbose(self.args.sort_by, self.args.color);

            chunked_stats.print(
                self.out,
                self.args.print_format,
                &self.args.print_fields,
                self.args.limit,
                self.args.color,
            )
        } else {
            data.sort_by(EventField::Duration);

            let stats =
                data.into_verbose_stats_vec(log_t.stats_t(), self.args.sort_by, self.args.color);

            stats.print(
                self.out,
                self.args.print_format,
                &self.args.print_fields,
                self.args.limit,
            )
        }
    }

    pub fn compare(mut self, cmp_input: Input) -> Result<(), io::Error> {
        self.parse_args.thread_ct /= 2;

        let mut cmp_args = self.args.clone();
        cmp_args.input = cmp_input.clone();

        let mut cmp_exec = Executor::new(cmp_args, io::stdout().lock())?;
        cmp_exec.parse_args.thread_ct /= 2;

        if self.log_t.stats_t() == cmp_exec.log_t.stats_t() {
            let sort_by = self.args.sort_by;
            let search_for = self.args.search_for.clone();
            let cmp_search_for = self.args.search_for.clone();

            let handle = thread::spawn(move || {
                BasicParser::<RawDataMap>::new(RawArgs::new(self.log_t, self.args.interval))
                    .parse(self.batcher, self.parse_args)
                    .filter(search_for.as_deref())
                    .into_stats_vec()
                    .sort_by(sort_by)
            });

            let cmp_handle = thread::spawn(move || {
                BasicParser::<RawDataMap>::new(RawArgs::new(self.log_t, self.args.interval))
                    .parse(cmp_exec.batcher, cmp_exec.parse_args)
                    .filter(cmp_search_for.as_deref())
                    .into_stats_vec()
                    .sort_by(sort_by)
            });

            let stats = handle.join().expect("Primary thread panicked");
            let cmp_stats = cmp_handle.join().expect("Compare thread panicked");

            let file_name = self.args.input.file_name();
            let cmp_file_name = cmp_input.file_name();

            let diffs = StatsDiffVec::new(
                stats.stats_t,
                &stats,
                &cmp_stats,
                &file_name,
                &cmp_file_name,
                self.args.limit,
            );

            diffs.print(
                self.out,
                self.args.print_format,
                &self.args.print_fields,
                self.args.limit,
                self.args.color,
            )
        } else {
            eprintln!(
                "Log types found -- {} : {}",
                self.log_t.stats_t(),
                cmp_exec.log_t.stats_t()
            );
            Err(io::Error::new(
                ErrorKind::InvalidInput,
                "Attempted to compare different log types",
            ))
        }
    }

    pub fn bench(self, bench_vers: String) -> Result<(), io::Error> {
        let handle = thread::spawn(move || {
            BasicParser::<RawDataMap>::new(RawArgs::new(self.log_t, self.args.interval))
                .parse(self.batcher, self.parse_args)
                .filter(self.args.search_for.as_deref())
                .into_stats_vec()
                .sort_by(self.args.sort_by)
        });

        let file_name = self.args.input.file_name();
        let stats = handle.join().expect("Primary thread panicked");
        let stats_t = stats.stats_t;

        let bench_stats = match crate::bench::bench_stats(&bench_vers, stats_t) {
            Some(Ok(s)) => s,
            _ => {
                eprintln!("Benchmarking data not available for this version");
                return Ok(());
            }
        };

        let diffs = StatsDiffVec::new(
            stats_t,
            &bench_stats,
            &stats,
            &bench_vers,
            &file_name,
            self.args.limit,
        );

        diffs.print(
            self.out,
            self.args.print_format,
            &self.args.print_fields,
            self.args.limit,
            self.args.color,
        )
    }

    pub fn top(self, sort_by: EventField, display: top::ValueFmt) -> io::Result<()> {
        use AppType as At;

        let log_t = self.log_t.specify(OutputType::Detail);
        if let Some(interval) = self.args.interval {
            match log_t.app_type() {
                At::Rails => {
                    let chunks: UnsortedTimeMap<RailsSummary> = log_t
                        .calculate_unsorted_time_stats(interval, self.batcher, self.parse_args);
                    let stats = chunks.into_sorted();
                    stats.print(
                        self.out,
                        sort_by.into(),
                        display,
                        self.args.print_format,
                        self.args.limit,
                        self.args.color,
                    )
                }
                At::Gitaly => {
                    let chunks: UnsortedTimeMap<GitalySummary> = log_t
                        .calculate_unsorted_time_stats(interval, self.batcher, self.parse_args);
                    let stats = chunks.into_sorted();
                    stats.print(
                        self.out,
                        sort_by.into(),
                        display,
                        self.args.print_format,
                        self.args.limit,
                        self.args.color,
                    )
                }
                At::Shell => {
                    let chunks: UnsortedTimeMap<ShellSummary> = log_t
                        .calculate_unsorted_time_stats(interval, self.batcher, self.parse_args);
                    let stats = chunks.into_sorted();
                    stats.print(
                        self.out,
                        sort_by.into(),
                        display,
                        self.args.print_format,
                        self.args.limit,
                        self.args.color,
                    )
                }
            }
        } else {
            match log_t.app_type() {
                At::Rails => {
                    let data: RailsSummary =
                        log_t.calculate_top_stats(self.batcher, self.parse_args);
                    data.print(
                        self.out,
                        sort_by.into(),
                        display,
                        self.args.print_format,
                        self.args.limit,
                    )
                }
                At::Gitaly => {
                    let data: GitalySummary =
                        log_t.calculate_top_stats(self.batcher, self.parse_args);
                    data.print(
                        self.out,
                        sort_by.into(),
                        display,
                        self.args.print_format,
                        self.args.limit,
                    )
                }
                At::Shell => {
                    let data: ShellSummary =
                        log_t.calculate_top_stats(self.batcher, self.parse_args);
                    data.print(
                        self.out,
                        sort_by.into(),
                        display,
                        self.args.print_format,
                        self.args.limit,
                    )
                }
            }
        }
    }

    pub fn errors(self) -> io::Result<()> {
        let log_t = self.log_t.specify(OutputType::Error);

        let mut errors = log_t.calculate_error_summary(self.batcher, self.parse_args);
        errors.sort();

        let border_len = if self.args.no_border { 0 } else { 2 };

        let width = match terminal_size() {
            Some((Width(w), _)) => w.saturating_sub(border_len).max(40) as usize,
            None => DEFAULT_TERM_WIDTH,
        };

        errors.print(
            self.out,
            width,
            self.args.color,
            self.args.no_border,
            self.args.print_format,
        )
    }
}

#[cfg(feature = "plot")]
impl<W> Executor<W> {
    pub fn plot(self, output_path: Option<&str>) -> Result<(), Box<dyn std::error::Error>> {
        use crate::plot::{GitalyPlot, RailsPlot, ShellPlot};
        use crate::time_chunks::{Interval, TimeUnit};

        use std::ffi::OsStr;
        use std::path::PathBuf;

        let log_t = self.log_t.specify(OutputType::Detail);

        let output_path = match output_path {
            Some(p) => PathBuf::from(p),
            None => match self.args.input {
                Input::File(s) => {
                    let mut out_name = PathBuf::from(s)
                        .file_stem()
                        .map(|s| s.to_owned())
                        .expect("Received empty file name");
                    out_name.push(OsStr::new("_plot.png"));

                    PathBuf::from(out_name)
                }
                Input::Stdin => PathBuf::from("plot.png"),
            },
        };

        match log_t.app_type() {
            AppType::Rails => {
                let unadjusted: UnsortedTimeMap<RailsPlot> = log_t.calculate_unsorted_time_stats(
                    Interval::new(MIN_PLOT_INTERVAL, TimeUnit::Second),
                    self.batcher,
                    self.parse_args,
                );
                let target_secs = (unadjusted.total_dur() / 100).num_seconds() as u64;
                let adjusted =
                    unadjusted.adjust_interval(Interval::new(target_secs, TimeUnit::Second));

                crate::plot::plot_rails(adjusted.into_sorted(), &output_path)?;
            }
            AppType::Gitaly => {
                let unadjusted: UnsortedTimeMap<GitalyPlot> = log_t.calculate_unsorted_time_stats(
                    Interval::new(MIN_PLOT_INTERVAL, TimeUnit::Second),
                    self.batcher,
                    self.parse_args,
                );
                let target_secs = (unadjusted.total_dur() / 100).num_seconds() as u64;
                let adjusted =
                    unadjusted.adjust_interval(Interval::new(target_secs, TimeUnit::Second));

                crate::plot::plot_gitaly(adjusted.into_sorted(), &output_path)?;
            }
            AppType::Shell => {
                let unadjusted: UnsortedTimeMap<ShellPlot> = log_t.calculate_unsorted_time_stats(
                    Interval::new(MIN_PLOT_INTERVAL, TimeUnit::Second),
                    self.batcher,
                    self.parse_args,
                );
                let target_secs = (unadjusted.total_dur() / 100).num_seconds() as u64;
                let adjusted =
                    unadjusted.adjust_interval(Interval::new(target_secs, TimeUnit::Second));

                crate::plot::plot_shell(adjusted.into_sorted(), &output_path)?;
            }
        }

        writeln!(io::stdout(), "Plots written to {}", &output_path.display())?;

        Ok(())
    }
}

#[cfg(feature = "compress")]
impl<W: Write> Executor<W> {
    pub fn compress(self, out_path: &str) -> Result<(), Box<dyn std::error::Error>> {
        let stats = self
            .log_t
            .calculate_stats(self.batcher, self.parse_args, self.args.sort_by);

        if stats.stats.len() == 0 {
            eprintln!("Unable to parse input");
            std::process::exit(1);
        }

        let encoded: Vec<u8> = bincode::serialize(&stats)?;
        let mut compressor = lz4::EncoderBuilder::new()
            .level(9)
            .build(File::create(out_path)?)?;

        compressor.write_all(&encoded)?;
        compressor.finish().1?;

        Ok(())
    }
}
