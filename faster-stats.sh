#!/usr/bin/env bash
set -eo pipefail

if ! command -v fast-stats &> /dev/null
then
    echo "fast-stats must be in \$PATH for $0 to work! https://gitlab.com/gitlab-com/support/toolbox/fast-stats"
    exit
fi

if [[ -d "/var/log/gitlab" ]]
then
    starting_point="/var/log/gitlab"
    server_mode=true
    report_prefix="/tmp"
else
    starting_point="."
    report_prefix="."
fi


echo "Recursively gathering statistics for any GitLab logs stored in: $starting_point"
report_dir="${report_prefix}/gitlab-stats-$(date +%Y%m%d%H%M%S)"
mkdir -p "$report_dir"

find "$starting_point" -type f -name 'api_json.log' -exec fast-stats '{}' ';' -exec fast-stats top '{}' ';' -exec fast-stats --limit 10 errors '{}' ';' > "$report_dir/api_stats"
find "$starting_point" -type f -name 'production_json.log' -exec fast-stats '{}' ';' -exec fast-stats top '{}' ';' -exec fast-stats --limit 10 errors '{}' ';' > "$report_dir/prod_stats"
find "$starting_point" -path '*/gitaly/current' -exec fast-stats '{}' ';' -exec fast-stats top '{}' ';' -exec fast-stats --limit 10 errors '{}' ';' > "$report_dir/gitaly_stats"
find "$starting_point" -path '*/praefect/current' -exec fast-stats '{}' ';' -exec fast-stats top '{}' ';' -exec fast-stats --limit 10 errors '{}' ';' > "$report_dir/praefect_stats"
find "$starting_point" -path '*/sidekiq/current' -exec fast-stats '{}' ';' -exec fast-stats top '{}' ';' -exec fast-stats --limit 10 errors '{}' ';' > "$report_dir/sidekiq_stats"

if command -v jq &> /dev/null
then
    find "$starting_point" -type f -name 'exceptions_json.log' -exec cat '{}' '+' | jq '.["exception.message"]' | sort | uniq -c | sort -rn > "$report_dir/exceptions"
else
    echo "For best results: install jq to include exceptions in this report!"
fi

printf "Done!\n\n"

echo "Files for viewing stats:"
echo "$(realpath "$report_dir"):"
ls -l "$report_dir"
printf "\n"

if [[ "$server_mode" = true ]]
then
    archive_path="${report_prefix}/gitlab-stats-$(hostname)_$(date +%Y%m%d%H%M%S).tar.gz"
    tar -czf "$archive_path" "$report_dir"
    echo "Archive for sharing stats:"
    ls -ld "$archive_path"
fi
