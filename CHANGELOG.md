# Changelog

## [Unreleased]
### Added

### Changed
- The names of log types in `--type, -t` have been changed to make the most
  frequently used types the shortest. Use `api`, `production`, `gitaly`,
  `sidekiq`, and `shell` are what should be used for logs created in the last
  several major versions of GitLab.
- The `--sort` option on the `top` subcommand is renamed to `--sort-by` to match
  the equivalent flag on the main command.
- Several options for the `top` subcommand's `--sort-by` option have been renamed:
  - `cpu` -> `cpu-s`
  - `disk_r` -> `disk-read`
  - `disk_w` -> `disk-write`
  - `fail` -> `fail-ct`

- The `perc` display option on the `top` subcommand is expanded to full `percentage` word.

### Deprecated

### Removed

### Fixed
- Fix incorrect calculations for Gitaly logs when using the `--verbose` and
  `--interval` flags, #33.
- Fix invalid `top` and `plot` results with `--thread-ct 1`.

### Security
